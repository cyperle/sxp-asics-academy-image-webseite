/**
 * Overlay für Beschreibungen und Texte
 */

var BIGOVERLAY = {};

(function () {

    var big = {};

    big.prevent_page = $('<div class="prevent_page">');
    big.overlay = $('<div class="big_overlay">');
    big.white1 = $('<div class="white_1">').appendTo(big.overlay);
    big.white2 = $('<div class="white_2">').appendTo(big.overlay);
    big.content = $('<div class="big_content">').appendTo(big.overlay);
    big.titel = $('<div class="big_titel">').appendTo(big.content);
    big.beschreibung = $('<div class="big_beschreibung">').appendTo(big.content);
    big.close = $('<div class="big_close">').appendTo(big.overlay);

    BIGOVERLAY.pos = function () {

        var width = big.overlay.width();
        var height = big.overlay.height();
        var scrollTop = $.doc.scrollTop();

        var white_css = {
            width: width + 30,
            height: height + 30,
            left: -15,
            top: -15
        };

        big.white1.css(white_css);
        big.white2.css(white_css);

        var left = $.win.width() / 2 - big.overlay.width() / 2;
        var top = $.win.height() / 2 - big.overlay.height() / 2 + scrollTop;

        left = Math.max(0, left);
        top = Math.max(0, top);

        big.overlay.css({
            top: top,
            left: left
        });
    };

    BIGOVERLAY.show = function (titel, beschreibung, farbe) {

        big.overlay.hide();

        $(document.body).append(big.overlay);
        $(document.body).append(big.prevent_page);

        big.titel.html(titel);
        big.beschreibung.html(beschreibung);

        //var class_to_remove = big.content.determineClass(available_colors);
        big.content.attr('data-farbe', farbe);
        //big.overlay.fadeIn();

        TweenMax.fromTo(big.overlay, .5, {display: 'block', alpha: 0}, {alpha: 1});

        $(window).on("pageresize.bigoverlay", BIGOVERLAY.pos);
        BIGOVERLAY.pos();
        big.prevent_page.on("click", BIGOVERLAY.leave);
        big.close.on("click", BIGOVERLAY.leave);

    };

    BIGOVERLAY.leave = function () {

        big.close.off("click");
        big.prevent_page.off("click");

        TweenMax.to(big.overlay, .5, {
            alpha: 0,
            onComplete: function () {
                big.overlay.remove();
                big.prevent_page.remove();
            }
        });

        $(window).off("pageresize.bigoverlay", BIGOVERLAY.pos);

    }
    ;

})();


/**
 * Sticky Season Button
 */

(function ($) {

    var sticky = false;
    var stateChange = false;
    var $btn = $(".season_toggle_btn");
    var $parent = $(".header_space");
    var scrollTop = 0;
    var btnTop = 0;

    function setPos() {
        var fixedLeft = $parent.width() + $parent.offset().left;
        $btn.css("left", fixedLeft - $btn.width());
    }

    function makeSticky() {
        if (stateChange) {
            stateChange = false;
            if (sticky) {
                $btn.css({
                    position: "fixed",
                    right: "auto"
                });
                setPos();
            } else {
                $btn.css({
                    position: "absolute",
                    left: "",
                    right: ""
                });
            }
        }
    }

    function checkOffsetTop() {

        scrollTop = $.win.scrollTop();

        if (!sticky) {
            btnTop = $btn.offset().top;
        }

        if (scrollTop > btnTop && !sticky) {
            sticky = true;
            stateChange = true;
        } else if (scrollTop <= btnTop && sticky) {
            sticky = false;
            stateChange = true;
        }

        makeSticky();

    }

    function init() {
        if ($btn.length > 0) {
            $.win.on("scroll", checkOffsetTop);
        }
    }

    $(init);

})(jQuery);

