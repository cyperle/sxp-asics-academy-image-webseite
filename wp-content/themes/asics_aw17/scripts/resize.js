var vps = [
    ["vp-1400", 1400, 0],
    ["vp-960", 960, 0],
    ["vp-640", 640, 1]
];

var anistop = true;

$(document).ready(function () {
    pageResize = {};
    if (anistop === true) {
        $('html').addClass('no-trans');
    }

    var currentBreakpoint = '';

    pageResize.work = function (vps) {
        currentBreakpoint = '';
        var wWidth = $(window).width();
        var addClasses = '';
        var removeClasses = '';

        for (var i = 0; i < vps.length; i++) {
            if (wWidth < vps[i][1]) {
                addClasses += ' ' + vps[i][0];

                currentBreakpoint = i;
            } else {
                removeClasses += ' ' + vps[i][0];
            }

        }

        if (currentBreakpoint !== '') {
            if (vps[currentBreakpoint][2] === 1) {
                $('html').css('font-size', 16 * (wWidth / vps[currentBreakpoint][1]));
                setTimeout(function() {
                    if (anistop === true) {
                        $('html').removeClass('no-trans');
                        anistop = false;
                    }
                }, 100);
            } else {
                $('html').css('font-size', '16px');
                setTimeout(function() {
                    if (anistop === true) {
                        $('html').removeClass('no-trans');
                        anistop = false;
                    }
                }, 100);
            }
        } else {
            $('html').css('font-size', '16px');
            setTimeout(function() {
                if (anistop === true) {
                    $('html').removeClass('no-trans');
                    anistop = false;
                }
            }, 100);
        }

        $('body').removeClass(removeClasses);
        $('body').addClass(addClasses);
    };

    pageResize.init = function (vps) {

        $(window).resize(function () {
            pageResize.work(vps);
            $(window).trigger("pageresize");
        });
        pageResize.work(vps);

    };

    pageResize.init(vps);

});

