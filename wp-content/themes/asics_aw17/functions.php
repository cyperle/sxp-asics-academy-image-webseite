<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:15
 */

require("CyClasses/CyModul.php");
require("CyClasses/CyTheme.php");

//include("functions/custom_taxonomies.php"); IN PLUGIN AUSGELAGERT!
//include("functions/disable_schrott.php"); IN PLUGIN AUSGELAGERT!

CyModul::add_script(get_template_directory_uri() . "/scripts/TweenMax.min.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/jquery-3.2.1.min.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/cy-jquery.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/resize.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/global.js");