<?php


class ModulProduct extends CyModul
{

    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {

        $datenArray = [];
        $datenArray['stage'] = get_field('stage');
        $datenArray['program'] = get_field('programs');

        /* Es existiert ein weiteres Feld mit dem ähnlichen Schlüssel "category". Zur Verständlichkeit wird der Schlüssel der Programm-Kategorie-Relation hier diesmal mit dem Präfix "rel_" ausgestattet! */
        $datenArray['rel_category'] = get_field('categories');
        $datenArray['related_overview'] = Asics::getOverviewUrl($datenArray['rel_category']->slug, $datenArray['program']->slug);

        if (!empty($datenArray['program'])) {

            $datenArray['layout'] = get_field('layout', $datenArray['program']);

            if (empty($datenArray['layout'])) {
                $datenArray['layout'] = 'blue';
            } else {
                $datenArray['layout'] = CyTheme::mapLayout($datenArray['layout']);
            }

        } else {

            $datenArray['layout'] = 'blue';

        }

        if (!empty($datenArray['stage'])) {

            $datenArray['stage'] = $datenArray['stage'][0];
            $datenArray['scrolldown'] = str_replace(' ', '<br/>', $datenArray['stage']['scrolldown']);

            if (is_array($datenArray['stage']['men_women'])) {
                $datenArray['has_men'] = (in_array('Men', $datenArray['stage']['men_women'])) ? true : false;
                $datenArray['has_women'] = (in_array('Women', $datenArray['stage']['men_women'])) ? true : false;
            } else {
                $datenArray['has_men'] = false;
                $datenArray['has_women'] = false;
            }

            if ($datenArray['has_men']) {


                $datenArray['has_men'] = (!empty($datenArray['stage']['material_varianten_1_men'][0]['product_image']));

                if ($datenArray['has_men']) {

                    for ($i = 0; $i < 7; $i++) {
                        $j = $i + 1;
                        if ($datenArray['stage']['material_varianten_' . $j . '_men'][0]['status'] == 'Active') {
                            $datenArray['materials_men'][] = $datenArray['stage']['material_varianten_' . $j . '_men'];
                        }
                    }


                    /*	Array(
                        $datenArray['stage']['material_varianten_1_men'],
                        $datenArray['stage']['material_varianten_2_men'],
                        $datenArray['stage']['material_varianten_3_men'],
                        $datenArray['stage']['material_varianten_4_men'],
                        $datenArray['stage']['material_varianten_5_men']
                    );*/

                    $j = 0;
                    $datenArray['materials_men']['highlights'] = array();
                    if ($datenArray['stage']['material_varianten_1_men'][0]['status'] == 'Active') {
                        foreach ($datenArray['materials_men'][0] as $nr => $material) {
                            if ($material['technologies'] !== false) {


                                foreach ($material['technologies'] as $key => $val) {


                                    $datenArray['materials_men']['highlights'][$j] = array();
                                    $datenArray['materials_men']['highlights'][$j]['bild'] = $nr;
                                    $datenArray['materials_men']['highlights'][$j]['name'] = $val['technology_selector']->name;
                                    $datenArray['materials_men']['highlights'][$j]['desc'] = $val['technology_selector']->description;
                                    $datenArray['materials_men']['highlights'][$j]['y'] = $val['y'];
                                    $datenArray['materials_men']['highlights'][$j]['x'] = $val['x'];
                                    $datenArray['materials_men']['highlights'][$j]['size'] = $val['size'];

                                    $j++;
                                }
                            }
                        }
                    }

                }

            }

            if ($datenArray['has_women']) {

                $datenArray['has_women'] = (!empty($datenArray['stage']['material_varianten_1_women'][0]['product_image']));
                for ($i = 0; $i < 7; $i++) {
                    $j = $i + 1;
                    if ($datenArray['stage']['material_varianten_' . $j . '_women'][0]['status'] == 'Active') {
                        $datenArray['materials_women'][] = $datenArray['stage']['material_varianten_' . $j . '_women'];
                    }
                }
                /*$datenArray['materials_women'] = Array(
                    $datenArray['stage']['material_varianten_1_women'],
                    $datenArray['stage']['material_varianten_2_women'],
                    $datenArray['stage']['material_varianten_3_women'],
                    $datenArray['stage']['material_varianten_4_women'],
                    $datenArray['stage']['material_varianten_5_women']
                );*/


                $datenArray['materials_women']['highlights'] = array();
                if ($datenArray['stage']['material_varianten_1_women'][0]['status'] == 'Active') {
                    foreach ($datenArray['materials_women'][0] as $nr => $material) {
                        if ($material['technologies'] !== false) {


                            foreach ($material['technologies'] as $key => $val) {


                                $datenArray['materials_women']['highlights'][$j] = array();
                                $datenArray['materials_women']['highlights'][$j]['bild'] = $nr;
                                $datenArray['materials_women']['highlights'][$j]['name'] = $val['technology_selector']->name;
                                $datenArray['materials_women']['highlights'][$j]['desc'] = $val['technology_selector']->description;
                                $datenArray['materials_women']['highlights'][$j]['y'] = $val['y'];
                                $datenArray['materials_women']['highlights'][$j]['x'] = $val['x'];
                                $datenArray['materials_women']['highlights'][$j]['size'] = $val['size'];

                                $j++;
                            }
                        }
                    }
                }
            }

        }

        $techs = get_field('technologies2');

        $datenArray['highlights'] = Array();
        $datenArray['technologies'] = Array();

        foreach ($techs as &$tech) {

            $name = $tech['technology']->name;
            $cat_name = get_field('category_name', $tech['technology']);

            if (!isset($datenArray['technologies'][$cat_name])) {
                $datenArray['technologies'][$cat_name] = Array();
            }
            $cat = &$datenArray['technologies'][$cat_name];

            $cat[$name]['name'] = $name;
            $cat[$name]['desc'] = $tech['technology']->description;
        }


        $datenArray['technologies_headline'] = get_field('technologies_headline');
        $datenArray['technologies_open_label'] = get_field('technologies_open_label');
        $datenArray['updates'] = get_field('updates');
        $datenArray['updates_headline'] = get_field('updates_headline');
        $datenArray['features_headline'] = get_field('headline');
        $datenArray['features_open_label'] = get_field('open_label');
        $datenArray['category'] = get_field('category');
        $datenArray['type'] = get_field('type');
        $datenArray['distance'] = get_field('distance');
        $datenArray['weight'] = get_field('weight');
        $datenArray['heel_drop'] = get_field('heel_drop');
        $datenArray['item'] = get_field('item');
        $datenArray['key_colors'] = get_field('key_colors');
        $datenArray['srp_euro'] = get_field('srp_euro');
        $datenArray['intro'] = get_field('intro');
        $datenArray['sdp'] = get_field('sdp');
        $datenArray['consumer_segment'] = get_field('consumer_segment');

        $this->data = $datenArray;

    }

    private $unique_create_triangle_mask = 0;

    private function create_triangle($pass)
    {

        $def = new stdClass();
        $def->src = "http://cy-live1.de/asics2017/wp-content/uploads/2017/06/primary_T749N_4358_GEL-KAYANO-24.png";
        $def->img_width = 662;
        $def->img_height = 306;
        $def->width = 150;
        $def->height = 75;
        $def->x = 220;
        $def->y = 150;
        $def->material = "";

        $def = (object)array_merge((array)$def, (array)$pass);

        $points = $def->x / $def->img_width . ' ' . $def->y / $def->img_height . ',
         ' . (($def->x + ($def->width / 2)) / $def->img_width) . ' ' . ((($def->y + $def->height) / $def->img_height)) . ',
         ' . (($def->x + $def->width) / $def->img_width) . ' ' . $def->y / $def->img_height;

        $id = 'unique-triangle-mask' . $this->unique_create_triangle_mask++;
        $tri_css = 'width:' . $def->width / 16 . 'rem;height:' . $def->height / 16 . 'rem';
        $tri_holder_css = 'position:relative;left:' . ($def->x * -1) . 'px;top:' . ($def->y * -1) . 'px';

        ?>

        <div class="tri" style="<?php echo $tri_css; ?>" data-material="<?php echo $def->material; ?>">
            <div class="tri_container" style="overflow:hidden;<?php echo $tri_css; ?>">
                <div class="tri_hover" data-material="<?php echo $def->material; ?>"></div>
                <div class="tri_holder">

                    <svg width="<?php echo $def->img_width / 16; ?>rem" height="<?php echo $def->img_height / 16; ?>rem"
                         style="width:<?php echo $def->img_width / 16; ?>rem; height: <?php echo $def->img_height / 16; ?>rem;"
                    >
                        <defs>
                            <clipPath id="<?php echo $id; ?>" clipPathUnits="objectBoundingBox">
                                <polygon points="<?php echo $points; ?>"/>
                            </clipPath>
                        </defs>
                        <image
                                class="tri_image"
                                width="100%" height="100%" style="width:100%; height: 100%;"
                                xlink:href="<?php echo $def->src; ?>"
                                clip-path="url(<?php echo '#' . $id; ?>)"/>
                    </svg>
                </div>
            </div>
        </div>

        <?php

    }

    public function print_module()
    {

        $has_men = $this->data['has_men'];
        $has_women = $this->data['has_women'];

        ?>

        <script>
            var readmore = '<?=__('read more', 'CyTheme@Asics');?>';
            var myvideo = '<?=$this->data['stage']['video_url']?>';
        </script>

        <section class="product_single <?php echo $this->data['layout']; ?>"
                 data-layout="<?php echo $this->data['layout']; ?>">

            <!-- Flap -->
            <div class="usn flap">
                <span>
                    <?php echo $this->data['program']->name ?>
                </span>
                <?php if ($this->data['related_overview'] !== null) : ?>
                    <p>
                        <a href="<?php echo $this->data['related_overview']; ?>">
                            <?php echo $this->data['stage']['overview_label'] ?>
                        </a>
                    </p>
                <?php endif; ?>
            </div>

            <div class="product_top">

                <!-- Beschreibung -->
                <section class="product_description">
                    <div class="inner_wrapper paragraphs">
                        <p>
                            <?php echo strip_tags(apply_filters('the_content', $this->data['stage']['description'])); ?>
                        </p>
                    </div>
                </section>

                <!-- Slider -->
                <?php

                $nav_triangles = "";
                $i = 0;

                if ($has_men) :

                    ?>

                    <section class="slider slider_men" data-gender="men">

                        <!-- Highlights -->
                        <div class="info-menu focus ">
                            <div class="pre-t"></div>
                            <div class="pre-t-2"></div>
                            <h4 class="heading">
                                // HIGHLIGHTS
                            </h4>
                            <div class="menu-wrapper">
                                <?php $i = 0;
                                if (isset($this->data['materials_men']['highlights'])) {
                                    foreach ($this->data['materials_men']['highlights'] as $key => $val):

                                        $i++;
                                        echo '<div class="menu-point menu-point-' . ($i + 1) . '"data-text="' . str_replace('"', '”', $val['desc']) . '"  data-highlight="' . ($i + 1) . '" data-image="' . $val['bild'] . '" data-size="' . $val['size'] . '" data-x="' . $val['x'] . '" data-y="' . $val['y'] . '"><p>' . $val['name'] . '</p></div>';
                                    endforeach;
                                }
                                ?>
                            </div>
                            <div class="after-t"></div>
                            <div class="after-t-2"></div>
                            <?php if ($this->data['stage']['video_url'] != '' && $this->data['stage']['video_label'] != ''): ?>
                                <div class="vid">
                                    <div>
                                        <?= $this->data['stage']['video_label'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->data['updates'] != '' && $this->data['updates_headline'] != ''): ?>
                                <div class="vid update">
                                    <div>
                                        <?= $this->data['updates_headline'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="slider_wrapper">
                            <div class="slider_holder">
                                <div class="highlights men">

                                    <?php
                                    $read = $this->data['stage']['info_button_text_top'];
                                    $more = $this->data['stage']['info_button_text_bottom'];
                                    $i = 0;
                                    if (isset($this->data['materials_men']['highlights'])) {
                                        $dataMater = $this->data['materials_men']['highlights'];
                                        foreach ($dataMater as $keys => $vals):
                                            $full_width = (760 / 16) * ($vals['size'] / 100);
                                            $half_width = $full_width / 2;
                                            $font_size = 760 / 6 * ($vals['size'] / 125) / 16;
                                            $i++;
                                            echo
                                                '<div class="highlight" data-image="' . $vals['bild'] . '" data-name="' . $vals['name'] . '" data-text="' . str_replace('"', '”', $vals['desc']) . '" data-highlight="' . ($i + 1) . '" style="left:' . $vals['x'] . '%;top:' . $vals['y'] . '%;">
													<div class="highlight_inner" style="height:' . $full_width . 'rem; width:' . $full_width . 'rem; margin-left:-' . $half_width . 'rem; margin-top:-' . $half_width . 'rem;">
														<div class="highlight_transparent">
															<div class="highlight_canvas">
																<div class="hightlight_canvas_inner">
																	<div class="highlight_hover">
																		<div class="highlight_hover_active" style="font-size:' . $font_size . 'rem">
																		<span>
																			   ' . $read . '<br/>
																			   <span class="more-svg"></span>
																			   ' . $more . '<br/>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>';
                                        endforeach;
                                    }
                                    $i = 0;
                                    ?>
                                </div>
                                <div class="slider_gallery">
                                    <div class="slider_frame">
                                        <?php foreach ($this->data['materials_men'] as $mat_i => &$mat) :
                                            if (empty($mat)) {
                                                continue;
                                            }
                                            foreach ($mat as &$pic) :
                                                $bg = $pic['product_image'];
                                                if (empty($bg)) {
                                                    continue;
                                                }
                                                ?>

                                                <div
                                                        class="slider_item"
                                                        data-item="<?php echo $i; ?>"
                                                        data-material="<?php echo $mat_i; ?>"
                                                        style="background-image:url('<?php echo $bg; ?>')">
                                                </div>

                                                <?php
                                                $nav_triangles .= '<div class="slider_nav_triangle" data-item="' . $i . '" data-material="' . $mat_i . '"></div>';
                                                $i++;
                                            endforeach;
                                            ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="arrow prev"></div>
                                <div class="arrow next"></div>
                            </div>

                            <div class="slider_nav">
                                <?php echo $nav_triangles; ?>
                            </div>

                        </div>

                    </section>

                <?php endif; ?>

                <?php

                $i = 0;
                $nav_triangles = "";
                if ($has_women) : ?>

                    <section class="slider slider_women" style="<?php if ($has_men) {
                        echo 'display:none';
                    } ?>">
                        <!-- Highlights -->
                        <div class="info-menu focus ">
                            <div class="pre-t"></div>
                            <div class="pre-t-2"></div>
                            <h4 class="heading">
                                // HIGHLIGHTS
                            </h4>
                            <div class="menu-wrapper">
                                <?php
                                $i = 0;
                                if (isset($this->data['materials_women']['highlights'])) {
                                    foreach ($this->data['materials_women']['highlights'] as $key => $val):

                                        $i++;
                                        echo '<div class="menu-point menu-point-' . ($i + 1) . '"data-text="' . str_replace('"', '”', $val['desc']) . '"  data-highlight="' . ($i + 1) . '" data-image="' . $val['bild'] . '" data-size="' . $val['size'] . '" data-x="' . $val['x'] . '" data-y="' . $val['y'] . '"><p>' . $val['name'] . '</p></div>';
                                    endforeach;
                                }
                                ?>
                            </div>
                            <div class="after-t"></div>
                            <div class="after-t-2"></div>

                            <?php if ($this->data['stage']['video_url'] != '' && $this->data['stage']['video_label'] != ''): ?>
                                <div class="vid">
                                    <div>
                                        <?= $this->data['stage']['video_label'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->data['updates'] != '' && $this->data['updates_headline'] != ''): ?>
                                <div class="vid update">
                                    <div>
                                        <?= $this->data['updates_headline'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>


                        <div class="slider_wrapper">
                            <div class="slider_holder">
                                <div class="highlights women">

                                    <?php
                                    $read = $this->data['stage']['info_button_text_top'];
                                    $more = $this->data['stage']['info_button_text_bottom'];
                                    $i = 0;
                                    if (isset($this->data['materials_women']['highlights'])) {
                                        $dataMater = $this->data['materials_women']['highlights'];
                                        foreach ($dataMater as $keys => $vals):
                                            $full_width = (760 / 16) * ($vals['size'] / 100);
                                            $half_width = $full_width / 2;
                                            $font_size = 760 / 6 * ($vals['size'] / 125) / 16;
                                            $i++;
                                            echo
                                                '<div class="highlight" data-image="' . $vals['bild'] . '" data-name="' . $vals['name'] . '" data-text="' . str_replace('"', '”', $vals['desc']) . '" data-highlight="' . ($i + 1) . '" style="left:' . $vals['x'] . '%;top:' . $vals['y'] . '%;">
													<div class="highlight_inner" style="height:' . $full_width . 'rem; width:' . $full_width . 'rem; margin-left:-' . $half_width . 'rem; margin-top:-' . $half_width . 'rem;">
														<div class="highlight_transparent">
															<div class="highlight_canvas">
																<div class="hightlight_canvas_inner">
																	<div class="highlight_hover">
																		<div class="highlight_hover_active" style="font-size:' . $font_size . 'rem">
																			<span>
																			   ' . $read . '<br/>
																			   <span class="more-svg"></span>
																			   ' . $more . '<br/>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>';
                                        endforeach;
                                    }
                                    $i = 0;
                                    ?>
                                </div>
                                <div class="slider_gallery">
                                    <div class="slider_frame">
                                        <?php foreach ($this->data['materials_women'] as $mat_i => &$mat) :
                                            if (empty($mat)) {
                                                continue;
                                            }
                                            foreach ($mat as &$pic) :
                                                if(!isset($pic['product_image'])) continue;
                                                $bg = $pic['product_image'];
                                                if (empty($bg)) {
                                                    continue;
                                                }
                                                ?>

                                                <div
                                                        class="slider_item"
                                                        data-item="<?php echo $i; ?>"
                                                        data-material="<?php echo $mat_i; ?>"
                                                        style="background-image:url('<?php echo $bg; ?>')">
                                                </div>

                                                <?php
                                                $nav_triangles .= '<div class="slider_nav_triangle" data-item="' . $i . '" data-material="' . $mat_i . '"></div>';
                                                $i++;
                                            endforeach;
                                            ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="arrow prev"></div>
                                <div class="arrow next"></div>
                            </div>

                            <div class="slider_nav">
                                <?php echo $nav_triangles; ?>
                            </div>

                        </div>

                    </section>

                <?php endif; ?>
                <div class="learn-more">
                    <?= $this->data['scrolldown'] ?><br/>
                </div>
                <section class="design">
                    <div class="inner_wrapper">

                        <?php if ($has_men || $has_women) :

                            $initial_gender = 'men';
                            if (!$has_men && $has_women) {
                                $initial_gender = 'women';
                            }

                            ?>

                            <div class="gender_select <?php echo $initial_gender; ?>">

                                <div class="bg"></div>
                                <div class="bg-active"></div>

                                <div class="men <?php if (!$has_men) {
                                    echo 'inactive';
                                } ?>">
                                    <div class="unskew">
                                        <div class="men_icon"></div>
                                        <div class="men_label"><?php echo $this->data['stage']['men_label']; ?></div>
                                    </div>
                                </div>

                                <div class="women  <?php if (!$has_women) {
                                    echo 'inactive';
                                } ?>">
                                    <div class="unskew">
                                        <div class="women_label"><?php echo $this->data['stage']['women_label']; ?></div>
                                        <div class="women_icon"></div>
                                    </div>
                                </div>

                            </div>

                        <?php endif; ?>

                        <div class="material_selection">

                            <div class="material_inner">

                                <?php if ($has_men) : ?>

                                    <div class="material_items material_men">

                                        <?php
                                        foreach ($this->data['materials_men'] as $mat_i => &$mat) {

                                            if (empty($mat)) {
                                                continue;
                                            }

                                            foreach ($mat as &$pic) {

                                                if(!isset($pic['product_image'])) continue;
                                                $bg = $pic['product_image'];
                                                if (empty($bg)) {
                                                    continue;
                                                }

                                                $args = new stdClass();
                                                $args->src = $bg;
                                                $args->material = $mat_i;
                                                $this->create_triangle($args);
                                                break;

                                            }

                                        }
                                        ?>

                                    </div>

                                <?php endif; ?>

                                <?php if ($has_women) : ?>

                                    <div class="material_items material_women"
                                         style="<?php if ($has_men) {
                                             echo 'display:none';
                                         } ?>">

                                        <?php

                                        foreach ($this->data['materials_women'] as $mat_i => &$mat) {

                                            if (empty($mat)) {
                                                continue;
                                            }

                                            foreach ($mat as &$pic) {

                                                if(!isset($pic['product_image'])) continue;
                                                $bg = $pic['product_image'];
                                                if (empty($bg)) {
                                                    continue;
                                                }

                                                $args = new stdClass();
                                                $args->src = $bg;
                                                $args->material = $mat_i;
                                                $this->create_triangle($args);
                                                break;

                                            }

                                        }
                                        ?>

                                    </div>

                                <?php endif; ?>

                            </div>

                        </div>

                    </div>

                </section>

            </div>

            <section>

                <header class="tech-header">
                    <h2><?php echo $this->data['technologies_headline']; ?></h2>
                    <span><?php echo $this->data['technologies_open_label']; ?><img
                                src="<?= get_template_directory_uri() ?>/images/Product_v200/pointer_arrow.png"
                                alt="V"></span>
                    <div></div>
                </header>

                <div class="tech-bg">
                    <div class="tech">
                        <div class="inner_wrapper clearfix">
                            <ul>

                                <?php
                                foreach ($this->data['technologies'] as $cat_name => &$cat) {
                                    echo '<li><h2>' . $cat_name . '</h2><p>';
                                    $i = 0;
                                    foreach ($cat as $tech_name => &$tech) {
                                        if ($i > 0) {
                                            echo ', ';
                                        }
                                        echo '<span class="tech-desc" data-desc="' . strip_tags(str_replace('"', '”', $tech['desc'])) . '">' . $tech['name'] . '</span>';
                                        $i++;
                                    }
                                }
                                ?>
                                <?php if ($this->data['updates_headline'] != '' && $this->data['updates'] != ''): ?>
                                    <li class="updates">
                                        <h2><?= $this->data['updates_headline'] ?></h2>
                                        <p class="updates_text">
                                            <?= strip_tags($this->data['updates'], '<br/><br><ul></ul><strong><li></li><a></a><underline></underline></strong><strike></strike><em></em><blockquote></blockquote><del></del><ins></ins><ol></ol><code></code>') ?>
                                        </p>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="close-tech"></div>
                    </div>
                </div>

            </section>

            <section>

                <header class="features-header">
                    <h2><?php echo $this->data['features_headline']; ?></h2>
                    <span><?php echo $this->data['features_open_label']; ?><img
                                src="<?= get_template_directory_uri() ?>/images/Product_v200/pointer_arrow.png"
                                alt="V"></span>
                    <div></div>
                </header>

                <div class="features">
                    <div class="inner_wrapper clearfix">
                        <div class="column">
                            <div class="col_headline clearfix"><h2>SPECIFICATION</h2></div>
                            <ul class="category">
                                <?php if (!empty($this->data['category'])) : ?>
                                    <li class="clearfix">
                                        <h4>Category</h4>
                                        <span><?php echo $this->data['category']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['type'])) : ?>
                                    <li class="clearfix">
                                        <h4>Type</h4>
                                        <span><?php echo $this->data['type']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['distance'])) : ?>
                                    <li class="clearfix">
                                        <h4>Size</h4>
                                        <span><?php echo $this->data['distance']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['weight'])) : ?>
                                    <li class="clearfix">
                                        <h4>Weight</h4>
                                        <span><?php echo $this->data['weight']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['heel_drop'])) : ?>
                                    <li class="clearfix">
                                        <h4>Heel Drop</h4>
                                        <span><?php echo $this->data['heel_drop']; ?></span>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="column">
                            <div class="col_headline clearfix"><h2>COMMERCIAL</h2>
                            </div>
                            <ul class="category">
                                <?php if (!empty($this->data['item'])) : ?>
                                    <li class="clearfix">
                                        <h4>Item#</h4>
                                        <span><?php echo $this->data['item']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['key_colors'])) : ?>
                                    <li class="clearfix">
                                        <h4>Key Color</h4>
                                        <span><?php echo $this->data['key_colors']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['srp_euro'])) : ?>
                                    <li class="clearfix">
                                        <h4>ARP Euro</h4>
                                        <span><?php echo $this->data['srp_euro']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['intro'])) : ?>
                                    <li class="clearfix">
                                        <h4>Intro</h4>
                                        <span><?php echo $this->data['intro']; ?></span>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($this->data['sdp'])) : ?>
                                    <li class="clearfix">
                                        <h4>SDP</h4>
                                        <span><?php echo $this->data['sdp']; ?></span>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="column">
                            <div class="col_headline clearfix"><h2>TARGET GROUP</h2></div>
                            <ul class="category">
                                <?php if (!empty($this->data['consumer_segment'])) : ?>
                                    <li class="clearfix">
                                        <h4>Consumer Segment</h4>
                                        <span><?php echo $this->data['consumer_segment']; ?></span>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="close-features"></div>
                </div>

            </section>

        </section>
        <?php

        parent::add_script(get_template_directory_uri() . '/module/Product_v200/scripts/Product_v200.js');
    }

}

new ModulProduct();

?>