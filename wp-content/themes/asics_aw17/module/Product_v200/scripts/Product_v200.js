(function () {

    readmore = readmore.replace('', '&nbsp;');
    var $tech_desc;
    var $product_single;
    var layout = 'blue';

    function on_tech() {
        var $this = $(this);
        var desc = $this.data('desc');
        var title = $this.text();
        BIGOVERLAY.show(title, desc, layout);
    }

    function init() {
        $product_single = $('.product_single');
        var tmp = $product_single.data('layout');
        if (tmp !== "") {
            layout = tmp;
        }
        $tech_desc = $('.tech-desc');
        $tech_desc.onEnterClick(on_tech);
    }

    $(init);

    $(document).ready(function () {

        var originalHeight = $('.tech-bg .inner_wrapper').height();
        var originalFeatHeight = $('.features .inner_wrapper').height();
        $(window).resize(function () {
            originalHeight = $('.tech-bg .inner_wrapper').height();
            originalFeatHeight = $('.features .inner_wrapper').height();
            if ($('.tech').height() !== originalHeight && $('.tech').height() !== 0) {
                $('.tech').height(originalHeight);
            }
            if ($('.features').height() !== originalFeatHeight && $('.features').height() !== 0) {
                $('.features').height(originalFeatHeight);
            }
        });

        $('.tech-header').click(function () {
            techOpener();
        });
        $(' .close-tech').click(function () {
            techOpener();
        });

        var techOpener = function () {
            if (!$('.tech').hasClass('closed')) {
                $('.tech-header div').addClass('out');
                $('.tech-header span').css('opacity', '1');

                $('.tech').animate({height: 0}, 300, function () {
                    $('.tech').addClass('closed');
                });
            }
            if ($('.tech').hasClass('closed')) {
                $('.tech').animate({height: originalHeight}, 300, function () {
                    $('.tech').removeClass('closed');
                    $('.tech-header div').removeClass('out');
                    $('.tech-header span').css('opacity', '0');
                });
            }
        };

        $('.features-header').click(function () {
            featOpener();
        });
        $('.close-features').click(function () {
            featOpener();
        });

        var featOpener = function () {
            if (!$('.features').hasClass('closed')) {
                $('.features-header div').addClass('out');
                $('.features-header span').css('opacity', '1');

                $('.features').animate({height: 0}, 300, function () {
                    $('.features').addClass('closed');
                });
            }
            if ($('.features').hasClass('closed')) {
                $('.features').animate({height: originalFeatHeight}, 300, function () {
                    $('.features').removeClass('closed');
                    $('.features-header div').removeClass('out');
                    $('.features-header span').css('opacity', '0');
                });
            }

        }

    });

})();

//Read More Functionality

(function ($) {

    var win = $.win;
    var infoItem;
    var originalText = '';

    /**
     * Initialisiert die Funktionalität der einzelnen Programm-Elemente.
     */
    function fn_programme() {

        /**
         * Diese Klasse dient als Controller für die Programm-Elemente im DOM.
         * @param el
         */
        function Programm_Item(el) {

            this.$el = $(el);
            this.titel = $(".bread-last span").text();


            //this.$btn_konzept.on("click", this.display_overlay.bind(this));
            //this.$btn_konzept.onEnterClick(this.display_overlay.bind(this));


            this.$beschreibung_div = this.$el.find(".product_description");
            this.$beschreibung_holder = this.$el.find(".inner_wrapper.paragraphs");
            this.$beschreibung_text_div = this.$el.find(".inner_wrapper.paragraphs p");
            this.$beschreibung_text_div_cached = this.$el.find(".inner_wrapper.paragraphs p");
            this.beschreibung_html = this.$beschreibung_text_div.html();
            this.beschreibung_text = this.$beschreibung_text_div.text();

            this.beschreibung_text_cached = this.$beschreibung_text_div.text();
            this.beschreibung_words = this.beschreibung_text.split(" ");

            this.$weiterlesen = $('<span class="weiterlesen">').html("...&nbsp;" + readmore + "&nbsp;");

            this.color = $('.product_single').data('layout');

            this.overlay_status = "closed"; // rendering, open
            this.overlay_rendered = false;

            Programm_Item.item_list.push(this);

        }

        Programm_Item.item_list = [];

        Programm_Item.prototype = {

            /**
             * Zeigt das große Overlay für das Programm-Element an.
             */
            show_big_overlay: function () {
                BIGOVERLAY.show(this.titel, this.beschreibung_html, this.color);

            },

            /**
             * Blendet das Standard-Overlay ein.
             */
            display_overlay: function () {

                this.overlay_status = "rendering";

                //this.$btn_konzept.off("click");
                this.$btn_konzept.offEnterClick();
                //this.$btn_close.on("click", this.hide_overlay.bind(this));
                this.$btn_close.onEnterClick(this.hide_overlay.bind(this));
                this.$beschreibung_text_div_cached.onEnterClick(this.show_big_overlay.bind(this));

                this.$overlay.show();

                this.render_beschreibung();

            },

            /**
             * Blendet das Standard-Overlay aus.
             */
            hide_overlay: function () {

                //this.$btn_konzept.on("click", this.display_overlay.bind(this));
                this.$beschreibung_text_div_cached.offEnterClick();

                this.overlay_status = "closed";

            },

            /**
             * Rendert den String der Beschreibung im Standard-Overlay und zeigt bei Überlänge einen "weiterlesen"-Button an, der das große Overlay öffnet.
             */
            render_beschreibung: function () {
                this.overlay_rendered = false;
                this.$beschreibung_text_div = this.$beschreibung_text_div_cached;

                /*fix*/
                originalText = (originalText == '') ? this.beschreibung_text_cached : this.$beschreibung_text_div.text(this.beschreibung_text_cached);
                this.beschreibung_text = this.beschreibung_text_cached;
                this.beschreibung_words = this.beschreibung_text.split(" ");

                if (this.overlay_rendered) {
                    return;
                }

                var _this = this,
                    beschreibung_height = this.$beschreibung_div.height(),
                    words = this.beschreibung_words,
                    current_text = "",
                    tmp_text = null,
                    cut = false;


                function validate_height() {
                    return _this.$beschreibung_holder.height() < beschreibung_height;
                }

                if (!validate_height()) {
                    var i;
                    for (i = 0; i < words.length; i++) {

                        tmp_text = current_text + " " + words[i] + " " + this.$weiterlesen.html();
                        var tmp_text_ohne_weiterlesen = current_text + " " + words[i];
                        this.$beschreibung_text_div.text(tmp_text);

                        if (validate_height()) {
                            current_text = tmp_text_ohne_weiterlesen;
                        } else {
                            this.$beschreibung_text_div.text(tmp_text_ohne_weiterlesen);
                            cut = true;
                            break;
                        }

                    }

                }

                if (!cut) {
                    this.$weiterlesen.remove();
                } else {
                    this.$beschreibung_text_div.html(this.$beschreibung_text_div.html() + '<span class="weiterlesen">...&nbsp;' + readmore + '&nbsp;</span>');
                }

                this.overlay_rendered = true;

            }

        };
        /**
         * Selektiert jedes Programm-Element und erzeugt ein Programm_Item-Objekt.
         */
        infoItem = new Programm_Item($(".product_top"));
        infoItem.render_beschreibung();
        $('.paragraphs p').onEnterClick(infoItem.show_big_overlay.bind(infoItem));
        $(window).resize(function () {
            infoItem.render_beschreibung();
            $('.paragraphs p').onEnterClick(infoItem.show_big_overlay.bind(infoItem));
        });

    }

    function init() {
        fn_programme();
    }

    $(init);

    $('.vid:not(.update)').click(function () {
        if (myvideo.indexOf("mp4") === 0) {
            BIGOVERLAY.show('', '<video controls src="' + myvideo + '"></video>', 'video');
        } else {
            BIGOVERLAY.show('', '<iframe src="https://player.vimeo.com/video' + myvideo.substr(myvideo.lastIndexOf("/"), myvideo.length) + '?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 'video');
        }
    });

    $('.update').click(function () {
        BIGOVERLAY.show($(this)[0].innerHTML, $('.updates_text')[0].outerHTML, $('.product_single').data('layout'));
    });

    $('.learn-more').click(function () {
        $('html, body').animate({
            scrollTop: $(".tech-header").offset().top
        }, 1000);
    });

})(jQuery);

(function () {

    var $tech_desc;
    var $product_single;
    var layout = 'blue';

    function on_tech() {
        var $this = $(this);
        var desc = $this.attr('data-name');
        var title = $this.attr('data-text');


        BIGOVERLAY.show(title, desc, layout);
    }

    function init() {
        $product_single = $('.product_single');
        var tmp = $product_single.data('layout');
        if (tmp !== "") {
            layout = tmp;
        }

        $tech_desc = $('.tech-desc');
        $tech_desc.onEnterClick(on_tech);
    }

    $(init);

    $(document).ready(function () {

        var originalHeight = $('.tech-bg .inner_wrapper').height();
        var originalFeatHeight = $('.features .inner_wrapper').height();
        $(window).resize(function () {
            originalHeight = $('.tech-bg .inner_wrapper').height();
            originalFeatHeight = $('.features .inner_wrapper').height();
            if ($('.tech').height() !== originalHeight && $('.tech').height() !== 0) {
                $('.tech').height(originalHeight);
            }
            if ($('.features').height() !== originalFeatHeight && $('.features').height() !== 0) {
                $('.features').height(originalFeatHeight);
            }
        });

        $('.tech-header').click(function () {
            techOpener();
        });
        $(' .close-tech').click(function () {
            techOpener();
        });

        var techOpener = function () {
            if (!$('.tech').hasClass('closed')) {
                $('.tech-header div').addClass('out');
                $('.tech-header span').css('opacity', '1');

                $('.tech').animate({height: 0}, 300, function () {
                    $('.tech').addClass('closed');
                });
            }
            if ($('.tech').hasClass('closed')) {
                $('.tech').animate({height: originalHeight}, 300, function () {
                    $('.tech').removeClass('closed');
                    $('.tech-header div').removeClass('out');
                    $('.tech-header span').css('opacity', '0');
                });
            }
        };

        $('.features-header').click(function () {
            featOpener();
        });
        $('.close-features').click(function () {
            featOpener();
        });

        var featOpener = function () {
            if (!$('.features').hasClass('closed')) {
                $('.features-header div').addClass('out');
                $('.features-header span').css('opacity', '1');

                $('.features').animate({height: 0}, 300, function () {
                    $('.features').addClass('closed');
                });
            }
            if ($('.features').hasClass('closed')) {
                $('.features').animate({height: originalFeatHeight}, 300, function () {
                    $('.features').removeClass('closed');
                    $('.features-header div').removeClass('out');
                    $('.features-header span').css('opacity', '0');
                });
            }

        }

    });

})();

/**
 * Hauptklasse: Slider
 */
(function () {

    var allow_triangles = true;
    var menu_points = new Array();

    function Slider(parent) {

        var that = this;

        this.parent = parent;

        this.gender = "";
        if (parent.hasClass('slider_men')) {
            this.gender = 'men';
        } else if (parent.hasClass('slider_women')) {
            this.gender = 'women';
        }

        this.wrapper = parent.find('.slider_wrapper');

        this.frame = this.wrapper.find(".slider_frame");

        this.items = this.wrapper.find('.slider_item');
        this.length = this.items.length;
        this.material = 0;

        this.arrow_prev = this.wrapper.find('.arrow.prev');
        this.arrow_prev.onEnterClick(function () {
            this.prev();
        }.bind(this));

        this.arrow_next = this.wrapper.find('.arrow.next');
        this.arrow_next.onEnterClick(function () {
            this.next();
        }.bind(this));

        this.arrows_nav = this.wrapper.find('.slider_nav_triangle');
        this.arrows_nav.onEnterClick(function () {
            var i = $(this).data('item');
            that.jump(i);
        });

        this.position = 0;
        this.frame.attr('data-pos', this.position);

        if (this.length <= 1) {
            this.arrow_prev.hide();
            this.arrow_next.hide();
        }

        if (this.length === 0) {
            this.wrapper.hide();
        }

        this._nav_update();

        Slider.instances.push(this);

    }

    Slider.instances = [];
    Slider.get_instance_by_gender = function (gender) {
        var ret = null;
        $.each(Slider.instances, function (index, instance) {
            if (instance.gender === gender) {
                ret = instance;
                return false;
            }
        });
        return ret;
    };

    Slider.toggler_ready = function () {
        $.each(Slider.instances, function (index, instance) {
            instance._update_toggler();
        });
    };

    Slider.prototype = {

        _nav_update: function () {
            $('.menu-point[data-image="' + this.position + '"]').addClass('active');
            var arrow;
            var that = this;
            this.arrows_nav.each(function () {
                arrow = $(this);
                if (arrow.data('material') !== that.material) {
                    arrow.hide().removeClass('active');
                } else {
                    arrow.show();
                    if (arrow.data('item') == that.position) {
                        arrow.addClass('active');
                    } else {
                        arrow.removeClass('active');
                    }
                }
            });

        },

        _update_toggler: function () {
            if (typeof fn_toggler.tri_active === 'function') {
                fn_toggler.tri_active(this);
            }
        },

        _update: function (direction) {

            /*	this.frame.css('transform', 'translateX(' + -100 * this.position + '%)');
             this.frame.attr('data-pos', this.position);
             */
            $('.highlight').css('display', 'none');
            $('.highlight[data-image="' + this.position + '"]').css('display', 'block');

            $('.menu-point').removeClass('active');
            $('.menu-point[data-image="' + this.position + '"]').addClass('active');

            if (direction === 'right') {


                $('.slider_item').addClass('transition');

                $('.slider_item[data-item="' + this.current_pos + '"]').removeClass('anim-right').removeClass('anim-left').addClass('pos-pre');
                $('.slider_item[data-item="' + this.position + '"]').addClass('anim-right').removeClass('pos-pre').removeClass('pos-next');


            } else {

                $('.slider_item').addClass('transition')

                $('.slider_item[data-item="' + this.current_pos + '"]').removeClass('anim-right').removeClass('anim-left').addClass('pos-next');
                $('.slider_item[data-item="' + this.position + '"]').addClass('anim-left').removeClass('pos-pre').removeClass('pos-next');
            }


            this.material = $(this.items[this.position]).data('material');
            this._update_toggler();
            this._nav_update();

        },

        _check_pos_value: function () {
            if (this.position > this.length - 1) {
                this.position = 0;
            }
            if (this.position < 0) {
                if (this.length > 1) {
                    this.position = this.length - 1;
                } else {
                    this.position = 0;
                }
            }
        },

        jump_to_material: function (material) {
            var that = this;

            if ($(this.items[this.position]).data('material') === material) {
                return;
            }

            this.items.each(function (index) {
                if ($(this).data('material') === material) {
                    that.jump(index);
                    return false;
                }
            });
        },

        jump: function (pos) {
            if (this.position != pos) {
                this.current_pos = this.position;
                this.position = pos;
                //this._check_pos_value();
                this._update('right');
            }
        },

        next: function () {
            this.current_pos = this.position;
            this.position++;
            this._check_pos_value();
            this._update('right');

        },

        prev: function () {
            this.current_pos = this.position;
            this.position--;
            this._check_pos_value();
            this._update('left');
        },

    };

    var state_gender;

    function fn_toggler() {

        var $slider_men = $('.slider_men');
        var $slider_women = $('.slider_women');

        var $gender_select = $('.gender_select');
        var $men = $('.gender_select .men');
        var $women = $('.gender_select .women');

        var $material_men = $('.material_men');
        var $material_men_tri = $material_men.find('.tri');
        var $material_men_tri_image = $material_men_tri.find('.tri_hover');

        var $material_women = $('.material_women');
        var $material_women_tri = $material_women.find('.tri');
        var $material_women_tri_image = $material_women_tri.find('.tri_hover');

        var slider_instance_men = null;
        var slider_instance_women = null;

        if ($slider_men.length > 0 && $slider_women.length > 0) {
            // Es existieren Materialien für Männer und Frauen. Der Button wird benötigt.
            state_gender = 'men';
            slider_instance_men = Slider.get_instance_by_gender('men');
            slider_instance_women = Slider.get_instance_by_gender('women');

            $gender_select.onEnterClick(gender_toggle);

        } else if ($slider_men.length > 0) {
            // Es existieren nur Materialien für Männer.
            state_gender = "men";
            slider_instance_men = Slider.get_instance_by_gender('men');

        } else if ($slider_women.length > 0) {

            // Es existierten nur Materialien für Frauen.
            state_gender = "women";
            slider_instance_women = Slider.get_instance_by_gender('women');

        } else {
            //In diesem Fall sind entweder keine Materialbilder eingepflegt oder beide Häckchen nicht gesetzt. Rausspringen, um Fehler zu vermeiden.
            return;
        }

        function gender_toggle() {
            $('div[data-item]').removeClass('transition pos-pre anim-right')

            if (state_gender === 'men') {
                state_gender = 'women';
                $gender_select.removeClass('men').addClass('women');
                $slider_men.hide();
                $slider_women.show();
                $material_men.hide();
                $material_women.show();
            } else {
                state_gender = 'men';
                $gender_select.removeClass('women').addClass('men');
                $slider_men.show();
                $slider_women.hide();
                $material_men.show();
                $material_women.hide();
            }

            Slider.toggler_ready();
            pageResize.init(vps);
        }

        $material_men_tri_image.onEnterClick(jump_tri_image);
        $material_women_tri_image.onEnterClick(jump_tri_image);

        function jump_tri_image() {


            if (allow_triangles) {

                var material = $(this).data('material');

                if (state_gender === 'men') {

                    if (slider_instance_men !== null) {
                        slider_instance_men.jump_to_material(material);
                    }

                } else if (state_gender === 'women') {

                    if (slider_instance_women !== null) {
                        slider_instance_women.jump_to_material(material);
                    }

                }

            }

        }


        fn_toggler.tri_active = function (slider_instance) {

            $use = null;

            var material = slider_instance.material;


            if (state_gender === 'men' && slider_instance.gender === 'men') {
                if (slider_instance_men !== null) {
                    $use = $material_men_tri;
                }
            } else if (state_gender === 'women' && slider_instance.gender === 'women') {
                if (slider_instance_women !== null) {
                    $use = $material_women_tri;
                }
            }

            if ($use !== null) {

                $use.each(function (index) {
                    var $tri = $(this);
                    if ($tri.data('material') === material) {
                        $tri.addClass('active');
                    } else {
                        $tri.removeClass('active');
                    }
                });

            }

        };

    }

    function Init() {
        $(".slider").each(function (index, item) {
            new Slider($(this));
        });
        fn_toggler();
        Slider.toggler_ready();
        $('.highlight[data-image!="0"]').css('display', 'none')
    }

    $(Init);

    var selectors = [$('.slider_wrapper .arrow'), $('.material_inner'), $('.slider_nav')];

    $(document).ready(function () {

        $('div[class^="menu-point"]').click(function (e) {

            var pos = $(this).data('image');


            //   $('div[class^="menu-point"]').removeClass('active');
            $('.highlight').removeClass('active');

            var highlight = $(this).attr('data-highlight');
            $('div[class="highlight"][data-highlight="' + highlight + '"]').addClass('active');
            $(this).addClass('active');


            $.each(Slider.instances, function (index, instance) {
                instance.jump(pos);
            });


        });
        $('.gender_select').click(function () {
            $('.highlight').removeClass('active');

            $.each(Slider.instances, function (index, instance) {
                instance.jump(0);
            });
        });
        $('.highlight').hover(function () {
            $('.highlight').removeClass('active');
        })
    });

})();

//Read More Functionality

(function ($) {

    var win = $.win;
    var infoItem;
    var originalText = '';

    /**
     * Initialisiert die Funktionalität der einzelnen Programm-Elemente.
     */
    function fn_programme() {

        /**
         * Diese Klasse dient als Controller für die Programm-Elemente im DOM.
         * @param el
         */
        function Programm_Item(el) {


            this.$el = $(el);
            this.titel = this.$el.attr('data-name');


            //this.$btn_konzept.on("click", this.display_overlay.bind(this));
            //this.$btn_konzept.onEnterClick(this.display_overlay.bind(this));


            this.$beschreibung_div = this.$el.find(".product_description");
            this.$beschreibung_holder = this.$el.find(".inner_wrapper.paragraphs");
            this.$beschreibung_text_div = this.$el.find(".inner_wrapper.paragraphs p");
            this.$beschreibung_text_div_cached = this.$el.find(".inner_wrapper.paragraphs p");
            this.beschreibung_html = this.$el.attr('data-text');
            this.beschreibung_text = this.$beschreibung_text_div.text();

            this.beschreibung_text_cached = this.$beschreibung_text_div.text();
            this.beschreibung_words = this.beschreibung_text.split(" ");

            this.$weiterlesen = $('<span class="weiterlesen">').html("...&nbsp;weiterlesen&nbsp;");

            this.color = $('.product_single').data('layout');

            this.overlay_status = "closed"; // rendering, open
            this.overlay_rendered = false;

            Programm_Item.item_list.push(this);

        }

        Programm_Item.item_list = [];

        Programm_Item.prototype = {

            /**
             * Zeigt das große Overlay für das Programm-Element an.
             */
            show_big_overlay: function () {
                BIGOVERLAY.show(this.titel, this.beschreibung_html, this.color);

            },

            /**
             * Blendet das Standard-Overlay ein.
             */
            display_overlay: function () {

                this.overlay_status = "rendering";

                //this.$btn_konzept.off("click");
                this.$btn_konzept.offEnterClick();
                //this.$btn_close.on("click", this.hide_overlay.bind(this));
                this.$btn_close.onEnterClick(this.hide_overlay.bind(this));
                this.$beschreibung_text_div_cached.onEnterClick(this.show_big_overlay.bind(this));

                this.$overlay.show();

                this.render_beschreibung();

            },

            /**
             * Blendet das Standard-Overlay aus.
             */
            hide_overlay: function () {

                //this.$btn_konzept.on("click", this.display_overlay.bind(this));
                this.$beschreibung_text_div_cached.offEnterClick();

                this.overlay_status = "closed";

            },

            /**
             * Rendert den String der Beschreibung im Standard-Overlay und zeigt bei Überlänge einen "weiterlesen"-Button an, der das große Overlay öffnet.
             */
            render_beschreibung: function () {
                this.overlay_rendered = false;
                this.$beschreibung_text_div = this.$beschreibung_text_div_cached;

                /*fix*/
                originalText = (originalText == '') ? this.beschreibung_text_cached : this.$beschreibung_text_div.text(this.beschreibung_text_cached);
                this.beschreibung_text = this.beschreibung_text_cached;
                this.beschreibung_words = this.beschreibung_text.split(" ");

                if (this.overlay_rendered) {
                    return;
                }

                var _this = this,
                    beschreibung_height = this.$beschreibung_div.height(),
                    words = this.beschreibung_words,
                    current_text = "",
                    tmp_text = null,
                    cut = false;


                function validate_height() {
                    return _this.$beschreibung_holder.height() < beschreibung_height;
                }

                if (!validate_height()) {
                    var i;
                    for (i = 0; i < words.length; i++) {

                        tmp_text = current_text + " " + words[i] + " " + this.$weiterlesen.html();
                        var tmp_text_ohne_weiterlesen = current_text + " " + words[i];
                        this.$beschreibung_text_div.text(tmp_text);

                        if (validate_height()) {
                            current_text = tmp_text_ohne_weiterlesen;
                        } else {
                            this.$beschreibung_text_div.text(tmp_text_ohne_weiterlesen);
                            cut = true;
                            break;
                        }

                    }

                }

                if (!cut) {
                    this.$weiterlesen.remove();
                } else {
                    this.$beschreibung_text_div.html(this.$beschreibung_text_div.html() + '<span class="weiterlesen">...&nbsp;weiterlesen&nbsp;</span>');
                }

                this.overlay_rendered = true;

            }

        };
        /**
         * Selektiert jedes Programm-Element und erzeugt ein Programm_Item-Objekt.
         */
        $('.highlight').each(function () {
            infoItem = new Programm_Item($(this));
            infoItem.render_beschreibung();
            $(this).onEnterClick(infoItem.show_big_overlay.bind(infoItem));

        })
        $(window).resize(function () {
            infoItem.render_beschreibung();
            $('.paragraphs p').onEnterClick(infoItem.show_big_overlay.bind(infoItem));
        });

    }

    function init() {
        fn_programme();
    }

    $(init);

})(jQuery);

