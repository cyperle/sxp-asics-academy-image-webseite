<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulTechOverview extends CyModul {

	public $data = [];
	public $path = "";

	public function __construct() {
		$this->set_module();
		$this->print_module();
	}

	public function set_module() {

		$this->path = parent::$config->uri . '/module/Produktseite/';

		$datenArray                       = [];
		$datenArray['category']           = get_field( 'highlight' );
		$datenArray['program']            = get_field( 'programs' );
		$datenArray['color']              = get_field( 'layout', $datenArray['program'] );
		$datenArray['page_title']         = get_field( 'overview_title' );
		$datenArray['show_product_label'] = get_field( 'show_product_label' );
		$datenArray['color']              = 'silver';
		$datenArray['tech_data']          = Asics::collectTechsByCat( $datenArray['category']->slug);
		if ( empty( $datenArray['show_product_label'] ) ) {
			$datenArray['show_product_label'] = 'Show';
		}

		$this->data = $datenArray;

	}


	public function print_module() {
        $ob = null;
		$has_data = false;

		if ( isset( $this->data['tech_data'] ) ) :

			$has_data = true;

			foreach ( $this->data['tech_data'] as &$data ) :
				ob_start();

				?>

                <li class="product-box">
                    <a href="<?php echo $data['permalink']; ?>">
                        <p class="p-name"><?php if(strlen($data['name'])>10){echo $data['short_name'];}else{echo $data['name'];} ?></p>
                        <p class="p-desc"><?= $data['short_description']; ?></p>
                        <img class="p-thumb" src="<?php echo $data['thumb']['url']; ?>" alt="">
                        <div class="show-p">
                            <span><?php echo $this->data['show_product_label']; ?></span>
                        </div>
                    </a>
                </li>

				<?php
				$ob .= ob_get_clean();
			endforeach;
		endif;

		?>

		<?php if ( ! empty( $this->data['category'] ) ) : ?>

            <div class="product-page <?php echo $this->data['color']; ?>">

                <div class="product-page-bg">

                    <div class="inner_wrapper">

                        <div class="top-triangles"></div>


                        <section class="wrapper">
                            <h2 class="p-headline"></h2>
                            <ul class="products-inner">
								<?= $ob ?>
                            </ul>
                        </section>


						<?php if ( ! $has_data ) : ?>
                            <section class="wrapper text">
								<?php CyTheme::alert( "Es sind keine Produkte mit diesen Kriterien angelegt." ); ?>
                            </section>
						<?php endif; ?>

                        <div class="bottom-triangles">
                            <div class="triangle">
                                <div class="big-t"></div>
                            </div>
                        </div>

                    </div>

                    <div class="left-triangles"></div>

                </div>

            </div>

		<?php else : ?>

            <div class="inner_wrapper">

				<?php

				$error_msg = '';
				if ( empty( $this->data['program'] ) && empty( $this->data['category'] ) ) {
					$error_msg = 'Diese Seite vom Typ "Product Overview" ist keinem Programm und keiner Kategorie zugewiesen oder  die bestehenden Relationen sind gelöscht.';
				} else if ( empty( $this->data['program'] ) ) {
					$error_msg = 'Diese Seite vom Typ "Product Overview" ist keinem Programm zugewiesen oder die bestehende Relation sind gelöscht.';
				} else if ( empty( $this->data['category'] ) ) {
					$error_msg = 'Diese Seite vom Typ "Product Overview" ist keiner Kategorie zugewiesen oder die bestehende Relation sind gelöscht.';
				}

				CyTheme::alert( $error_msg );

				?>

            </div>

		<?php endif; ?>

		<?php

	}

}

new ModulTechOverview();

?>