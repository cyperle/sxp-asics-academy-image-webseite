<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulHeader extends CyModul
{

	public $data = [];

	public function __construct()
	{
		$this->set_module();
		$this->create_css();
		$this->print_module();
	}

	public function set_module()
	{
		$datenArray = [];
		$datenArray['picture_desktop'] = get_sub_field('picture_desktop');
		$datenArray['picture_tablet'] = get_sub_field('picture_tablet');
		$datenArray['picture_phone'] = get_sub_field('picture_phone');
		$this->data = $datenArray;
		//CyTheme::Printt($datenArray);
	}

	private function create_css()
	{

		$style = '<style data-info="dynamic-header-styles">';

		if (!empty($this->data['picture_desktop'])) {
			$style .= '.header_bg{background-image: url("' . $this->data['picture_desktop'] . '")}';
		}
		if (!empty($this->data['picture_tablet'])) {
			$style .= '.vp-960 .header_bg{background-image: url("' . $this->data['picture_tablet'] . '")}';
		}
		if (!empty($this->data['picture_phone'])) {
			$style .= '.vp-640 .header_bg{background-image: url("' . $this->data['picture_phone'] . '");
	            background-size: cover;
    background-position: center;}';
		}
		if (empty($this->data['picture_phone']) && empty($this->data['picture_tablet']) && empty($this->data['picture_desktop'])) {
			$style .= '.header_wrapper{display:none;height:0}';
		}

		$style .= '</style>';

		//parent::add_html($style);

		echo $style;

	}

	public function print_module()
	{

		?>

		<div class="header_wrapper">
			<div class="para-triangle-left"></div>
			<div class="header_inner inner_wrapper">

				<div class="header_bg"></div>

				<div class="header_dreieck1_clone"></div>
				<div class="header_dreieck1"></div>
				<div class="header_dreieck2_clone"></div>
				<div class="header_dreieck2"></div>

				<div class="header_space">
					<?php //@AsicsToggleSeason::btn("Go to", "FS 2018", Asics::getLang(), "asics_fruehjahrsommer2018");
					?>
				</div>

				<div class="border-horicontal"></div>

			</div>

		</div>

		<?php

	}

}

new ModulTeaserSpecial();