/**
 * Created by aberezinets on 29.06.2017.
 */
$(document).ready(function () {
    var cushioning = $('.cushioning');
    var structuredCushioning = $('.structured-cushioning');
    var cushioningContainer = $('.wrapper:nth-of-type(2)');
    var structuredCushioningContainer = $('.wrapper:nth-of-type(1)');

    structuredCushioning.click(function () {
        $(this).toggleClass('active');

        if($(this).hasClass('active')){
            cushioningContainer.animate({opacity:0},500,function(){$(this).css('display','none')});
        }

        if(cushioning.hasClass('active')){

            cushioning.removeClass('active');

            structuredCushioningContainer.css('display','block');
            structuredCushioningContainer.animate({opacity:1},500);
        }

        checkBoth();

    });
    cushioning.click(function () {
        $(this).toggleClass('active');

        if($(this).hasClass('active')){
            structuredCushioningContainer.animate({opacity:0},500,function(){$(this).css('display','none')});
        }

        if(structuredCushioning.hasClass('active')){

            structuredCushioning.removeClass('active');

            cushioningContainer.css('display','block');
            cushioningContainer.animate({opacity:1},500);
        }
        checkBoth();
    });

    function checkBoth() {
        if(!cushioning.hasClass('active') && !structuredCushioning.hasClass('active')){
            cushioningContainer.css('display','block');
            cushioningContainer.animate({opacity:1},500);
            structuredCushioningContainer.css('display','block');
            structuredCushioningContainer.animate({opacity:1},500);

        }
    }
});