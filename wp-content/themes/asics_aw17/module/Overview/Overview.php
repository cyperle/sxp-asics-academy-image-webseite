<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulProduktseite extends CyModul
{

    public $data = [];
    public $path = "";

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {

        $datenArray = [];

        $datenArray['category'] = get_field('categories');
        $datenArray['program'] = get_field('programs');
        $datenArray['color'] = get_field('layout', $datenArray['program']);
        $datenArray['page_title'] = get_field('overview_title');
        $datenArray['show_product_label'] = get_field('show_product_label');
        if (empty($datenArray['show_product_label'])) {
            $datenArray['show_product_label'] = 'Show';
        }

        if (empty($datenArray['color'])) {
            $datenArray['color'] = 'lightblue';
        }

        //Lade alle Produkte, die derselben Kategorie und demselben Programm angehören
        $SiteIds = $this->getSiteIds($datenArray['category'], get_field('programs'));

        $count2 = 0;

        for ($i = 0; $i < count($SiteIds); $i++) {
            if (have_rows('stage', $SiteIds[$i])) {
                while (have_rows('stage', $SiteIds[$i])): the_row();

                    if (get_row_layout() == 'stage_layout'):

                        $datenArray['teaserList'][$count2]['page_id'] = $SiteIds[$i];
                        $datenArray['teaserList'][$count2]['state'] = get_post_status($SiteIds[$i]);
                        $datenArray['teaserList'][$count2]['title'] = get_the_title($SiteIds[$i]);
                        $datenArray['teaserList'][$count2]['name'] = get_sub_field('name');
                        $datenArray['teaserList'][$count2]['men_women'] = get_sub_field('men_women');
                        $datenArray['teaserList'][$count2]['type'] = get_sub_field('type');
                        $datenArray['teaserList'][$count2]['permalink'] = get_permalink($SiteIds[$i]);
                        $product_image_set = false;

                        if ($datenArray['teaserList'][$count2]['men_women'][0] == 'Men') {
                            $mat = get_sub_field('material_varianten_1_men');
                            if (!empty($mat)) {
                                if (!empty($mat[0]['product_image'])) {
                                    $product_image_set = true;
                                    $datenArray['teaserList'][$count2]['product_image'] = $mat[0]['product_image'];
                                }
                            }
                        } else if ($datenArray['teaserList'][$count2]['men_women'][0] == 'Women') {
                            $mat = get_sub_field('material_varianten_1_women');
                            if (!empty($mat)) {
                                if (!empty($mat[0]['product_image'])) {
                                    $product_image_set = true;
                                    $datenArray['teaserList'][$count2]['product_image'] = $mat[0]['product_image'];
                                }
                            }
                        }

                        if (!$product_image_set) {
                            $datenArray['teaserList'][$count2]['product_image'] = false;
                        }
                        $count2++;
                    endif;
                endwhile;
            }
        }

        if (!empty($datenArray['teaserList'])) {
            usort($datenArray['teaserList'], function ($a, $b) {
                return strcmp($a['title'], $b['title']);
            });
        }

        //CyTheme::printt($datenArray['teaserList']);

        $this->data = $datenArray;

    }

    private function getSiteIds($compare_category, $compare_program)
    {

        $result = [];

        $args = [
            'post_type' => 'page',
            'nopaging' => true,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_wp_page_template',
                    'value' => 'page-product-single.php',
                    'compare' => '='
                ),
                array(
                    'key' => 'categories',
                    'value' => $compare_category->term_id,
                    'compare' => '='
                ),
                array(
                    'key' => 'programs',
                    'value' => $compare_program->term_id,
                    'compare' => '='
                )
            )
        ];

        $pages = get_posts($args);

        foreach ($pages as $page) :

            //Prüfe, ob das Produkt der aktuellen Season zugeordnet ist
            if (!Asics::isInSeason(get_field("season", $page))) continue;

            //Prüfe, ob das Produkt aktiv ist
            if (!get_field("stage_0_status", $page) === "Active") continue;

            //Prüfe, ob das Produkt in der derzeitigen Sprache verfügbar ist
            $lan = apply_filters('wpml_post_language_details', null, $page->ID);
            if ($lan['language_code'] != Asics::getLang()) continue;

            //Wenn alle Tests bestanden, dann füge der Liste hinzu
            $result[] = $page;

        endforeach;

        return $result;

    }

    private function getSiteIdsOld($compare_category, $compare_program)
    {

        $page_ids = get_all_page_ids();

        $idArray = [];

        foreach ($page_ids as $page_id):

            if (have_rows('stage', $page_id)) {
                while (have_rows('stage', $page_id)): the_row();
                    $status = get_sub_field('status', $page_id);
                endwhile;
            }

            $template = get_page_template_slug($page_id);
            $category = get_field('categories', $page_id);
            $program = get_field('programs', $page_id);

            $lan = apply_filters('wpml_post_language_details', null, $page_id);

            if (
                $lan['language_code'] == Asics::getLang()
                && $template == "page-product-single.php"
                && $category
                && $program
                && $status == 'Active'
            ) {
                if ($category->term_id == $compare_category->term_id && $program->term_id == $compare_program->term_id) {
                    array_push($idArray, $page_id);
                }
            }

        endforeach;

        return $idArray;

    }

    public function print_module()
    {

        $structured_cushioning = '';
        $cushioning = '';
        $no_cushioning = '';
        $has_data = false;

        if (isset($this->data['teaserList'])) :

            $has_data = true;
            $allowed_states = ['publish'];

            if (is_user_logged_in()) {
                //array_push($allowed_states, 'draft');
            }

            foreach ($this->data['teaserList'] as &$data) :

                $is_draft = false;
                if (!in_array($data['state'], $allowed_states)) {
                    continue;
                } else {
                    if ($data['state'] === 'draft') {
                        $is_draft = true;
                    }
                }

                $gender_class = "";
                if (is_array($data['men_women'])) {
                    if (in_array('Men', $data['men_women'])) {
                        $gender_class .= "m";
                    }
                    if (in_array('Women', $data['men_women'])) {
                        $gender_class .= "w";
                    }
                }

                if ($data['type'] === "Cushioning") {
                    $stradd = &$cushioning;
                } else if ($data["type"] === "Structured Cushioning") {
                    $stradd = &$structured_cushioning;
                } else {
                    $stradd = &$no_cushioning;
                }

                $stradd .= <<<html
<li class="product-box">
    <a href="{$data['permalink']}">
        <p class="p-name">{$data['name']}</p>
        <img class="p-thumb" src="{$data['product_image']}">
        <div class="show-p {$gender_class}">
            <span>{$this->data['show_product_label']}</span>
        </div>
    </a>
</li>
html;

            endforeach;

        endif;

        ?>

        <?php

        $product_page_color_class = "test";
        if (!empty($this->data['color'])) {
            $product_page_color_class = $this->data['color'];
        }

        ?>

        <?php if (!empty($this->data['program']) && !empty($this->data['category'])) : ?>

        <div class="product-page <?php echo CyTheme::mapLayout($product_page_color_class); ?>">

            <div class="product-page-bg">

                <div class="inner_wrapper">

                    <div class="top-triangles"></div>

                    <?php if ($structured_cushioning !== '' && $has_data) : ?>
                        <div class="selector">
                            <div class="structured-cushioning active">
                                <span>Structured Cushioning</span>
                            </div>
                            <div class="cushioning">
                                <span>Cushioning</span>
                            </div>
                        </div>
                        <section class="wrapper">
                            <h2 class="p-headline">Structured Cushioning</h2>
                            <ul class="products-inner">
                                <?php echo $structured_cushioning; ?>
                            </ul>
                        </section>
                    <?php endif; ?>

                    <?php if ($cushioning !== '' && $has_data) : ?>
                        <section class="wrapper" style="display: none;">
                            <h2 class="p-headline">Cushioning</h2>
                            <ul class="products-inner">
                                <?php echo $cushioning; ?>
                            </ul>
                        </section>
                    <?php endif; ?>

                    <?php if ($no_cushioning !== '' && $has_data) : ?>
                        <section class="wrapper">
                            <h2 class="p-headline"></h2>
                            <ul class="products-inner">
                                <?php echo $no_cushioning; ?>
                            </ul>
                        </section>
                    <?php endif; ?>

                    <?php if (!$has_data) : ?>
                        <section class="wrapper text">
                            <?php CyTheme::alert("Es sind keine Produkte mit diesen Kriterien angelegt."); ?>
                        </section>
                    <?php endif; ?>

                    <div class="bottom-triangles">
                        <div class="triangle">
                            <div class="big-t"></div>
                        </div>
                    </div>

                </div>

                <div class="left-triangles"></div>

            </div>

        </div>

    <?php else : ?>

        <div class="inner_wrapper">

            <?php

            $error_msg = '';
            if (empty($this->data['program']) && empty($this->data['category'])) {
                $error_msg = 'Diese Seite vom Typ "Product Overview" ist keinem Programm und keiner Kategorie zugewiesen oder  die bestehenden Relationen sind gelöscht.';
            } else if (empty($this->data['program'])) {
                $error_msg = 'Diese Seite vom Typ "Product Overview" ist keinem Programm zugewiesen oder die bestehende Relation sind gelöscht.';
            } else if (empty($this->data['category'])) {
                $error_msg = 'Diese Seite vom Typ "Product Overview" ist keiner Kategorie zugewiesen oder die bestehende Relation sind gelöscht.';
            }

            CyTheme::alert($error_msg);

            ?>

        </div>

    <?php endif; ?>

        <?php
        parent::add_script(get_template_directory_uri() . '/module/Overview/scripts/Overview.js');

    }

}

new ModulProduktseite();

?>