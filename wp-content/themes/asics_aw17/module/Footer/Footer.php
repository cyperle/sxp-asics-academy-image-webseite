<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulFooter extends CyModul
{

    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {
        $this->data['has_imprint'] = Asics::getImprint() !== null;
        $this->data['has_data_protection'] = Asics::getPrivacy() !== null;
    }

    public function print_module()
    {

        ?>

        <footer class="footer_wrapper">

            <div class="footer_inner inner_wrapper clearfix">
                <div>
                    <div class="footer_logo">
                        <img
                                class="footer_logo_src"
                                src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
                    </div>

                    <div class="footer_copyright">
                        <p>© 2016 ASICS Europe</p>
                    </div>
                </div>
                <nav class="footer_nav">

                    <ul>

                        <?php if ($this->data['has_imprint']) : ?>

                            <li class="footer_nav_item">
                                <a target="_blank" href="<?php echo Asics::getImprint()['permalink']; ?>"
                                   title="<?php echo Asics::getImprint()['title']; ?>">
                                    <?php echo Asics::getImprint()['title']; ?>
                                </a>
                            </li>

                        <?php endif; ?>

                        <?php if ($this->data['has_data_protection']) : ?>

                            <li class="footer_nav_item">
                                <a target="_blank" href="<?php echo Asics::getPrivacy()['permalink']; ?>"
                                   title="<?php echo Asics::getPrivacy()['title']; ?>">
                                    <?php echo Asics::getPrivacy()['title']; ?>
                                </a>
                            </li>

                        <?php endif; ?>

                    </ul>

                </nav>

                <div class="border-horicontal"></div>

            </div>

        </footer>

        <?php

    }

}

new ModulFooter();