(function ($) {

    var $offcanvas_nav_wrapper;
    var $offcanvas_nav_close;
    var $topnav_nav_search;
    var $topnav_nav_burger;
    var $lang_list;
    var $search;

    var is_open = false
    var min_width = 570;
    var $html = $('html');
    var rem;

    /**
     * Reguliert die Handler zum Öffnen und Schließen der Navigation.
     */
    function fn_nav_handler() {

        $topnav_nav_burger.onEnterClick(open_nav);
        $offcanvas_nav_close.onEnterClick(close_nav);
        $offcanvas_nav_wrapper.hide();


        function open_nav() {

            if (!is_open) {
                TweenMax.fromTo($offcanvas_nav_wrapper, .5, {
                    x: "100%",
                    display: "block"
                }, {
                    x: "0%",

                });

                is_open = true;
                $offcanvas_nav_wrapper.addClass('opened');

                on_resize();

            }

        }

        function close_nav() {

            if (is_open) {
                TweenMax.fromTo($offcanvas_nav_wrapper, .5, {
                    x: "0%"
                }, {
                    x: "100%",
                    display: "none"
                });

                is_open = false;
                $offcanvas_nav_wrapper.removeClass('opened');

            }


        }

        function on_resize() {

            if (is_open) {

                rem = parseFloat($html.css('font-size'));
                var mwidth = min_width / 16 * rem;

                var adjust_on = [$topnav_nav_burger, $topnav_nav_search];

                var offset_left, width, height;

                for (var i = 0; i < adjust_on.length; i++) {
                    offset_left = adjust_on[i].offset().left;
                    width = $.win.width() - offset_left;
                    if (width >= mwidth) {
                        break;
                    }
                }

                if (width <= mwidth) {
                    if (mwidth > $.win.width()) {
                        width = $.win.width();
                    } else {
                        width = mwidth;
                    }
                }

                height = ($('.site_wrapper').height() - $('.offcanvas_nav_top').height() - $('.footer_wrapper').height());
                $offcanvas_nav_wrapper.width(width);
                $offcanvas_nav_wrapper.height(height);

            }

        }

        TweenMax.set($offcanvas_nav_wrapper, {
            x: "100%"
        });

        $(window).on("pageresize", on_resize);
        on_resize();

    }


    /**
     * Bedienung der Sprachauswahl.
     */
    function fn_lang() {

        if ($lang_list.length < 1) return;

        var state = 0;

        var $lang_item_link = $('.lang_item_link');
        var $lang_item_0 = $('.lang_item.i_0');

        $lang_item_link.on('click', function (e) {
            e.stopPropagation();
        });

        $lang_item_link.on('focus', on_focus);
        $lang_item_link.on('blur', on_blur);

        $lang_item_0.onEnterClick(on_toggle);

        function on_toggle(e) {
            e.stopPropagation();
            if (state === 0) {
                on_focus();
            } else {
                on_blur();
            }
        }

        function on_focus() {
            if (state === 0) {
                state = 1;
                $lang_list.addClass('open');
                click_outside_listener();
            }
        }

        function on_blur() {
            if (state === 1) {
                state = 0;
                $lang_list.removeClass('open');
            }
        }

        function click_outside_listener() {
            $.doc.on("click.click_outside_listener", function (e) {
                on_blur();
                $.doc.off("click.click_outside_listener");
            });
        }

    }


    /**
     * Suche
     */

    function fn_search() {
        $search.find('.topbar_nav_item_value').onEnterClick(open_search);
        $search.find('.topbar_nav_item_close').onEnterClick(close_search);

        $('#search').click(function (e) {
            e.stopPropagation();
        })
        $('html').click(close_search);

        $('#search-input').keyup(sbm_search)
        $('#search-form').submit(function (e) {
            e.preventDefault();
            sbm_search()
        })

        function open_search() {
            $('#search').attr('active', 'true');
            $('.navIcons').fadeOut(200);
            // setTimeout(function(){$('.navIcons').fadeOut();},300)
        }

        function close_search() {
            $('#search').attr('active', 'false');
            $('#search-result ul').html('')
            //  $('.navIcons').fadeIn()
            setTimeout(function () {
                $('.navIcons').fadeIn();
            }, 200)
            //$('.navIcons').hide().delay(2000).show();
        }

        function sbm_search() {
            var action = $('#search-form').attr('action');

            $.get(action, {"s": $('#search-input').val()}).done(function (msg) {
                var result = JSON.parse(msg);
                if (result.length > 0) {
                    var htmlString = ''

                    $.each(result, function (index, value) {
                        console.log(value);

                        htmlString += '<li class="search-result-item">' +
                            '<a href="' + value.link + '">' +
                            '<div><img src="' + value.product_image + '"></div>' +
                            '<p>' + value.name + '</p>' +
                            '<span class="clearfix"></span>'
                        '</a>'
                        '</li>'

                    })

                    $('#search-result ul').html(htmlString)
                } else {

                }
            })
        }
    }


    function init() {

        $offcanvas_nav_wrapper = $(".offcanvas_nav_wrapper");
        $offcanvas_nav_close = $(".offcanvas_nav_close");
        $topnav_nav_search = $(".topnav_nav_search");
        $topnav_nav_burger = $(".topnav_nav_burger");
        $lang_list = $(".lang_list");
        $search = $('#search');


        fn_nav_handler();
        fn_lang();
        fn_search();

    }

    $(init);

    function onElementHeightChange(elm, callback) {
        var lastHeight = elm.clientHeight, newHeight;
        (function run() {
            newHeight = elm.clientHeight;
            if (lastHeight != newHeight)
                callback();
            lastHeight = newHeight;

            if (elm.onElementHeightChangeTimer)
                clearTimeout(elm.onElementHeightChangeTimer);

            elm.onElementHeightChangeTimer = setTimeout(run, 200);
        })();
    }


    onElementHeightChange(document.body, function () {
        $offcanvas_nav_wrapper.height(($('.site_wrapper').height() - $('.offcanvas_nav_top').height() - $('.footer_wrapper').height()));
    });

})(jQuery);