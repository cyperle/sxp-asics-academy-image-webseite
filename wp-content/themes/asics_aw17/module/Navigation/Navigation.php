<?php

class ModulNavigation extends CyModul
{

    public $data = [];
    public $breadhtml = "";

    public function __construct()
    {

        $this->set_module();
        $this->print_module();

    }

    public function set_module()
    {
        $this->determine_breadcrumbs();
    }

    private function init_cat($key)
    {

        return Asics::getHomeUrl() . '?' . parent::$init_cat_key . '=' . $this->lowercase($key);
    }

    private function determine_breadcrumbs()
    {

        $breadhtml = "";
        $breaddata = [];
        @ $techTranslation = Asics::getTechTranslation(get_field('highlight')->slug);

        if (get_page_template_slug() == 'page-product-overview.php') {

            $cat = get_field('categories');
            $prog = get_field('programs');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $breaddata['program'] = Array(
                'name' => $prog->name,
                'permalink' => Asics::getTechOverviewUrl($cat->slug, $prog->slug)
            );

            if (!empty($cat) && !empty($prog)) {

                $breaddata['layout'] = CyTheme::mapLayout(get_field('layout', $prog));

                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . '</span></a></div>';

                $breadhtml .= '<div class="bread_item bread-last ' . $breaddata['layout'] . '"><span>' . $breaddata['program']['name'] . '</span></div>';

            }


        }
        if (get_page_template_slug() == 'page-tech-overview.php') {

            $cat = get_field('categories');
            $prog = get_field('type');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $breaddata['program'] = Array(
                'name' => $techTranslation['headline'],
            );

            if (!empty($cat) && !empty($prog)) {

                $breaddata['layout'] = 'silver';

                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . '</span></a></div>';

                $breadhtml .= '<div class="bread_item bread-last ' . $breaddata['layout'] . '"><span>' . $breaddata['program']['name'] . '</span></div>';

            }

        }

        if (get_page_template_slug() == 'page-product-single.php') {

            $cat = get_field('categories');
            $prog = get_field('programs');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $breaddata['program'] = Array(
                'name' => $prog->name,
                'permalink' => Asics::getOverviewUrl($cat->slug, $prog->slug)
            );

            $breaddata['layout'] = CyTheme::mapLayout(get_field('layout', $prog));

            $stage = get_field('stage');
            if ($stage[0]['name']) {
                $name = $stage[0]['name'];
            } else {
                $name = null;
            }

            $breaddata['product'] = Array(
                'name' => $name
            );


            if (!empty($breaddata['category']['name'])) {
                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . '</span></a></div>';
            }

            if (!empty($breaddata['program']['name'])) {
                $breadhtml .= '<div class="bread_item bread_program"><a href="' . $breaddata['program']['permalink'] . '"><span>' . $breaddata['program']['name'] . '</span></a></div>';
            }

            if (!empty($breaddata['product']['name'])) {
                $breadhtml .= '<div class="bread_item bread-last ' . $breaddata['layout'] . '"><span>' . $breaddata['product']['name'] . '</span></div>';
            }

        }

        if (get_page_template_slug() == 'page-technology.php') {

            $cat = get_field('categories');
            $highlight = get_field('highlight');
            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->slug),
                'permalink' => $this->init_cat($cat->name)
            );

            Asics::collectTechOverviews();
            $breaddata['program'] = Array(
                'name' => $techTranslation['headline'],
                'permalink' => Asics::getTechOverviewUrl($highlight->slug)
            );

            $breaddata['layout'] = 'silver';

            $name = get_field('name');
            if (strlen($name) > 10) {
                $name = get_field('short_name');
            }
            $breaddata['techname'] = $name;
            $breaddata['product'] = Array(
                'name' => $name
            );


            if (!empty($breaddata['category']['name'])) {
                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . '</span></a></div>';
            }

            if (!empty($breaddata['program']['name'])) {
                $breadhtml .= '<div class="bread_item bread_program"><a href="' . $breaddata['program']['permalink'] . '"><span>' . $breaddata['program']['name'] . '</span></a></div>';
            }

            if (!empty($breaddata['product']['name'])) {
                $breadhtml .= '<div class="bread_item bread-last ' . $breaddata['layout'] . '"><span>' . $breaddata['product']['name'] . '</span></div>';
            }

        }

        if ($breadhtml !== "") {
            $breadhtml = '<div class="breadcrumbs"><div class="inner_wrapper">' . $breadhtml . '</div></div>';
        }
        $this->data['translations'] = $techTranslation;
        $this->breadhtml = $breadhtml;
        $this->data['breadcrumbs'] = $breaddata;

    }

    public function print_module()
    {

        ?>
        <div class="topbar_wrapper">

            <header class="topbar_inner inner_wrapper">

                <a class="topbar_logo" href=" <?php echo Asics::getHomeUrl(); ?>" title="Zur Asics-Startseite">
                    <img class="topbar_logo_src" src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
                </a>

                <ul class="topbar_nav">

                    <li class="topnav_nav_search topbar_nav_item search">
                        <div id="search">
                            <div class="unskew">
                                <img class="topbar_nav_item_value"
                                     src="<?php echo get_template_directory_uri(); ?>/images/Navigation/search.png">

                                <div id="search_mobile_wrap">
                                    <div id="search_form_wrap">
                                        <?php
                                        echo get_search_form();
                                        ?>
                                    </div>
                                    <img class="topbar_nav_item_close"
                                         src="<?php echo get_template_directory_uri(); ?>/images/Navigation/close.png">
                                </div>

                            </div>
                            <div id="search-result">
                                <ul>

                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="topnav_nav_burger topbar_nav_item">
                        <div class="unskew">
                            <img class="topbar_nav_item_value"
                                 src="<?php echo get_template_directory_uri(); ?>/images/Navigation/burger.png">
                        </div>
                    </li>

                </ul>
                <ul class="topbar_nav navIcons">
                    <?php $cat = get_terms('sociallinks', 'orderby=count&hide_empty=0');
                    //print_r($cat);
                    ?>

                    <li class="topnav_nav_burger topbar_nav_item instagramIcon">
                        <a href="<?php echo $cat[1]->description; ?>" target="_blank">
                            <img class="topbar_nav_item_value"
                                 src="<?php echo get_template_directory_uri(); ?>/images/Navigation/instagram.png">
                        </a>
                    </li>
                    <li class="topnav_nav_burger topbar_nav_item facebookIcon">
                        <a href="<?php echo $cat[0]->description; ?>" target="_blank">
                            <img class="topbar_nav_item_value"
                                 src="<?php echo get_template_directory_uri(); ?>/images/Navigation/facebook.png">
                        </a>
                    </li>

                </ul>
            </header>

            <section class="offcanvas_nav_wrapper">

                <div class="offcanvas_nav_top">
                    <div class="unskew">
                        <div class="offcanvas_nav_top_inner">

                            <?php if (Asics::getLangs() !== null) : ?>
                                <ul class="lang_list">
                                    <?php
                                    $i = 0;
                                    $l = count(Asics::getLangs());
                                    foreach (Asics::getLangs() as &$lang) : ?>

                                        <li class="lang_item i_<?php echo $i; ?> <?php if ($l > 1) {
                                            echo 'has_selection';
                                        } ?>">

                                            <?php if ($i > 0) {
                                                echo '<a href="' . $lang['url'] . '" class="lang_item_link" title="' . $lang['native_name'] . '">';
                                            } ?>

                                            <div class="lang_item_holder">
                                                <span class="country_flag"
                                                      style="background:url('<?php echo $lang['country_flag_url']; ?>')"></span>
                                                <span class="lang_text">
													<?php echo $lang['tag']; ?>
												</span>
                                            </div>

                                            <?php if ($i > 0) {
                                                echo '</a>';
                                            } ?>

                                        </li>

                                        <?php

                                        $i++;
                                    endforeach;
                                    ?>
                                </ul>
                            <?php endif; ?>

                            <?php if (class_exists("AsicsToggleSeason")) : ?>

                                <div class="season_list">
                                    <div class="season_item" <?php echo AsicsToggleSeason::data("SS 2018"); ?>>
                                        <div class="season_item_holder">
                                            <div class="season_icon">
                                                <?php AsicsToggleSeason::echoSvg(); ?>
                                            </div>
                                            <div class="season_text">
                                                <?php AsicsToggleSeason::echoLabel("SS 2018", Asics::getLang()); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>

                            <div class="offcanvas_nav_close"></div>

                        </div>
                    </div>
                </div>

                <div class="offcanvas_nav_content">

                    <?php

                    $error_at_overviews = false;
                    $my_type = "";
                    $my_cat = "";
                    $my_prog = "";
                    if (!empty($this->data['breadcrumbs']['program']['name'])) {
                        $my_prog = $this->data['breadcrumbs']['program']['name'];
                        $my_cat = $this->data['breadcrumbs']['category']['name'];
                        if (isset($this->data['breadcrumbs']['type'])) {
                            $my_type = $this->data['breadcrumbs']['type']['name'];
                        }
                    }

                    CyTheme::collectTechs();
                    $techs = CyTheme::$techs;

                    foreach (Asics::getOverviews() as &$category) :
                        if ($category['name'] != ''):
                            ?>

                            <div class="offcanvas_nav_section">

                                <div class="offcanvas_nav_kategorie_holder">
                                    <div class="offcanvas_nav_kategorie <?php if ($my_cat == $category['name']) {
                                        echo 'active';
                                    } ?>"><?php echo $category['name']; ?></div>
                                </div>

                                <div class="offcanvas_nav_programme">

                                    <ul class="offcanvas_nav_programme_list clearfix">

                                        <?php foreach ($category['programs'] as &$program) : ?>

                                            <li>
                                                <a class="<?php echo CyTheme::mapLayout($program['layout']);
                                                if ($my_prog == $program['name']) {
                                                    echo ' active';
                                                } ?>"
                                                   href="<?php echo $program['permalink']; ?>">
                                                    <span><?php echo $program['name']; ?></span>
                                                </a>
                                            </li>

                                        <?php endforeach; ?>


                                    </ul>

                                </div>

                            </div>

                            <?php
                        endif;
                    endforeach;
                    ?>

                    <!--TECH HIGHLIGHTS-->
                    <?php if (count($techs) > 0): ?>
                        <div class="offcanvas_nav_section">

                            <div class="offcanvas_nav_kategorie_holder">
                                <div class="offcanvas_nav_kategorie <?php if ($my_cat == $category['name'] && (get_page_template_slug() == 'page-tech-overview.php' || get_page_template_slug() == 'page-technology.php')) {
                                    echo 'active';
                                } ?>"><?= __('Technology Highlights', 'CyTheme@Asics'); ?>
                                </div>
                            </div>


                            <div class="offcanvas_nav_programme">

                                <ul class="offcanvas_nav_programme_list clearfix">

                                    <?php foreach ($techs as &$tech) :

                                        ?>
                                        <li>
                                            <a class="<?= 'silver';
                                            if ($this->data['breadcrumbs']['techname'] == $tech['name']) {
                                                echo ' active';
                                            } ?>"
                                               href="<?= $tech['permalink']; ?>">
                                                <span><?= $tech['name']; ?></span>
                                            </a>
                                        </li>

                                    <?php endforeach; ?>

                                </ul>

                            </div>

                        </div>
                    <?php endif; ?>
                </div>

            </section>

        </div>

        <?php echo $this->breadhtml; ?>

        <?php
        parent::add_script(get_template_directory_uri() . "/module/Navigation/scripts/Navigation.js");

    }

}

new ModulNavigation();