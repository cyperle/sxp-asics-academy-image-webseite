<?php

class ModulProduct extends CyModul
{

    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->print_module();

    }

    public function set_module()
    {

        $datenArray = [];

        /* Get General Data */
        $datenArray['program'] = get_field('programs');
        $datenArray['rel_category'] = get_field('categories');
        if (!empty($datenArray['program']) && !empty($datenArray['rel_category'])) {
            $datenArray['related_overview'] = Asics::getOverviewUrl($datenArray['rel_category']->slug, $datenArray['program']->slug);
        } else {
            $datenArray['related_overview'] = null;
        }
        $datenArray['layout'] = 'grey';

        /* Header Content */
        $datenArray['name'] = get_field('name');
        $datenArray['short_name'] = get_field('short_name');
        $datenArray['description'] = get_field('description');

        /* Get Asset URLs as Array */
        $i = 0;
        foreach (get_field('assets') as $key => $asset) {


            $datenArray['slideshow_assets'][$i]['video'] = ($asset['imagevideo'] == 'Video') ? true : false;
            $datenArray['slideshow_assets'][$i]['src'] = ($asset['imagevideo'] == 'Video') ?
                array($asset['video_desktop'], $asset['video_tablet'], $asset['video_mobile'])
                : array($asset['image_desktop']['url'], $asset['image_tablet']['url'], $asset['image_mobile']['url']);
            $i++;
        }

        /* How it works Data (hiw) */
        $datenArray['hiw_headline'] = get_field('hiw_headline');
        $datenArray['hiw_open_label'] = get_field('hiw_open_label');

        $datenArray['hiw_content'] = get_field('hiw_content');
        //echo get_field( 'hiw_content' );
        /*echo '<pre>';
            print_r(get_field( 'hiw_content' ));*/
        /* Get How it works Content as Array */
        foreach (get_field('hiw_content') as $key => $content) {
            $datenArray['hiw_content'][$key] = $content;
        }

        /* What it does Data (wid) */
        $datenArray['wid_headline'] = get_field('wid_headline');
        $datenArray['wid_open_label'] = get_field('wid_open_label');

        $datenArray['wid_content'] = get_field('wid_content');


        $this->data = $datenArray;
    }

    public function print_module()
    {

        ?>
        <script>
            var readmore = '<?=__('read more', 'CyTheme@Asics');?>';
        </script>
        <section id="tech" class="product_single <?php echo $this->data['layout']; ?>"
                 data-layout="<?php echo $this->data['layout']; ?>">

            <!-- Flap -->
            <div class="usn flap">
                <?php if (!empty($this->data['program'])) : ?>
                    <span>
                    <?php echo $this->data['program']->name ?>
                </span>
                <?php endif; ?>
                <?php if ($this->data['related_overview'] !== null) : ?>
                    <p>
                        <a href="<?php echo $this->data['related_overview']; ?>">
                            <?php echo $this->data['stage']['overview_label'] ?>
                        </a>
                    </p>
                <?php endif; ?>
            </div>


            <div class="product_top ">

                <!-- Beschreibung -->


                <!-- Slider -->
                <?php

                $nav_triangles = "";
                $i = 0;

                ?>

                <section class="slider slider_men" data-gender="men">

                    <div class="slider_wrapper">
                        <div class="slider_holder">
                            <div class="slider_gallery">

                                <div class="slider_frame">
                                    <?php
                                    $i = 0;

                                    foreach ($this->data['slideshow_assets'] as $key => $value) {
                                        if ($i == 0) {
                                            $active = ' active';
                                        } else {
                                            $active = '';
                                        }
                                        $nav_triangles .= '<div class="slider_nav_triangle' . $active . '" data-item="' . $i . '" data-material="men"></div>';

                                        if (!$value['video']) {


                                            echo "<div
                                                        class=\"slider_item\"
                                                        data-item=\"" . $i . "\"
                                                        data-material=\"men\"
                                                        data-desktop=\"" . str_replace(get_home_url(), '..', $value['src'][0]) . "\"
                                                        data-tablet=\"" . str_replace(get_home_url(), '..', $value['src'][1]) . "\"
                                                        data-mobile=\"" . str_replace(get_home_url(), '..', $value['src'][2]) . "\" style=\"background: url('" . str_replace(get_home_url(), '..', $value['src'][0]) . "') center center / cover\">
                                                </div>";

                                        } else if (strpos($value['src'][0], ".mp4") != 0) {
                                            echo "<div
                                                        class=\"slider_item\"
                                                        data-item=\"" . $i . "\"
                                                        data-material=\"men\"
                                                        data-desktop=\"" . $value['src'][0] . "\"
                                                        data-tablet=\"" . $value['src'][1] . "\"
                                                        data-mobile=\"" . $value['src'][2] . "\" style=\"background: url('" . $value['src'][0] . "') center center / cover\">
                                                <video>
                                                      <source src=\"" . $value['src'][0] . "\" type=\"video/mp4\">
                                                      <source src=\"" . $value['src'][0] . "\" type=\"video/mp4\">
                                                      <source src=\"" . $value['src'][0] . "\" type=\"video/mp4\">
                                                </video></div>";
                                        } else {
                                            echo "<div
                                                        class=\"slider_item\"
                                                        data-item=\"" . $i . "\"
                                                        data-material=\"men\"
                                                        data-desktop=\"" . $value['src'][0] . "\"
                                                        data-tablet=\"" . $value['src'][1] . "\"
                                                        data-mobile=\"" . $value['src'][2] . "\" style=\"background: url('" . $value['src'][0] . "') center center / cover\">
                                               <iframe src='https://player.vimeo.com/video" . substr($value['src'][0], strrpos($value['src'][0], '/'), strlen($value['src'][0])) . "?title=0&byline=0&portrait=0' width='640' height='360' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                </div>";
                                        }
                                        $i++;
                                    };
                                    ?>

                                </div>
                            </div>
                            <div class="slider_arrows">
                                <div class="arrow prev"></div>
                                <div class="arrow next"></div>
                            </div>
                        </div>

                        <div class="slider_nav">
                            <?php echo $nav_triangles; ?>
                        </div>

                    </div>

                </section>

                <section class="product_description first-teaser">

                    <div class="inner_wrapper paragraphs">
                        <h2>
                            <?php echo strip_tags(apply_filters('the_content', $this->data['name'])); ?>
                        </h2>
                        <p>
                            <?php echo strip_tags(apply_filters('the_content', $this->data['description'])); ?>
                        </p>
                    </div>
                </section>

            </div>


            <section>

                <header class="tech-header">
                    <h2><?php echo $this->data['hiw_headline']; ?></h2>
                    <span><?php echo $this->data['hiw_open_label']; ?><img
                                src="<?= get_template_directory_uri() ?>/images/Product_v200/pointer_arrow.png"
                                alt="V"></span>
                    <div></div>
                </header>

                <div class="tech-bg">
                    <div class="tech">
                        <div class="inner_wrapper clearfix">

                            <?php
                            $set = false;
                            foreach ($this->data['hiw_content'] as $key => $val) {

                                if ($set) {
                                    echo '<div class="trenner"></div>';
                                }
                                $set = true;

                                if ($val['heading'] != '') {
                                    echo '<div class="heading content-split"><h2>' . str_replace([
                                            '<p>',
                                            '</p>',
                                            'font-size: 20px;'
                                        ], '', $val['heading']) . '</h2></div>';
                                }
                                $html = [];
                                if ($val['content_layout'] == 'Image+Text') {

                                    $html[] = '<div class="content-split">' .
                                        '<img src="' . $val['image'] . '">' .
                                        '</div>';

                                    $html[] = '<div class="content-split">' .
                                        $val['text_field_1'] .
                                        '</div>';


                                } elseif ($val['content_layout'] == 'Text+Text') {


                                    $html[] =
                                        '<div class="content-split">' .
                                        $val['text_field_1'] .
                                        '</div>';


                                    $html[] =
                                        '<div class="content-split">' .
                                        $val['text_field_2'] .
                                        '</div>';


                                } else {

                                }
                                if ($val['content_mirror'] == 'Yes') {
                                    $html = array_reverse($html);
                                }


                                echo '<div>' . implode('', $html) . '</div>';
                            }
                            ?>

                        </div>
                        <div class="close-tech"></div>
                    </div>
                </div>

            </section>

            <section>

                <header class="features-header">
                    <h2><?php echo $this->data['wid_headline']; ?></h2>
                    <span><?php echo $this->data['wid_open_label']; ?><img
                                src="<?php echo get_template_directory_uri(); ?>/images/Product_v200/pointer_arrow.png"
                                alt="V"></span>
                    <div></div>
                </header>

                <div class="features">
                    <div class="inner_wrapper clearfix">

                        <?php
                        $set = false;
                        foreach ($this->data['wid_content'] as $key => $val) {

                            if ($set) {
                                echo '<div class="trenner"></div>';
                            }
                            $set = true;

                            if ($val['heading'] != '') {
                                echo '<div class="heading content-split"><h2>' . str_replace([
                                        '<p>',
                                        '</p>',
                                        'font-size: 20px;'
                                    ], '', $val['heading']) . '</h2></div>';
                            }
                            $html = [];
                            if ($val['content_layout'] == 'Image+Text') {

                                $html[] = '<div class="content-split">' .
                                    '<img src="' . $val['image'] . '">' .
                                    '</div>';

                                $html[] = '<div class="content-split">' .
                                    $val['text_field_1'] .
                                    '</div>';


                            } elseif ($val['content_layout'] == 'Text+Text') {


                                $html[] =
                                    '<div class="content-split">' .
                                    $val['text_field_1'] .
                                    '</div>';


                                $html[] =
                                    '<div class="content-split">' .
                                    $val['text_field_2'] .
                                    '</div>';


                            } else {

                            }
                            if ($val['content_mirror'] == 'Yes') {
                                $html = array_reverse($html);
                            }


                            echo '<div>' . implode('', $html) . '</div>';
                        }
                        ?>

                    </div>
                    <div class="close-features"></div>
                </div>

            </section>

        </section>

        <?php

        parent::add_script(get_template_directory_uri() . '/module/TechHighlights/scripts/TechHighlights.js');

    }

}

new ModulProduct();

?>