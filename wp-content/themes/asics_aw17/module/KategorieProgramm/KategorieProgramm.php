<?php

/*
Hierarchie Programme:

Kategorie (z.B. Running)
	Typ (shoes, apparel)
		Programm (road, fast, easy, trail)

Beispielhafter Aufbau des Arrays im Attribut $data ist in der example.txt hinterlegt.

 */

class ModulKategorieProgramm extends CyModul
{

    private static $programm_item_count = -1;
    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {

        Asics::collectTechOverviews();
        $datenArray = [];

        $count = 0;

        $datenArray['label_for_shoes'] = get_sub_field('label_for_shoes');
        $datenArray['label_for_apparel'] = get_sub_field('label_for_aparel');

        if (isset($_GET[parent::$init_cat_key])) {
            $datenArray['icat'] = $_GET[parent::$init_cat_key];
        } else {
            $datenArray['icat'] = 'none';
        }

        $datenArray['kategorien'] = [];
        $kategorien = &$datenArray['kategorien'];

        while (have_rows('categorie_navigation')): the_row();

            $current_cat = get_sub_field('categorie_name');
            $current_cat_name = $current_cat->name;
            $current_cat_slug = $current_cat->slug;

            if (empty($current_cat_name)) {
                continue;
            }

            $kategorien[$current_cat_name] = [];
            $current_cat_data = &$kategorien[$current_cat_name];
            $current_cat_data['kategoriename'] = $current_cat_name;
            $current_cat_data['slug'] = $current_cat_slug;
            $current_cat_data['teaser_desktop'] = get_sub_field('teaser_desktop');
            $current_cat_data['teaser_tablet'] = get_sub_field('teaser_tablet');
            $current_cat_data['teaser_mobile'] = get_sub_field('teaser_mobile');

            $count2 = 0;

            while (have_rows('programm_field_shoes')): the_row();
                //CyTheme::printt(get_sub_field('acf_fc_layout'));
                if (get_row_layout() == 'programm'):
                    $current_prog = get_sub_field('name');
                    $current_cat_data['type_shoes'][$count2]['program'] = $current_prog;
                    $current_cat_data['type_shoes'][$count2]['permalink'] = Asics::getOverviewUrl($current_cat->slug, $current_prog->slug);
                    $current_cat_data['type_shoes'][$count2]['layout'] = get_field('layout', $current_cat_data['type_shoes'][$count2]['program']);
                    $current_cat_data['type_shoes'][$count2]['programm_label'] = get_sub_field('programm_label');
                    $current_cat_data['type_shoes'][$count2]['concept_label'] = get_sub_field('concept_label');
                    $current_cat_data['type_shoes'][$count2]['concept'] = get_sub_field('concept', false);
                    $current_cat_data['type_shoes'][$count2]['picture_desktop'] = get_sub_field('picture_desktop');
                    $current_cat_data['type_shoes'][$count2]['picture_tablet'] = get_sub_field('picture_tablet');
                    $current_cat_data['type_shoes'][$count2]['picture_phone'] = get_sub_field('picture_phone');
                endif;
                if (get_row_layout() == 'highlight'):
                    $current_highlight = get_sub_field('tax');
                    if (isset($current_highlight->slug)) {
                        $current_cat_data['type_shoes'][$count2]['slug'] = $current_highlight->slug;
                    }
                    $current_cat_data['type_shoes'][$count2]['headline'] = get_sub_field('headline');
                    $current_cat_data['type_shoes'][$count2]['subheadline'] = get_sub_field('subheadline');
                    $current_cat_data['type_shoes'][$count2]['picture_desktop'] = get_sub_field('picture_desktop');
                    $current_cat_data['type_shoes'][$count2]['picture_tablet'] = get_sub_field('picture_tablet');
                    $current_cat_data['type_shoes'][$count2]['picture_phone'] = get_sub_field('picture_phone');
                    $current_cat_data['type_shoes'][$count2]['type'] = 'highlight';
                    if (!get_sub_field('link_type') == 'Taxonomy') {
                        $current_cat_data['type_shoes'][$count2]['site_url'] = get_sub_field('page');
                    } else {
                        if (isset($current_highlight->slug)) {
                            $current_cat_data['type_shoes'][$count2]['site_url'] = Asics::getTechOverviewUrl($current_highlight->slug);
                        }
                    }
                endif;
                $count2++;
            endwhile;


            $count2 = 0;

            while (have_rows('programm_field_apparel')): the_row();

                if (get_row_layout() == 'programm'):
                    $current_prog = get_sub_field('name');
                    $current_cat_data['type_apparel'][$count2]['program'] = $current_prog;
                    $current_cat_data['type_apparel'][$count2]['permalink'] = Asics::getOverviewUrl($current_cat->slug, $current_prog->slug);
                    $current_cat_data['type_apparel'][$count2]['layout'] = get_field('layout', $current_cat_data['type_shoes'][$count2]['program']);
                    $current_cat_data['type_apparel'][$count2]['programm_label'] = get_sub_field('programm_label');
                    $current_cat_data['type_apparel'][$count2]['concept_label'] = get_sub_field('concept_label');
                    $current_cat_data['type_apparel'][$count2]['concept'] = get_sub_field('concept', false);
                    $current_cat_data['type_apparel'][$count2]['picture_desktop'] = get_sub_field('picture_desktop');
                    $current_cat_data['type_apparel'][$count2]['picture_tablet'] = get_sub_field('picture_tablet');
                    $current_cat_data['type_apparel'][$count2]['picture_phone'] = get_sub_field('picture_phone');
                endif;
                if (get_row_layout() == 'highlight'):
                    $current_highlight = get_sub_field('tax');
                    if (isset($current_highlight->slug)) {
                        $current_cat_data['type_apparel'][$count2]['slug'] = $current_highlight->slug;
                    }
                    $current_cat_data['type_apparel'][$count2]['headline'] = get_sub_field('headline');
                    $current_cat_data['type_apparel'][$count2]['subheadline'] = get_sub_field('subheadline');
                    $current_cat_data['type_apparel'][$count2]['picture_desktop'] = get_sub_field('picture_desktop');
                    $current_cat_data['type_apparel'][$count2]['picture_tablet'] = get_sub_field('picture_tablet');
                    $current_cat_data['type_apparel'][$count2]['picture_phone'] = get_sub_field('picture_phone');
                    $current_cat_data['type_apparel'][$count2]['type'] = 'highlight';
                    if (!get_sub_field('link_type') == 'Taxonomy') {
                        $current_cat_data['type_apparel'][$count2]['site_url'] = get_sub_field('page');
                    } else {
                        if (isset($current_highlight->slug)) {
                            $current_cat_data['type_apparel'][$count2]['site_url'] = Asics::getTechOverviewUrl($current_highlight->slug);
                        }
                    }
                endif;
                $count2++;
            endwhile;

            $count++;

        endwhile;

        $this->data = $datenArray;

    }

    /**
     * Erhöht den Zähler für alle jemals erzeugten Programm-Items, um jedem Item eine unique ID vergeben zu können.
     * @return int Der neue Wert.
     */
    private function increase_programm_item()
    {
        return ++self::$programm_item_count;
    }

    /**
     * Wandelt einen String in lowercase um und ersetzt alle Whitespaces durch Underscores.
     */
    /*private function lowercase($str) {
        return str_replace(" ", "_", strtolower($str));
    }*/

    /**
     * Erzeugt CSS für das Hintergrundbild eines Programm-Items anhand der Daten aus dem Feed.
     */
    private function create_unique_item_css($programm_data, $unique_classname)
    {

        $desk = $programm_data['picture_desktop'];
        $mob = $programm_data['picture_phone'];

        $css =
            '.' . $unique_classname . ' .bg {
				background-image: url(' . $desk . ');
			}';

        $css .=
            '.vp-960 .' . $unique_classname . ' .bg {
				background-image: url(' . $mob . ');
				opacity: 1;
				background-position:center !important;
			}';

        $css .=
            '.' . $unique_classname . ' .text_name {
				font-size: ' . $programm_data['programm_name_em'] . 'em;
			}';

        $css .=
            '.' . $unique_classname . ' .text_program {
				font-size: ' . $programm_data['programm_label_em'] . 'em;
			}';

        $css .=
            '.' . $unique_classname . ' .text_concept {
				font-size: ' . $programm_data['programm_concept_label_em'] . 'em;
			}';

        parent::add_css($css);

    }


    /**
     * Konvertiert ein Array aus String zu einem CSS-Klassennamen.
     */
    private function to_classname($array)
    {
        return implode(" ", $array);
    }

    /**
     * Erzeugt die Liste der Programme für ein Apparel (z.B. Schuhe).
     *
     * @param $from_data
     */
    private function print_programme($kategorie_data)
    {

        $types = [];

        foreach ($kategorie_data as $key => $value) {
            if (strpos($key, "type_") !== false) {
                $types[$key] = $value;
            }
        }

        foreach ($types as $type_key => $type_value) :

            $ul_classname = 'nav_programm_list';
            $ul_attr_typ = str_replace("type_", "", strtolower($type_key));

            ?>

            <ul class="<?php echo $ul_classname; ?>" data-typ="<?php echo $ul_attr_typ; ?>">

                <?php

                $programm_data_count = -1;

                foreach ($type_value as $programm_data) :

                    $programmType = "";
                    if (isset($programm_data['type'])) {
                        $programmType = $programm_data['type'];
                    }

                    if ($programmType != 'highlight'):

                        $programm_data_count++;
                        $unique_classname = 'nav_programm_item_' . $this->increase_programm_item();

                        $programm_link = $programm_data["permalink"];

                        $programm_name = $programm_data["program"]->name;;
                        $programm_name_em = 5 / strlen(utf8_decode($programm_name));
                        if ($programm_name_em > 1) {
                            $programm_name_em = 1;
                        }
                        $programm_data["programm_name_em"] = $programm_name_em;

                        $programm_label = $programm_data["programm_label"];
                        if (strlen($programm_label) == 0) {
                            $programm_label = "Program";
                        }
                        $programm_label_em = 7 / strlen(utf8_decode($programm_label));
                        if ($programm_label_em > 1) {
                            $programm_label_em = 1;
                        }
                        $programm_data["programm_label_em"] = $programm_label_em;

                        $programm_concept_label = $programm_data["concept_label"];
                        if (strlen($programm_concept_label) != 0) {
                            $programm_concept_label_em = 7 / strlen(utf8_decode($programm_concept_label));
                            if ($programm_concept_label_em > 1) {
                                $programm_concept_label_em = 1;
                            }
                        } else {
                            $programm_concept_label_em = 1;
                        }

                        $programm_data["programm_concept_label_em"] = $programm_concept_label_em;

                        $programm_concept = $programm_data["concept"];

                        $programm_classname = ["nav_programm_item", $unique_classname, "small"];

                        // Ausrichtungsklasse bestimmen
                        if ($programm_data_count % 2 == 0) {
                            array_push($programm_classname, "left");
                        } else {
                            array_push($programm_classname, "right");
                        }

                        array_push(
                            $programm_classname,
                            CyTheme::mapLayout($programm_data["layout"])
                        );

                        ?>


                        <li class="<?php echo $this->to_classname($programm_classname); ?>"
                            data-programm="<?php echo $this->lowercase($programm_name); ?>">

                            <?php $this->create_unique_item_css($programm_data, $unique_classname); ?>

                            <div class="bg"></div>

                            <div class="inner">
                                <div class="holder_name">
                                    <div class="text_name"><?php echo $programm_name; ?></div>
                                </div>
                                <div class="verlauf">
                                    <?php if (!empty($programm_link)) : ?>
                                        <a href="<?php echo $programm_link; ?>" class="btn_programm">
                                            <div class="text_program">
                                                <?php echo $programm_label; ?>
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if ($programm_concept_label != ''): ?>
                                        <div class="btn_konzept">
                                            <div class="text_concept">
                                                <?php echo $programm_concept_label; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="overlay_konzept">
                                <div class="overlay_konzept_inner">
                                    <div class="overlay_konzept_titel"><?php echo $programm_name; ?></div>
                                    <div class="overlay_konzept_beschreibung">
                                        <div class="overlay_konzept_beschreibung_holder">
									<span class="overlay_konzept_beschreibung_text">
										<?php echo htmlentities($programm_concept); ?>
									</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="overlay_konzept_close"></div>
                            </div>
                        </li>

                    <?php elseif (isset($programm_data['slug'])): ?>

                        <li class="tech <?= $programm_data['slug'] ?>">
                            <style type="text/css">
                                .tech.<?=$programm_data['slug']?> {
                                    background-image: url('<?=$programm_data['picture_desktop']?>');
                                }

                                .vp-960 .tech.<?=$programm_data['slug']?> {
                                    background-image: url('<?=$programm_data['picture_tablet']?>');
                                }

                                .vp-640 .tech.<?=$programm_data['slug']?> {
                                    background-image: url('<?=$programm_data['picture_phone']?>');
                                    background-size: cover;
                                }
                            </style>
                            <a href="<?= $programm_data['site_url'] ?>">
                                <div class="tech_container">
                            <span class="headline">
							<?= $programm_data['headline'] ?>
                            </span>
                                    <span class="subtitle">
							<?= $programm_data['subheadline'] ?>
                            </span>
                                </div>
                            </a>
                        </li>

                    <?php endif; endforeach; ?>
            </ul>

        <?php endforeach;

    }

    public function print_module()
    {

        ?>

        <section class="auswahl_bereich" data-itype="shoes" data-icat="<?php echo $this->data['icat']; ?>">

            <?php if (count($this->data['kategorien']) == 0) : ?>

                <div class="inner_wrapper">
                    <?php CyTheme::alert("Es ist keine Kategorie angelegt oder zuvor bestehende wurden gelöscht."); ?>
                </div>

            <?php else : ?>

                <section class="nav_kategorie clearfix">
                    <div class="inner_wrapper">
                        <ul class="nav_kategorie_list">

                            <?php foreach ($this->data['kategorien'] as $kat_name => &$kategorie) : ?>

                                <?php if (empty($kat_name)) {
                                    continue;
                                } ?>
                                <li class="nav_kategorie_item"
                                    data-kategorie="<?php echo $kategorie['slug']; ?>">
								<span class="nav_kategorie_item_text">
									<?php echo $kat_name; ?>
								</span>
                                </li>

                            <?php endforeach; ?>

                        </ul>

                        <div class="border-horicontal"></div>

                    </div>
                    <div class="triangle-overlay"></div>
                </section>
                <div class="multi-cat">
                    <?php $number = 0;
                    $blacklist_shoes = '';
                    $blacklist_apparel = '';
                    foreach ($this->data['kategorien'] as $key => $kat): $number++ ?>
                        <style>
                            .nav_typ_startscreen.<?=$kat['slug']?> {
                                background-image: url(<?=$kat['teaser_desktop']?>);
                            }

                            .vp-960 .nav_typ_startscreen.<?=$kat['slug']?> {
                                background-image: url(<?=$kat['teaser_tablet']?>);
                            }

                            .vp-640 .nav_typ_startscreen.<?=$kat['slug']?> {
                                background-image: url(<?=$kat['teaser_mobile']?>);
                            }
                        </style>
                        <section class="nav_typ_startscreen <?php echo $kat['slug'];
                        if ($number == 1) {
                            echo ' show';
                        } ?>">
                            <div class="inner_wrapper">
                                <div class="startstreen_btn hover_apparel <?php if (count($kat['type_apparel']) == 0) {
                                    echo 'disabled';
                                    $blacklist_apparel .= $kat['slug'] . ' ';
                                } ?>" data-typ="apparel">
                                    <div class="start_apparel"><span><?= $this->data['label_for_apparel'] ?></span>
                                    </div>
                                </div>
                                <div class="startstreen_btn hover_shoes <?php if (count($kat['type_shoes']) == 0) {
                                    echo 'disabled';
                                    $blacklist_shoes .= $kat['slug'] . ' ';
                                } ?>" data-typ="shoes">
                                    <div class="start_shoes"><span><?= $this->data['label_for_shoes'] ?></span></div>
                                </div>
                            </div>
                        </section>
                    <?php endforeach; ?>
                </div>

                <section class="nav_typ clearfix">

                    <div class="inner_wrapper">

                        <ul class="nav_typ_list">

                            <li class="nav_typ_item" data-typ="apparel"
                                data-hidden="<?= $blacklist_apparel; ?>">
                                <div class="bg"></div>
                                <div class="nav_typ_item_text">
                                    <span><?= $this->data['label_for_apparel'] ?></span>
                                </div>
                            </li>

                            <li class="nav_typ_item" data-typ="shoes" data-hidden="<?= $blacklist_shoes; ?>">
                                <div class="bg"></div>
                                <div class="nav_typ_item_text">
                                    <span><?= $this->data['label_for_shoes'] ?></span>
                                </div>
                            </li>

                        </ul>

                        <div class="border-horicontal"></div>

                    </div>

                    <div class="border-horicontal"></div>
                </section>

                <?php foreach ($this->data["kategorien"] as $kategorie_name => $kategorie_data) : ?>

                    <section class="nav_programm clearfix"
                             data-kategorie="<?php echo $kategorie_data['slug']; ?>">
                        <?php $this->print_programme($kategorie_data); ?>
                    </section>

                <?php endforeach; ?>

            <?php endif; ?>

        </section>

        <script>
            var readmore = '<?=__('read more', 'CyTheme@Asics');?>';
        </script>

        <?php

        parent::add_script(get_template_directory_uri() . '/module/KategorieProgramm/scripts/KategorieProgramm.js');

    }

}

new ModulKategorieProgramm();