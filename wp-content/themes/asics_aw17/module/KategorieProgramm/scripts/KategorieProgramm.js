(function ($) {

    var win = $.win;
    var pItems = new Array();

    /**
     * Zentriert die Elemente in der Kategorieauswahl mit Float oder flex-box.
     */
    function fn_kategorien_flex() {

        var nav_kategorie_list = $(".nav_kategorie_list");
        var items = $(".nav_kategorie_item");

        var count = items.length;
        var width = 100 / count + "%";

        items.css({
            display: "block",
            margin: "0",
            float: "left",
            width: width
        });

    }

    /**
     * Initialisiert die Funktionalität der einzelnen Programm-Elemente.
     */
    function fn_programme() {

        /**
         * Verfügbare Farben der Programme. Dies wird benötigt, um die Farbe des großen Overlays zu bestimmen.
         * @type {[*]}
         */
        var available_colors = ["dunkelblau", "dunkelgruen", "hellblau", "hellgruen"];

        /**
         * Diese Klasse dient als Controller für die Programm-Elemente im DOM.
         * @param el
         */
        function Programm_Item(el) {

            this.$el = $(el);
            this.titel = this.$el.find(".text_name").text();
            this.$overlay = this.$el.find(".overlay_konzept");
            this.$btn_konzept = this.$el.find(".btn_konzept");
            this.$btn_close = this.$el.find(".overlay_konzept_close");
            this.$verlauf = this.$el.find(".verlauf");
            this.$name = this.$el.find(".holder_name");

            //this.$btn_konzept.on("click", this.display_overlay.bind(this));
            this.$btn_konzept.onEnterClick(this.display_overlay.bind(this));

            this.$beschreibung_div = this.$el.find(".overlay_konzept_beschreibung");
            this.$beschreibung_holder = this.$el.find(".overlay_konzept_beschreibung_holder");
            this.$beschreibung_text_div = this.$el.find(".overlay_konzept_beschreibung_text");

            this.beschreibung_html = this.$beschreibung_text_div.html();
            this.beschreibung_text = this.$beschreibung_text_div.text();
            this.beschreibung_words = this.beschreibung_text.split(" ");
            this.$weiterlesen = $('<span class="weiterlesen">').html("...&nbsp;" + readmore + "&nbsp;");

            this.color = this.$el.determineClass(available_colors);
            this.side = this.$el.determineClass(["left", "right"]);

            this.overlay_status = "closed"; // rendering, open
            this.overlay_rendered = false;

            Programm_Item.item_list.push(this);

        }


        Programm_Item.item_list = [];

        Programm_Item.prototype = {

            /**
             * Zeigt das große Overlay für das Programm-Element an.
             */
            show_big_overlay: function () {

                BIGOVERLAY.show(this.titel, this.beschreibung_html, this.color);

            },

            /**
             * Blendet das Standard-Overlay ein.
             */
            display_overlay: function () {

                this.overlay_status = "rendering";

                //this.$btn_konzept.off("click");
                this.$btn_konzept.offEnterClick();
                //this.$btn_close.on("click", this.hide_overlay.bind(this));
                this.$btn_close.onEnterClick(this.hide_overlay.bind(this));
                this.$weiterlesen.onEnterClick(this.show_big_overlay.bind(this));

                this.$overlay.show();

                if (TweenMax) {

                    if (this.side === "left") {

                        TweenMax.fromTo(this.$overlay, 1, {}, {
                            x: "0%",
                            opacity: 1
                        });
                        TweenMax.fromTo(this.$verlauf, 1, {}, {
                            x: "-200%",
                            opacity: 0
                        });
                        TweenMax.fromTo(this.$name, 1, {}, {
                            opacity: "0"
                        });

                    } else if (this.side === "right") {

                        TweenMax.fromTo(this.$overlay, 1, {}, {
                            x: "0%",
                            opacity: 1
                        });
                        TweenMax.fromTo(this.$verlauf, 1, {}, {
                            x: "200%",
                            opacity: 0
                        });
                        TweenMax.fromTo(this.$name, 1, {}, {
                            opacity: "0"
                        });
                    }

                } else {

                    this.$overlay.css({
                        transform: "translateX(0%)",
                        opacity: 1
                    });

                }

                this.render_beschreibung();

                $.each(Programm_Item.item_list, function () {
                    if (this.overlay_status === "open") {
                        this.hide_overlay();
                    }
                });

                this.overlay_status = "open";

            },

            /**
             * Blendet das Standard-Overlay aus.
             */
            hide_overlay: function () {
                //this.$btn_konzept.on("click", this.display_overlay.bind(this));
                this.$btn_konzept.onEnterClick(this.display_overlay.bind(this));
                this.$btn_close.offEnterClick();
                this.$weiterlesen.offEnterClick();

                if (TweenMax) {

                    if (this.side === "left") {

                        TweenMax.fromTo(this.$overlay, 1, {}, {
                            x: "-100%",
                            opacity: 0
                        });
                        TweenMax.fromTo(this.$verlauf, 1, {}, {
                            x: "0%",
                            opacity: 1
                        });
                        TweenMax.fromTo(this.$name, 1, {}, {
                            opacity: "1"
                        });

                    } else if (this.side === "right") {

                        TweenMax.fromTo(this.$overlay, 1, {}, {
                            x: "100%",
                            opacity: 0
                        });
                        TweenMax.fromTo(this.$verlauf, 1, {}, {
                            x: "0%",
                            opacity: 1
                        });
                        TweenMax.fromTo(this.$name, 1, {}, {
                            opacity: "1"
                        });

                    }

                } else {

                    this.$overlay.hide().css({
                        transform: "translateX(100%)",
                        opacity: 0
                    })

                }

                this.overlay_status = "closed";

            },

            /**
             * Rendert den String der Beschreibung im Standard-Overlay und zeigt bei Überlänge einen "weiterlesen"-Button an, der das große Overlay öffnet.
             */
            render_beschreibung: function () {
                this.overlay_rendered = false;

                this.$beschreibung_text_div.text(this.beschreibung_text);
                if (this.overlay_rendered) {
                    return;
                }

                var _this = this,
                    beschreibung_height = this.$beschreibung_div.height(),
                    words = this.beschreibung_words,
                    current_text = "",
                    tmp_text = null,
                    cut = false;

                this.$beschreibung_holder.append(this.$weiterlesen);


                function validate_height() {
                    return _this.$beschreibung_holder.height() < beschreibung_height;
                }


                if (!validate_height()) {

                    var i;
                    for (i = 0; i < words.length; i++) {

                        tmp_text = current_text + " " + words[i];
                        this.$beschreibung_text_div.text(tmp_text);

                        if (validate_height()) {
                            current_text = tmp_text;
                        } else {
                            this.$beschreibung_text_div.text(current_text);
                            cut = true;
                            break;
                        }

                    }

                }

                if (!cut) {
                    this.$weiterlesen.remove();
                } else {

                }

                this.overlay_rendered = true;

            }

        };


        /**
         * Selektiert jedes Programm-Element und erzeugt ein Programm_Item-Objekt.
         */
        $(".nav_programm_item").each(function (index, el) {
            pItems.push(new Programm_Item(el));

        });

        $("#nav_programm").addClass("anzahl-" + Programm_Item.item_list.length);

        $(window).resize(function () {
            resize_start();
            $.each(pItems, function (key, value) {
                value.render_beschreibung();
            });
        });
    }

    /**
     * Zählt die Anzahl der Programme in einer Liste und fügt eine entsprechende Klasse hinzu.
     */
    function fn_child_count() {
        $(".nav_programm_list").each(function (index, element) {
            var _this = $(this);
            var children = _this.children().length;
            _this.addClass("anzahl-" + children);
        });
    }

    /**
     * Initialisiert die Logik zum Wechsel zwischen Kategorien und Typen.
     */
    function fn_switcher() {

        var initial_typ = '';
        var initial_kategorie = '';

        var current_typ = null;
        var current_kategorie = null;

        /**
         * Typen (Shoes, Apparel)
         */
        function Typ(name, active) {
            this.name = name;
            this.active = active;
            this.startbtn = $('.startstreen_btn[data-typ=' + name + ']');
            this.navbtn = $('.nav_typ_item[data-typ=' + name + ']');
            if (active) {

                this.navbtn.onEnterClick(this.show.bind(this));
                this.startbtn.onEnterClick(this.show.bind(this));
            } else {
                this.startbtn.addClass('disabled');
                this.navbtn.addClass('disabled');
            }
            Typ.instances.push(this);
        }

        Typ.instances = [];

        Typ.show = function (name) {
            console.log('Nama' + name);
            $.each(Typ.instances, function (index, instance) {
                if (instance.name === name) {
                    if (current_typ === null) {
                        $('.nav_typ_startscreen').removeClass('show');
                        $('.nav_typ').addClass('show');
                    }

                    $('.multi-cat').css('display', 'none');

                    current_typ = name;
                    instance.navbtn.addClass('active');
                    instance.navbtn.offEnterClick();
                } else {

                    instance.navbtn.removeClass('active');
                    if (instance.active) {
                        instance.navbtn.onEnterClick(instance.show.bind(instance));
                    }
                }
            });
            if (current_kategorie !== null) {
                current_kategorie.show();
            }
        };

        Typ.prototype = {
            show: function () {
                Typ.show(this.name);
            }
        };

        /**
         * Kategorien
         */
        function Kategorie(btn) {
            this.btn = btn;
            this.name = this.btn.data('kategorie'); //zb Running
            if (this.name == "") {
                return;
            }
            this.programmShoes = new ProgrammTyp('shoes', this.name);
            this.programmApparels = new ProgrammTyp('apparel', this.name);
            this.btn.onEnterClick(this.show.bind(this));
            Kategorie.instances.push(this);
        }

        Kategorie.show = function (name) {
            $.each(Kategorie.instances, function (index, instance) {
                if (instance.name === name) {

                    instance.btn.addClass('active');
                    if (current_typ === 'shoes') {
                        this.programmShoes.items.addClass('show');
                        this.programmApparels.items.removeClass('show');
                    } else if (current_typ === 'apparel') {
                        this.programmApparels.items.addClass('show');
                        this.programmShoes.items.removeClass('show');
                    }

                    if(-1 === $('.nav_typ_item[data-typ="shoes"]').data('hidden').search(name)){
                        $('.nav_typ_item[data-typ="shoes"]').removeClass('hide_nav');
                    } else{
                        $('.nav_typ_item[data-typ="shoes"]').addClass('hide_nav');
                    }

                    if(-1 === $('.nav_typ_item[data-typ="apparel"]').data('hidden').search(name)){
                        $('.nav_typ_item[data-typ="apparel"]').removeClass('hide_nav');
                    } else{
                        $('.nav_typ_item[data-typ="apparel"]').addClass('hide_nav');
                    }

                    if (current_kategorie !== null) {
                        if (current_kategorie.name == name) {
                            $('.multi-cat').css('display', 'none');
                            $('.nav_typ').addClass('show');
                        } else {
                            $('.multi-cat').css('display', 'block');
                            if (current_typ === 'shoes') {
                                this.programmShoes.items.removeClass('show');
                                this.programmApparels.items.removeClass('show');
                            } else if (current_typ === 'apparel') {
                                this.programmApparels.items.removeClass('show');
                                this.programmShoes.items.removeClass('show');
                            }

                            $('.nav_typ').removeClass('show');
                        }
                    }
                    current_kategorie = instance;
                    $('.nav_typ_startscreen.' + instance.name).addClass('show')
                } else {
                    instance.btn.removeClass('active');
                    this.programmShoes.items.removeClass('show');
                    this.programmApparels.items.removeClass('show');
                    $('.nav_typ_startscreen.' + instance.name).removeClass('show')
                }
            });
        };

        Kategorie.instances = [];

        Kategorie.prototype = {
            show: function () {
                Kategorie.show(this.name);
            }
        };

        function ProgrammTyp(name, category) {
            if (name == "") {
                return;
            }
            if (category == "") {
                return;
            }
            this.name = name;
            this.holder = $('.nav_programm[data-kategorie=' + category + ']');
            this.items = this.holder.find('.nav_programm_list[data-typ=' + name + ']');
        }

        /**
         * Init Switcher
         */
        function Init() {

            var $auswahl_bereich = $('.auswahl_bereich');
            initial_kategorie = $auswahl_bereich.data('icat');
            initial_typ = $auswahl_bereich.data('itype');

            new Typ('apparel', true);
            new Typ('shoes', true);

            $('.nav_kategorie_item').each(function (index, btn) {
                new Kategorie($(btn));
            });

            var instance_found = false

            if (initial_kategorie !== 'none') {
                $.each(Kategorie.instances, function (index, instance) {
                    if (instance.name === initial_kategorie) {
                        instance.show();
                        instance_found = true;
                        Typ.show(initial_typ);
                    }
                });
            }

            if (!instance_found) {
                if (Kategorie.instances.length > 0) {
                    Kategorie.instances[0].show();
                }
            }

            //Typ.show('shoes');

        }

        Init();

    }

    function init() {
        fn_kategorien_flex();
        fn_programme();
        fn_child_count();
        fn_switcher();
    }


    var shoes_menu = $('.start_shoes');
    var apparel_menu = $('.start_apparel');
    var shoes_text = $('.start_shoes span');
    var apparel_text = $('.start_apparel span');
    var script_end = false;
    var resize_start = function () {
        script_end = false;
        if (script_end == false) {

            shoes_menu.css('font-size', '4.25rem')
            apparel_menu.css('font-size', '4.25rem')

            var font_size_apparel = parseFloat(apparel_menu.css('font-size'))
            var font_size_shoes = parseFloat(shoes_menu.css('font-size'))

            if (shoes_menu.width() < shoes_text.width()) {
                while (shoes_menu.width() < shoes_text.width()) {
                    font_size_shoes--;
                    shoes_menu.css('font-size', font_size_shoes / 16 + 'rem');
                }
            }
            if (apparel_menu.width() < apparel_text.width()) {
                while (apparel_menu.width() < apparel_text.width()) {
                    console.log('hello')
                    font_size_apparel--;
                    apparel_menu.css('font-size', font_size_apparel / 16 + 'rem');
                }
            }
        }

        script_end = true;
    }
    setTimeout(function () {
        resize_start();
    }, 100);
    $(init);
    setTimeout(function () {
        $('.start_shoes').animate({opacity: 1}, 500)
    }, 250);
    setTimeout(function () {
        $('.start_apparel').animate({opacity: 1}, 500)
    }, 250);

})(jQuery);

//WIP
/*
$(window).scroll(function (e) {
    parallax();
});
function parallax() {
    var scrolled = $(window).scrollTop();
    $('.para-triangle-left').css('top', -(scrolled * 0.2) + 'px');
}
*/
