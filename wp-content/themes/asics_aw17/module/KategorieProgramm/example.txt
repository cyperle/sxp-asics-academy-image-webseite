Array
(
    [label_for_shoes] => Shoes
    [label_for_aparel] => Aparel
    [kategorien] => Array
        (
            [Running] => Array
                (
                    [picture_desktop] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/Ritzenhoff_Sprenger_016.jpg
                    [picture_tablet] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-17s-300x222.jpg
                    [picture_phone] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-15s-150x150.jpg
                    [type_shoes] => Array
                        (
                            [0] => Array
                                (
                                    [name] => Road
                                    [layout] => Blau
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                                    [picture_desktop] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/kategoriebild_road_links_mouseover.png
                                    [picture_tablet] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-4s-300x207.jpg
                                    [picture_phone] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/A4_Ritzenhoff.jpg
                                )

                            [1] => Array
                                (
                                    [name] => Fast
                                    [layout] => Gelb
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => ong established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a se
                                    [picture_desktop] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/kategoriebild_fast_links_mouseover.png
                                    [picture_tablet] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-17s-300x222.jpg
                                    [picture_phone] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-15s-150x150.jpg
                                )

                            [2] => Array
                                (
                                    [name] => Trail
                                    [layout] => Grün
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => ng it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.1
                                    [picture_desktop] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/kategoriebild_trail_links_mouseover.png
                                    [picture_tablet] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-6s-300x225.jpg
                                    [picture_phone] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/A4_Ritzenhoff.jpg
                                )

                            [3] => Array
                                (
                                    [name] => Easy
                                    [layout] => Lila
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => e standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                                    [picture_desktop] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/kategoriebild_easy_links_mouseover.png
                                    [picture_tablet] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/BMS_Eichinger-4s-300x207.jpg
                                    [picture_phone] => http://cy-live1.de/asics2017/wp-content/uploads/2017/05/A4_Ritzenhoff.jpg
                                )

                        )

                    [type_apparel] => Array
                        (
                            [0] => Array
                                (
                                    [name] => Apparel 1
                                    [layout] => Blau
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => e standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                                    [picture_desktop] => 25
                                    [picture_tablet] => 24
                                    [picture_phone] => 23
                                )

                            [1] => Array
                                (
                                    [name] => Appreal 2
                                    [layout] => Gelb
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => e standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                                    [picture_desktop] => 24
                                    [picture_tablet] => 22
                                    [picture_phone] => 20
                                )

                            [2] => Array
                                (
                                    [name] => Apparel
                                    [layout] => Grün
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => e standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                                    [picture_desktop] => 21
                                    [picture_tablet] => 22
                                    [picture_phone] => 23
                                )

                            [3] => Array
                                (
                                    [name] => Apparel 4
                                    [layout] => Lila
                                    [programm_label] => Programm
                                    [concept_label] => Concept
                                    [concept] => e standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                                    [picture_desktop] => 25
                                    [picture_tablet] => 22
                                    [picture_phone] => 24
                                )

                        )

                )

        )

)