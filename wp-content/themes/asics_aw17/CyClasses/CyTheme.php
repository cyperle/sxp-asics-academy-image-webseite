<?php
/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 09.11.2017
 * Time: 14:29
 */

/**
 * Allgemeine Theme-Funktionalität.
 */
abstract class CyTheme
{

    static $techs = null;

    static function printt($data)
    {
        echo "<pre class='print-debug'><strong>Debug:::</strong><br>";
        print_r($data);
        echo "</pre>";
    }

    static function collectTechs()
    {
        $data = Array();

        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-technology.php',
        );

        $pages = get_pages($args);

        foreach ($pages as $key => $page) {

            $id = $page->ID;
            $highlight = get_field('highlight', $id);

            if (isset($highlight->slug)) {

                if ($highlight->slug == 'overview_tech_highlights') {

                    $name = get_field('name', $id);

                    //$data[$key]['name'] = get_field('name', $id);
                    $data[$key]['categories'] = get_field('categories', $id)->slug;
                    $data[$key]['cat'] = get_field('categories', $id)->name;
                    $data[$key]['permalink'] = get_permalink($id);

                    if (strlen($name) > 10) {

                        $short_name = get_field('short_name', $id);;

                        if (strlen($short_name) < 1) {

                            $name = substr($name, 0, 7) . "...";

                        } else {

                            $name = $short_name;

                        }


                    }

                    $data[$key]['name'] = $name;

                }

            }

        }

        self::$techs = $data;

    }

    static function silent_alert($msg)
    {
        return '<div class="error">
			<strong>Fehler!</strong><br>
			<p>' . $msg . '</p>
		</div>	';
    }

    static function alert($msg)
    {
        echo self::silent_alert($msg);
    }

    static function init()
    {
        if (class_exists("Asics")) {
            Asics::init();
        } else {
            wp_die('Asics-Klasse ist nicht geladen! Plugin "Asics Main" (asics-functions) muss aktiviert sein!');
        }
    }

    static function mapLayout($name)
    {

        $map = Array(
            "scheme_1" => "lightgreen", //Easy
            "scheme_2" => "blue", //Road
            "scheme_3" => "lightblue", //Fast
            "scheme_4" => "darkgreen", //Trail
//            "scheme_5" => "blue",
//            "scheme_6" => "blue",
            "scheme_7" => "yellow", //Speed,
            "scheme_8" => "red", //Stability
            "scheme_9" => "ocean", //Power, Condition
            "scheme_10" => "blue", //Fitness
            "scheme_11" => "yellow", //Handball
            "scheme_12" => "carrot", //Multicourt
            "scheme_13" => "orange", //Volleyball
        );

        if (isset($map[$name])) {
            $result = $map[$name];
        } else {
            $result = $name;
        }

        return $result;

    }

    static function css_program()
    {
        $key = "css_program";
        if (!CyCache::exists($key)) {
            $slug = "none";
            $field = get_field("programs");
            if (!empty($field)) {
                $slug = $field->slug;
            }
            $css_class = "program_$slug";
            CyCache::set($key, $css_class);
        } else {
            $css_class = CyCache::get($key);
        }

        return $css_class;
    }

}

/**
 * Lade Theme-Funktionalität im Frontend
 */

add_action('init', function () {
    if (!is_admin()) {
        CyTheme::init();
    }
});