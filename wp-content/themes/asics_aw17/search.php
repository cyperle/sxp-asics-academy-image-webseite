<?php

$s=get_search_query();
$args = array(
    's' =>$s
);

$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
        $the_query->the_post();
        $SiteIds = [];
        $posts = $the_query->posts;
        foreach($posts as $post_){
            $SiteIds[] = $post_->ID;
        }
        $datenArray = [];
        $count2 = 0;
        for ($i = 0; $i < count($SiteIds); $i++) {
            if (have_rows('stage', $SiteIds[$i])) {
                while (have_rows('stage', $SiteIds[$i])): the_row();
                    if (get_row_layout() == 'stage_layout'):
                        $datenArray[$count2]['name'] = get_sub_field('name');
                        $datenArray[$count2]['men_women'] = get_sub_field('men_women');
                        $datenArray[$count2]['type'] = get_sub_field('type');
                        $datenArray[$count2]['link'] = get_permalink( $SiteIds[$i]);

                        if ($datenArray[$count2]['men_women'][0] == 'Men') {
                            $mat = get_sub_field('material_varianten_1_men');
                            if (!empty($mat)) {
                                if (!empty($mat[0]['product_image'])) {
                                    $product_image_set = true;
                                    $datenArray[$count2]['product_image'] = $mat[0]['product_image'];
                                }
                            }
                        } else if ($datenArray[$count2]['men_women'][0] == 'Women') {
                            $mat = get_sub_field('material_varianten_1_women');
                            if (!empty($mat)) {
                                if (!empty($mat[0]['product_image'])) {
                                    $product_image_set = true;
                                    $datenArray[$count2]['product_image'] = $mat[0]['product_image'];
                                }
                            }
                        }

                        $count2++;
                    endif;
                endwhile;
            }
        }

    }
    //$out = array_values($datenArray);


    //$datenArray;
    //echo json_encode($datenArray);
    echo json_encode($datenArray);
}else{
    return 'not Found';
}




