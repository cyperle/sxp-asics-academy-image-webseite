<?php if ( is_active_sidebar( 'contact_bms' ) ) : ?>
    <div id="sidebar-header">
        <?php dynamic_sidebar( 'contact_bms' ); ?>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'cy_test_area_2' ) ) : ?>
    <div id="sidebar-header">
        <?php dynamic_sidebar( 'cy_test_area_2' ); ?>
    </div>
<?php endif; ?>
