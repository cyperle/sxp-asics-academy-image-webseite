var Viewport = Viewport || {};

(function ($) {

    var $win = $(window);
    var $html = $(document.documentElement);
    var $body = $(document.body);

    Viewport._settings = [];
    Viewport._active = [];
    Viewport._inactive = [];
    Viewport._currentFontSize = null;

    /**
     * Füge eine Viewport-Einstellung hinzu.
     * @param data
     * @param data.name (String) CSS-Klassenname
     * @param data.min (Number) Mindestwert der Fensterbreite, bei dem dieser Viewport eintritt
     * @param data.max (Number) Maximalwert der Fensterbreite, bei dem dieser Viewport eintritt
     * @param data.remScale (Boolean) Soll die Eigenschaft font-size des HTML-Elements skaliert werden, um Skalierung per Einheit rem zu ermöglichen?
     * @param data.remPx (Number) Ausgangswert der Schriftgröße bei aktiver Rem-Skalierung. Default 16.
     * @param data.remRange.min (Number) Mindestwert, anhand dessen der Skalierungsfaktor berechnet wird. Default data.min .
     * @param data.remRange.max(Number) Maximalwert, anhand dessen der Skalierungsfaktor berechnet wird. Default data.max .
     * @param data.onTrue (Function) Callback der ausgeführt wird, wenn dieser Viewport zutrifft.
     * @param data.onFalse (Function) Callback der ausgeführt wird, wenn dieser Viewport NICHT zutrifft.
     *
     */
    Viewport.addSetting = function (data) {

        var setting = {};
        setting.name = (new String(data.name)).valueOf();

        setting.min = parseInt(data.min);
        setting.max = (data.max === Infinity) ? Infinity : parseInt(data.max);

        if (setting.min > setting.max) {
            var tmp = setting.min;
            setting.min = setting.max;
            setting.max = tmp;
        } else if (setting.min == setting.max) {
            throw new Error("Viewport.addSetting(): min und max dürfen nicht gleich sein!");
        }

        setting.remScale = (new Boolean(data.remScale)).valueOf();

        if (setting.remScale) {

            setting.remRange = {};

            if (typeof data.remRange !== "undefined") {

                if (typeof data.remRange.min === "number") {
                    setting.remRange.min = data.remRange.min;
                } else {
                    setting.remRange.min = data.min;
                }

                if (typeof data.remRange.max === "number") {
                    setting.remRange.max = data.remRange.max;
                } else {
                    setting.remRange.max = data.max;
                }

            } else {

                setting.remRange.min = data.min;
                setting.remRange.max = data.max;

            }

            setting.remPx = (typeof data.remPx === "number") ? data.remPx : 16;

        }

        setting.onTrue = (typeof data.onTrue === "function") ? data.onTrue : null;
        setting.onFalse = (typeof data.onFalse === "function") ? data.onFalse : null;

        Viewport._settings.push(setting);

    };

    Viewport.getWidth = function () {
        return $win.width();
    };

    Viewport.getActive = function () {
        return Viewport._active;
    };

    Viewport.setFontSize = function (value) {
        Viewport._currentFontSize = value;
        $html.css("font-size", value + "px");
    };

    Viewport.getFontSize = function () {
        return Viewport._currentFontSize;
    };

    Viewport.resetFontSize = function () {
        $html.css("font-size", "");
    };

    Viewport.getActive = function () {
        return Viewport._active;
    };

    Viewport.setActive = function (active) {
        Viewport._active = active;
    };

    Viewport.getInactive = function () {
        return Viewport._inactive;
    };

    Viewport.setInactive = function (_inactive) {
        Viewport._active = _inactive;
    };

    Viewport._createClassName = function (settingsList) {
        var ret = [];
        $.each(settingsList, function (index, setting) {
            ret.push(setting.name);
        });
        return ret.join(" ");
    };

    Viewport.getActiveClassName = function () {
        return Viewport._createClassName(Viewport.getActive());
    };

    Viewport.getInactiveClassName = function () {
        return Viewport._createClassName(Viewport.getInactive());
    };

    Viewport.getActiveClassName = function () {
        var active = [];
        $.each(Viewport.getActive(), function (index, setting) {
            active.push(setting.name);
        });
        return active.join(" ");
    };

    Viewport.is = function (check) {
        return $body.hasClass(check);
    };

    Viewport.run = function () {

        var width = Viewport.getWidth();
        var active = [];
        var inactive = [];

        //Durchlaufe Viewports, um festzustellen, ob aktiv oder inaktiv.
        $.each(Viewport._settings, function (index, setting) {

            if (width <= setting.max && width >= setting.min) {
                active.push(setting);
            } else {
                inactive.push(setting);
            }

        });

        Viewport._active = active;
        Viewport._inactive = inactive;

        //Führe Hooks aus.
        $.each(active, function (index, setting) {

            if (setting.onTrue !== null) {
                setting.onTrue();
            }

        });

        $.each(inactive, function (index, setting) {

            if (setting.onFalse !== null) {
                setting.onFalse();
            }

        });

        //Weise Klassennamen zu.
        $body.removeClass(Viewport.getInactiveClassName()).addClass(Viewport.getActiveClassName());

        //Ermittle font-size bzw. setze diese zurück auf ihren Ursprungswert.
        if (active.length > 0) {

            var refData = active[active.length - 1];

            if (refData.remScale === true && isFinite(refData.remRange.max) && isFinite(refData.remRange.min)) {

                /*

                 Beispielrechnung für:
                 remRange.min = 320, hier wäre der Faktor 0
                 remRange.max = 640, hier wäre der Faktor 1

                 Fall 1:
                 width = 640
                 differenceRange = remRange.max - remRange.min = 320
                 differenceWindow = width - remRange.min = 320
                 factor = differenceWindow / differenceRange = 1

                 Fall 2:
                 width = 320
                 differenceRange = remRange.max - remRange.min = 320
                 differenceWindow = width - remRange.min = 0
                 factor = differenceWindow / differenceRange

                 */

                var differenceRange = refData.remRange.max - refData.remRange.min;
                var differenceWindow = width - refData.remRange.min;
                var factor = differenceWindow / differenceRange;
                var newFontSize = refData.remPx * factor;
                Viewport.setFontSize(newFontSize);

            } else {

                Viewport.resetFontSize();

            }

        } else {

            Viewport.resetFontSize();

        }

        $win.trigger("viewport");

    };

    Viewport.init = function () {

        Viewport._currentFontSize = parseFloat($(document.documentElement).css("font-size"));

        Viewport.run();

        $win.on("resize.Viewport", Viewport.run);
        $win.trigger("initviewport");

    };

    Viewport.addSetting({
        name: "vp1440x",
        min: 1440,
        max: Infinity,
        remScale: false
    });

    Viewport.addSetting({
        name: "vp1202x",
        min: 1202,
        max: Infinity,
        remScale: false
    });

    Viewport.addSetting({
        name: "vp0x1440",
        min: 0,
        max: 1440,
        remScale: false
    });

    Viewport.addSetting({
        name: "vp0x1280",
        min: 0,
        max: 1280,
        remScale: false
    });

    Viewport.addSetting({
        name: "vp0x1201",
        min: 0,
        max: 1201,
        remScale: false
    });

    Viewport.addSetting({
        name: "vp0x960",
        min: 0,
        max: 960,
        remScale: false
    });

    Viewport.addSetting({
        name: "vp0x640",
        min: 0,
        max: 640,
        remScale: true,
        remPx: 16
    });

    Viewport.addSetting({
        name: "vp641x1050",
        min: 641,
        max: 1050,
        remScale: false
    });

    $(Viewport.init);

})(jQuery);