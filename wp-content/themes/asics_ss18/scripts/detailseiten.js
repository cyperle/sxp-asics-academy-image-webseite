/**
 * Sliderfunktionalität auf Detailseiten
 */
(function ($) {

    var allow_triangles = true;
    var menu_points = new Array();

    function Slider(parent) {

        var that = this;

        this.parent = parent;

        this.gender = "";
        if (parent.hasClass('slider_men')) {
            this.gender = 'men';
        } else if (parent.hasClass('slider_women')) {
            this.gender = 'women';
        }

        this.wrapper = parent.find('.slider_wrapper');

        this.frame = this.wrapper.find(".slider_frame");

        this.items = this.wrapper.find('.slider_item');
        this.length = this.items.length;
        this.material = 0;

        this.arrow_prev = this.wrapper.find('.arrow.prev');
        this.arrow_prev.onEnterClick(function () {
            this.prev();
        }.bind(this));

        this.arrow_next = this.wrapper.find('.arrow.next');
        this.arrow_next.onEnterClick(function () {
            this.next();
        }.bind(this));

        this.arrows_nav = this.wrapper.find('.slider_nav_triangle');
        this.arrows_nav.onEnterClick(function () {
            var i = $(this).data('item');
            that.jump(i);
        });

        this.position = 0;
        this.frame.attr('data-pos', this.position);

        if (this.length <= 1) {
            this.arrow_prev.hide();
            this.arrow_next.hide();
            this.arrows_nav.hide();
        }

        if (this.length === 0) {
            this.wrapper.hide();
        }

        this._nav_update();

        Slider.instances.push(this);

    }

    Slider.instances = [];
    Slider.get_instance_by_gender = function (gender) {
        var ret = null;
        $.each(Slider.instances, function (index, instance) {
            if (instance.gender === gender) {
                ret = instance;
                return false;
            }
        });
        return ret;
    };

    Slider.toggler_ready = function () {
        $.each(Slider.instances, function (index, instance) {
            instance._update_toggler();
        });
    };

    Slider.prototype = {

        _nav_update: function () {
            $('.menu-point[data-image="' + this.position + '"]').addClass('active');
            var arrow;
            var that = this;
            this.arrows_nav.each(function () {
                if (that.length <= 1) return;
                arrow = $(this);
                if (arrow.data('material') !== that.material) {
                    arrow.hide().removeClass('active');
                } else {
                    arrow.show();
                    if (arrow.data('item') == that.position) {
                        arrow.addClass('active');
                    } else {
                        arrow.removeClass('active');
                    }
                }
            });

        },

        _update_toggler: function () {
            if (typeof fn_toggler.tri_active === 'function') {
                fn_toggler.tri_active(this);
            }
        },

        _update: function (direction) {

            $('.highlight').css('display', 'none');
            $('.highlight[data-image="' + this.position + '"]').css('display', 'block');

            $('.menu-point').removeClass('active');
            $('.menu-point[data-image="' + this.position + '"]').addClass('active');

            $('.slider_item[data-item="' + this.current_pos + '"]').find(".slider_video").each(function () {
                if (this.nodeName === "VIDEO") {
                    this.pause();
                } else if (this.nodeName === "IFRAME") {
                    this.src = this.src;
                }
            });

            if (direction === 'right') {


                $('.slider_item').addClass('transition');

                $('.slider_item[data-item="' + this.current_pos + '"]').removeClass('anim-right').removeClass('anim-left').addClass('pos-pre');
                $('.slider_item[data-item="' + this.position + '"]').addClass('anim-right').removeClass('pos-pre').removeClass('pos-next');


            } else {

                $('.slider_item').addClass('transition')

                $('.slider_item[data-item="' + this.current_pos + '"]').removeClass('anim-right').removeClass('anim-left').addClass('pos-next');
                $('.slider_item[data-item="' + this.position + '"]').addClass('anim-left').removeClass('pos-pre').removeClass('pos-next');

            }

            this.material = $(this.items[this.position]).data('material');
            this._update_toggler();
            this._nav_update();

        },

        _check_pos_value: function () {
            if (this.position > this.length - 1) {
                this.position = 0;
            }
            if (this.position < 0) {
                if (this.length > 1) {
                    this.position = this.length - 1;
                } else {
                    this.position = 0;
                }
            }
        },

        jump_to_material: function (material) {
            var that = this;

            if ($(this.items[this.position]).data('material') === material) {
                return;
            }

            this.items.each(function (index) {
                if ($(this).data('material') === material) {
                    that.jump(index);
                    return false;
                }
            });
        },

        jump: function (pos) {
            if (this.position != pos) {
                this.current_pos = this.position;
                this.position = pos;
                //this._check_pos_value();
                this._update('right');
            }
        },

        next: function () {
            this.current_pos = this.position;
            this.position++;
            this._check_pos_value();
            this._update('right');

        },

        prev: function () {
            this.current_pos = this.position;
            this.position--;
            this._check_pos_value();
            this._update('left');
        },

    };

    var state_gender;

    function fn_toggler() {

        var $slider_men = $('.slider_men');
        var $slider_women = $('.slider_women');

        var $gender_select = $('.gender_select');
        var $men = $('.gender_select .men');
        var $women = $('.gender_select .women');

        var $material_men = $('.material_men');
        var $material_men_tri = $material_men.find('.tri');
        var $material_men_tri_image = $material_men_tri.find('.tri_hover');

        var $material_women = $('.material_women');
        var $material_women_tri = $material_women.find('.tri');
        var $material_women_tri_image = $material_women_tri.find('.tri_hover');

        var slider_instance_men = null;
        var slider_instance_women = null;

        if ($slider_men.length > 0 && $slider_women.length > 0) {
            // Es existieren Materialien für Männer und Frauen. Der Button wird benötigt.
            state_gender = 'men';
            slider_instance_men = Slider.get_instance_by_gender('men');
            slider_instance_women = Slider.get_instance_by_gender('women');

            $gender_select.onEnterClick(gender_toggle);

        } else if ($slider_men.length > 0) {
            // Es existieren nur Materialien für Männer.
            state_gender = "men";
            slider_instance_men = Slider.get_instance_by_gender('men');

        } else if ($slider_women.length > 0) {

            // Es existierten nur Materialien für Frauen.
            state_gender = "women";
            slider_instance_women = Slider.get_instance_by_gender('women');

        } else {
            //In diesem Fall sind entweder keine Materialbilder eingepflegt oder beide Häckchen nicht gesetzt. Rausspringen, um Fehler zu vermeiden.
            return;
        }

        function gender_toggle(e) {

            $('div[data-item]').removeClass('transition pos-pre anim-right')

            if (state_gender === 'men') {
                state_gender = 'women';
                $gender_select.removeClass('men').addClass('women');
                $slider_men.hide();
                $slider_women.show();
                $material_men.hide();
                $material_women.show();
            } else {
                state_gender = 'men';
                $gender_select.removeClass('women').addClass('men');
                $slider_men.show();
                $slider_women.hide();
                $material_men.show();
                $material_women.hide();
            }

            Slider.toggler_ready();

            //pageResize.init(vps);

        }

        $material_men_tri_image.onEnterClick(jump_tri_image);
        $material_women_tri_image.onEnterClick(jump_tri_image);

        function jump_tri_image() {


            if (allow_triangles) {

                var material = $(this).data('material');

                if (state_gender === 'men') {

                    if (slider_instance_men !== null) {
                        slider_instance_men.jump_to_material(material);
                    }

                } else if (state_gender === 'women') {

                    if (slider_instance_women !== null) {
                        slider_instance_women.jump_to_material(material);
                    }

                }

            }

        }


        fn_toggler.tri_active = function (slider_instance) {

            $use = null;

            var material = slider_instance.material;


            if (state_gender === 'men' && slider_instance.gender === 'men') {
                if (slider_instance_men !== null) {
                    $use = $material_men_tri;
                }
            } else if (state_gender === 'women' && slider_instance.gender === 'women') {
                if (slider_instance_women !== null) {
                    $use = $material_women_tri;
                }
            }

            if ($use !== null) {

                $use.each(function (index) {
                    var $tri = $(this);
                    if ($tri.data('material') === material) {
                        $tri.addClass('active');
                    } else {
                        $tri.removeClass('active');
                    }
                });

            }

        };

    }

    function Init() {

        $(".slider").each(function (index, item) {
            new Slider($(this));
        });

        fn_toggler();

        $('.highlight[data-image!="0"]').css('display', 'none');

        var selectors = [$('.slider_wrapper .arrow'), $('.material_inner'), $('.slider_nav')];

        $('div[class^="menu-point"]').click(function (e) {

            var pos = $(this).data('image');

            $('.highlight').removeClass('active');

            var highlight = $(this).attr('data-highlight');
            $('div[class="highlight"][data-highlight="' + highlight + '"]').addClass('active');
            $(this).addClass('active');

            $.each(Slider.instances, function (index, instance) {
                instance.jump(pos);
            });

        });

        $('.gender_select').click(function () {

            $('.highlight').removeClass('active');

            $.each(Slider.instances, function (index, instance) {
                instance.jump(0);
            });

        });

        $('.highlight').hover(function () {
            $('.highlight').removeClass('active');
        }).on("click", function () {
            var $this = $(this);
            BIGOVERLAY.show($this.data("name"), $this.data("text"));
        });

        Slider.toggler_ready();

    }

    $(Init);

})(jQuery);

/**
 * Overlay-Funktionalität auf Detailseiten
 */

(function ($) {

    var $tech_desc;
    var $product_single;
    var $detail_section;
    var layout = 'blue';

    function on_tech() {

        var $this = $(this);
        var desc = $this.data('desc');
        var title = $this.text();
        BIGOVERLAY.show(title, desc, layout);

    }

    function init() {

        $product_single = $('.product_single');
        var tmp = $product_single.data('layout');

        if (tmp !== "") {
            layout = tmp;
        }

        $tech_desc = $('.tech-desc');
        $tech_desc.onEnterClick(on_tech);

        $detail_section = $(".detail_section");

        if ($detail_section.length > 0) {

            $detail_section.each(function (index, el) {

                var $this = $(this);
                var state = true;
                var $content = $this.find(".detail_content");
                $content.css("transform-origin", "0 0");

                var $auf = $this.find(".detail_auf");
                var $zu = $this.find(".detail_zu");

                $auf.on("click", function () {

                    if (!state) {

                        $auf.addClass("active");
                        $zu.removeClass("active");

                        state = true;

                        TweenMax.set($content, {
                            height: "auto"
                        });

                        TweenMax.from($content, ANIMID, {
                            height: 0
                        });

                    }

                });

                $zu.on("click", function () {

                    if (state) {

                        $zu.addClass("active");
                        $auf.removeClass("active");

                        state = false;

                        TweenMax.to($content, ANIMID, {
                            height: 0
                        });

                    }

                });

            });

        }

    }

    $(init);

})(jQuery);

/**
 * Funktionalität Weiterlesen Detailseiten
 */

(function ($) {

    function init() {

        //fn_programme();

        $('.vid:not(.update)').click(function () {
            if (myvideo.indexOf("mp4") === 0) {
                BIGOVERLAY.show('', '<video controls src="' + myvideo + '"></video>', 'video');
            } else {
                BIGOVERLAY.show('', '<iframe src="https://player.vimeo.com/video' + myvideo.substr(myvideo.lastIndexOf("/"), myvideo.length) + '?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 'video');
            }
        });

        $('.update').click(function () {
            BIGOVERLAY.show($(this)[0].innerHTML, $('.updates_text')[0].outerHTML, $('.product_single').data('layout'));
        });

        $('.learn-more').click(function () {
            $('html, body').animate({
                scrollTop: $(".section_technologies").offset().top
            }, 1000);
        });

    }

    $(init);

})(jQuery);