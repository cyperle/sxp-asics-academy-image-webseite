<?php
/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 09.11.2017
 * Time: 14:29
 */

/**
 * Allgemeine Theme-Funktionalität.
 */
abstract class CyTheme
{

    static $techs = null;

    static function printt($data)
    {
        echo "<pre class='print-debug'><strong>Debug:::</strong><br>";
        print_r($data);
        echo "</pre>";
    }

    static function dump($data)
    {
        echo "<pre class='print-debug'><strong>Debug:::</strong><br>";
        var_dump($data);
        echo "</pre>";
    }

    static function clipTechName($name, $shortname)
    {

        $result = $name;

        if (strlen($name) > 10) {

            if (strlen($shortname) < 1) {
                $result = substr($name, 0, 10) . "...";
            } else {
                $result = $shortname;
            }

        }

        return $result;

    }

    static function collectAllTechs()
    {

        $data = Array();

        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-technology.php',
        );

        $pages = get_pages($args);

        foreach ($pages as $key => $page) {

            $id = $page->ID;

            if(Asics::isInSeason(get_field('season',$id))) {

            } else {
                continue;
            }
            
            $highlight = get_field('highlight', $id);

            if ($highlight->slug == 'overview_tech_highlights') {

                //$data[$key]['name'] = get_field('name', $id);
                $data[$key]['name'] = CyTheme::clipTechName(get_field('name', $id), get_field('short_name', $id));
                $data[$key]['categories'] = get_field('categories', $id)->slug;
                $data[$key]['cat'] = get_field('categories', $id)->name;
                $data[$key]['permalink'] = get_permalink($id);

            }

        }

        self::$techs = $data;

    }

    static function silent_alert($msg)
    {
        return '<div class="error">
			<p>' . $msg . '</p>
		</div>	';
    }

    static function alert($msg)
    {
        echo self::silent_alert($msg);
    }

    static function init()
    {
        if (class_exists("Asics")) {
            Asics::init();
        } else {
            wp_die('Asics-Klasse ist nicht geladen! Plugin "Asics Main" (asics-functions) muss aktiviert sein!');
        }
    }

}

/**
 * Lade Theme-Funktionalität im Frontend
 */

add_action('init', function () {
    if (!is_admin()) {
        CyTheme::init();
    }
});