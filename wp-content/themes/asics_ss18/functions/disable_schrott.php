<?php
/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 09.11.2017
 * Time: 14:34
 */

/**
 * Wordpress-Schrott für geringen Performance-Schub im Frontend deaktivieren.
 */

function remove_admin_bar_bump() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_bar_bump');

function disable_emojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
}

add_action('init', 'disable_emojis');

function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

function fd_disable_feed()
{
    wp_die(__('Feeds sind nicht verfügbar, besuche bitte die <a href="' . get_bloginfo('url') . '">Website</a>!'));
}

add_action('do_feed', 'fd_disable_feed', 1);
add_action('do_feed_rdf', 'fd_disable_feed', 1);
add_action('do_feed_rss', 'fd_disable_feed', 1);
add_action('do_feed_rss2', 'fd_disable_feed', 1);
add_action('do_feed_atom', 'fd_disable_feed', 1);
add_action('do_feed_rss2_comments', 'fd_disable_feed', 1);
add_action('do_feed_atom_comments', 'fd_disable_feed', 1);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

function fd_remove_version()
{
    return '';
}

add_filter('the_generator', 'fd_remove_version');
