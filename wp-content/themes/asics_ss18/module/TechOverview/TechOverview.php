<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulTechOverview extends CyModul
{

    public $data = [];
    public $path = "";

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {

        $this->path = parent::$config->uri . '/module/Produktseite/';

        $datenArray = [];

        $datenArray['category'] = get_field('highlight');
        //$datenArray['category2'] = get_field('categories');
        $datenArray['type'] = get_field('type');
        //$datenArray['program'] = get_field('programs');
        //$datenArray['color'] = get_field('layout', $datenArray['program']);

        $datenArray['page_title'] = get_field('overview_title');
        $datenArray['show_product_label'] = get_field('show_product_label');
        $datenArray['tech_data'] = Asics::collectTechsByCat($datenArray['category']->slug);

        if (empty($datenArray['show_product_label'])) {
            $datenArray['show_product_label'] = 'Show';
        }

        //CyTheme::printt($datenArray);

        $this->data = $datenArray;

    }


    public function print_module()
    {


        $ob = "";
        $has_data = false;

        if (!!count($this->data['tech_data'])) :

            $has_data = true;

            foreach ($this->data['tech_data'] as &$data) :

                $ob .= <<<html
<li class="tech-box" style="background-image:url({$data['thumb']['sizes']['large']})">
    <a href="{$data['permalink']}">
        <p class="p-name">{$data['name']}</p>
        <div class="show-p">
            <span>
                <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;{$this->data['show_product_label']}
            </span>
        </div>
    </a>
</li>
<br class="tech-divider">
html;

            endforeach;

            ?>

            <?php

            if (!empty($this->data['category'])) :

                ?>

                <div class="product-page">

                    <div class="inner_wrapper">

                        <section class="wrappe wrapper_product_list">

                            <ul class="products-inner">
                                <?php echo $ob; ?>
                            </ul>

                        </section>

                    </div>

                </div>

            <?php endif; ?>

        <?php else : ?>

            <div class="inner_wrapper">

                <?php

                $error_msg = '';
                if (!count($this->data['tech_data'])) {
                    $error_msg = 'Es sind keine Technologien mit diesen Kriterien vorhanden.';
                } else if (empty($this->data['program']) && empty($this->data['category'])) {
                    $error_msg = 'Diese Seite vom Typ "Technologies Overview" ist keinem Programm und keiner Kategorie zugewiesen oder die bestehenden Relationen sind gelöscht.';
                } else if (empty($this->data['program'])) {
                    $error_msg = 'Diese Seite vom Typ "Technologies Overview" ist keinem Programm zugewiesen oder die bestehende Relation sind gelöscht.';
                } else if (empty($this->data['category'])) {
                    $error_msg = 'Diese Seite vom Typ "Technologies Overview" ist keiner Kategorie zugewiesen oder die bestehende Relation sind gelöscht.';
                }

                CyTheme::alert($error_msg);

                ?>

            </div>

        <?php endif;

    }

}

new ModulTechOverview();

?>