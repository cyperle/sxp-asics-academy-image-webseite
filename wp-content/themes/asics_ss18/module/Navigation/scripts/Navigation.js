(function ($) {

    var $body;

    var $topbar;
    var $topbar_right;
    var $topnav_nav_search;
    var $search;
    var $topnav_nav_burger;
    var $lang_list;

    var $sidebar_wrapper;
    var $sidebar_content;
    var $sidebar_close;

    var $footer_wrapper;

    var is_open = false
    var $html = $('html');
    var rem;

    /**
     * Reguliert die Handler zum Öffnen und Schließen der Navigation.
     */
    function fn_nav_adjust_height() {

        var offsetTop = $sidebar_wrapper.offset().top;
        var refHeight = $.doc.height();
        var height = Math.floor(refHeight - offsetTop);
        $sidebar_wrapper.css("min-height", height);

    }

    function fn_nav_adjust_position() {

        var right = 0;

        if (Viewport.is("vp0x640")) {
            right = 0;
        } else {
            right = $.win.width() - ($topbar_right.offset().left + $topbar_right.width());
        }

        $sidebar_wrapper.css("right", right);

        fn_nav_adjust_height();

    }

    function fn_nav_handler() {

        var translate = "110%";

        $topnav_nav_burger.onEnterClick(open_nav);
        $sidebar_close.onEnterClick(close_nav);
        $sidebar_wrapper.hide();

       // onElementHeightChange(document.body, on_resize);
        $.win.on("viewport.nav", on_resize);
        // $.win.on("stickyfooter.nav", fn_nav_adjust_height);

        function open_nav() {

            if (!is_open) {

                TweenMax.fromTo($sidebar_wrapper, ANIMID, {
                    x: translate,
                    alpha: 0,
                    display: "block"
                }, {
                    x: "0%",
                    alpha: 1
                });

                is_open = true;
                $sidebar_wrapper.addClass('opened');

                on_resize();

            }

        }

        function close_nav() {

            if (is_open) {

                TweenMax.fromTo($sidebar_wrapper, ANIMID, {
                    x: "0%",
                    alpha: 1
                }, {
                    x: translate,
                    alpha: 0,
                    display: "none",
                    onComplete: function () {
                        is_open = false;
                        $sidebar_wrapper.removeClass('opened');
                    }
                });
            }


        }

        function on_resize() {

            if (is_open) {

                //Höhe angleichen an Header
                $sidebar_content.css("margin-top", $topbar.height());
                fn_nav_adjust_position();

            }

        }

        TweenMax.set($sidebar_wrapper, {
            x: translate
        });

        $.win.on("pageresize", on_resize);
        on_resize();

    }


    /**
     * Bedienung der Sprachauswahl.
     */
    function fn_lang() {

        if ($lang_list.length < 1) return;

        var state = 0;

        var $lang_item_link = $('a.lang_item_link');
        var $lang_item_0 = $('.lang_item.i_0');
        var $is_option = $('.lang_item.is_option');

        $lang_item_link.on('click', function (e) {
            e.stopPropagation();
        });
        $lang_item_link.on('focus', on_focus);
        $lang_item_link.on('blur', on_blur);

        $lang_item_0.onEnterClick(on_toggle);

        function on_toggle(e) {
            e.stopPropagation();
            if (state === 0) {
                on_focus();
            } else {
                on_blur();
            }
        }

        function on_focus() {
            if (state === 0) {
                state = 1;
                $lang_list.addClass('open');
                TweenMax.staggerFromTo($is_option, .2, {
                    x: "-100%",
                    alpha: 0
                }, {
                    x: "0%",
                    alpha: 1,
                    ease: Back.easeOut.config(1.7)
                }, .1);
                click_outside_listener();
            }
        }

        function on_blur() {
            if (state === 1) {
                state = 0;
                TweenMax.staggerFromTo($is_option, .2, {
                    x: "0%",
                    alpha: 1
                }, {
                    x: "-100%",
                    alpha: 0,
                    ease: Back.easeIn.config(1.7)
                }, -.1, function () {
                    $lang_list.removeClass('open');
                });
            }
        }

        function click_outside_listener() {
            $.doc.on("click.click_outside_listener", function (e) {
                on_blur();
                $.doc.off("click.click_outside_listener");
            });
        }

    }

    /**
     * Suche
     */

    function fn_search() {

        var duration = ANIMID;

        var isOpen = false;
        var $search = $("#search");
        var $hide_on_search = $(".hide_on_search");
        var $search_input = $search.find("#search-input");
        var $search_form = $search.find("#search-form");
        var $search_result = $search.find("#search-result ul");
        var $topbar_inner = $(".topbar_inner");

        var $search_form_wrapper = $search.find(".search_form_wrapper");
        var $search_btn = $search.find(".search_btn");
        var $topnav_btn_search = $search.find(".topnav_btn_search");
        var $search_form_ani = $search.find(".search_form_ani").width(0);
        var $topnav_btn_close = $search.find(".topnav_btn_close");

        var original_position,
            original_offset,
            closed_width,
            opened_width,
            search_btn_width;

        $search.click(function (e) {
            $search.addClass("interact");
            e.stopPropagation();
        });

        $topnav_btn_search.onEnterClick(open_search);
        $topnav_btn_close.onEnterClick(close_search);

        $search_input.keyup(sbm_search)
        $search_form.submit(function (e) {
            e.preventDefault();
            sbm_search();
        });

        function resize_search() {

            if (isOpen) {

                // var tmp = duration;
                // duration = 0;
                // close_search();
                // duration = tmp;

            }

        }

        function open_search() {

            var search_btn_to = {},
                overflowing_width = 0;

            original_offset = $search.offset();

            closed_width = $search.width();
            search_btn_width = $search_btn.width();

            $search_form_wrapper.removeClass("hide").addClass("visible");
            opened_width = $search_form_wrapper.width();

            if (Viewport.is("vp0x640")) {

                TweenMax.set($topbar_inner, {
                    position: "static"
                });


                TweenMax.set($search, {
                    position: "absolute",
                    left: original_offset.left,
                    margin: "0"
                });

                TweenMax.set($search, {
                    position: "absolute",
                    left: original_offset.left,
                    margin: "0"
                });

                TweenMax.to($search, duration, {
                    left: 0
                });

                overflowing_width = $.win.width() - 6 - opened_width;
                opened_width += overflowing_width;

                TweenMax.set($search_input, {width: "+=" + overflowing_width});

            }

            TweenMax.to($search_form_ani, duration, {
                width: opened_width
            });

            TweenMax.to($search_btn, duration, {
                x: search_btn_width,
                width: 0,
                alpha: 0
            });

            TweenMax.set($hide_on_search, {
                display: "none"
            });

            //$('html').on("click.search", close_search);
            $.win.on("resize.search", resize_search);

            isOpen = true;

        }

        function close_search() {

            $search.attr('active', 'false');
            $search_result.html('');

            if (Viewport.is("vp0x640")) {

                TweenMax.to($search, duration, {
                    left: original_offset.left
                });

            }

            TweenMax.to($search_form_ani, duration, {
                width: 0
            });

            TweenMax.to($search_btn, duration, {

                x: 0,
                width: search_btn_width,
                alpha: 1,

                onComplete: function () {

                    TweenMax.set($search_btn, {width: "auto"});
                    TweenMax.set($search_input, {width: "auto"});
                    TweenMax.set($search, {
                        position: "",
                        left: "",
                        margin: ""
                    });

                }

            });

            TweenMax.set($hide_on_search, {
                display: "inline-block",
                delay: duration
            });

            // $('html').off("click.search", close_search);
            $.win.off("resize.search", resize_search);

            isOpen = false;

        }

        function sbm_search() {

            var action = $search_form.attr('action');

            $.get(action, {"s": $search_input.val()}).done(function (msg) {

                if (msg.length > 0) {

                    var result = JSON.parse(msg);

                    if (result.length > 0) {

                        var htmlString = "";

                        $.each(result, function (index, value) {

                            htmlString += '<li class="search-result-item">' +
                                '<a href="' + value.link + '">' +
                                '<div class="search-result-img"><img src="' + value.product_image + '"></div>' +
                                '<p class="search-result-name">' + value.name + '</p>'
                            '</a>'
                            '</li>'

                        });

                        //console.log(htmlString);

                        $search_result.html(htmlString);

                    }

                }

            });
        }
    }

    function init() {

        $body = $(document.body);
        $topbar = $(".topbar");
        $topbar_right = $(".topbar_right");
        $topnav_nav_burger = $(".topnav_burger");
        $topnav_nav_search = $(".topnav_search");
        $search = $('#search');
        $lang_list = $(".lang_list");
        $sidebar_wrapper = $(".sidebar_wrapper");
        $sidebar_content = $(".sidebar_content");
        $sidebar_close = $(".sidebar_close .topnav_btn");
        $footer_wrapper = $(".footer_wrapper");

        fn_nav_handler();
        fn_lang();
        fn_search();

    }

    $(init);

})(jQuery);