<?php

class ModulProduct extends CyModul
{

    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->print_module();

    }

    public function set_module()
    {

        $datenArray = [];

        /**
         * Stelle Technologie-Typ fest: Apparel oder Shoe.
         *
         */

        $layout = get_field("layout");
        $techtype = "techtype_";

        switch ($layout) {
            case "shoe_highlight": {
                $techtype .= "shoe";
                break;
            }
            case "apparel_highlight": {
                $techtype .= "apparel_highlight";
                break;
            }
            case "apparel_product": {
                $techtype .= "apparel_product";
                break;
            }
            default: {
                $field_type = get_field("type");
                if ($field_type == "Apparels") {
                    $techtype .= "apparel_highlight";
                } else if ($field_type) {
                    $techtype .= "shoe";
                } else {
                    $techtype .= "shoe";
                }
            }
        }

        $datenArray["techtype"] = $techtype;

        /* Header Content */
        $datenArray['name'] = get_field('name');
        $datenArray['short_name'] = get_field('short_name');
        $datenArray['description'] = get_field('description');

        /* Get Asset URLs as Array */
        $i = 0;
        foreach (get_field('assets') as $key => $asset) {

            $datenArray['slideshow_assets'][$i]['video'] = ($asset['imagevideo'] == 'Video') ? true : false;
            $datenArray['slideshow_assets'][$i]['src'] = ($asset['imagevideo'] == 'Video') ?
                array($asset['video_desktop'], $asset['video_tablet'], $asset['video_mobile'])
                : array($asset['image_desktop']['url'], $asset['image_tablet']['url'], $asset['image_mobile']['url']);
            $i++;

        }

        /* How it works Data (hiw) */
        $datenArray['hiw_headline'] = get_field('hiw_headline');
        $datenArray['hiw_open_label'] = get_field('hiw_open_label');

        $datenArray['hiw_content'] = get_field('hiw_content');
        foreach (get_field('hiw_content') as $key => $content) {
            $datenArray['hiw_content'][$key] = $content;
        }

        /* What it does Data (wid) */
        $datenArray['wid_headline'] = get_field('wid_headline');
        $datenArray['wid_open_label'] = get_field('wid_open_label');
        $datenArray['wid_content'] = get_field('wid_content');

        //CyTheme::dump($datenArray);

        $this->data = $datenArray;

    }

    public function print_module()
    {

        ?>

        <script>
            var readmore = '<?php echo __('read more', 'CyTheme@Asics'); ?>';
        </script>

        <section <?php
        $dataLayout = "";
        if(isset($this->data["layout"])) {
            $dataLayout = $this->data["layout"];
        }
        echo htmlAttr([
            "class" => ["product_single", $this->data['techtype']],
            "data-layout" => [$dataLayout]
        ]);

        ?>>

            <!-- Slider -->
            <?php

            $nav_triangles = "";
            $i = 0;

            ?>

            <div class="inner_wrapper detail_slider">

                <section class="slider slider_men" data-gender="men">

                    <div class="slider_wrapper">
                        <div class="slider_holder">

                            <div class="slider_gallery">

                                <div class="slider_frame">
                                    <?php
                                    $i = 0;

                                    foreach ($this->data['slideshow_assets'] as $key => $value) {

                                        if ($i == 0) {
                                            $active = ' active';
                                        } else {
                                            $active = '';
                                        }

                                        $nav_triangles .= '<div class="slider_nav_triangle' . $active . '" data-item="' . $i . '" data-material="0"></div>';

                                        if (!$value['video']) {


                                            echo "<div
                                                        class=\"slider_item\"
                                                        data-item=\"" . $i . "\"
                                                        data-material=\"0\"
                                                        data-desktop=\"" . str_replace(get_home_url(), '..', $value['src'][0]) . "\"
                                                        data-tablet=\"" . str_replace(get_home_url(), '..', $value['src'][1]) . "\"
                                                        data-mobile=\"" . str_replace(get_home_url(), '..', $value['src'][2]) . "\" style=\"background-image: url('" . str_replace(get_home_url(), '..', $value['src'][0]) . "')\">
                                                </div>";

                                        } else if (strpos($value['src'][0], ".mp4") != 0) {

                                            echo "<div
                                                        class=\"slider_item\"
                                                        data-item=\"" . $i . "\"
                                                        data-material=\"0\"
                                                        data-desktop=\"" . $value['src'][0] . "\"
                                                        data-tablet=\"" . $value['src'][1] . "\"
                                                        data-mobile=\"" . $value['src'][2] . "\" style=\"background-image: url('" . $value['src'][0] . "')\">
                                                        
                                                <videoclass=\"slider_video\">
                                                      <source src=\"" . $value['src'][0] . "\" type=\"video/mp4\">
                                                      <source src=\"" . $value['src'][0] . "\" type=\"video/mp4\">
                                                      <source src=\"" . $value['src'][0] . "\" type=\"video/mp4\">
                                                </video></div>";

                                        } else {

                                            echo "<div
                                                        class=\"slider_item\"
                                                        data-item=\"" . $i . "\"
                                                        data-material=\"0\"
                                                        data-desktop=\"" . $value['src'][0] . "\"
                                                        data-tablet=\"" . $value['src'][1] . "\"
                                                        data-mobile=\"" . $value['src'][2] . "\" style=\"background-image: url('" . $value['src'][0] . "')\">
                                               <iframe class=\"slider_video\" src='https://player.vimeo.com/video" . substr($value['src'][0], strrpos($value['src'][0], '/'), strlen($value['src'][0])) . "?title=0&byline=0&portrait=0' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                </div>";

                                        }

                                        $i++;

                                    };

                                    ?>

                                </div>

                            </div>

                            <div class="arrow prev"></div>
                            <div class="arrow next"></div>

                        </div>

                        <div class="slider_nav">
                            <?php echo $nav_triangles; ?>
                        </div>

                    </div>

                </section>

            </div>

            <div class="inner_wrapper detail_wrapper">

                <section class="detail_section product_description first-teaser">

                    <h2 class="product_description_heading">
                        <?php echo strip_tags(apply_filters('the_content', $this->data['name'])); ?>
                    </h2>

                    <?php echo apply_filters("the_content", $this->data["description"]); ?>

                </section>

                <section class="detail_section section_technologies">

                    <header class="technologies_header">

                        <div class="technologies_header_inner">
                            <h2 class="technologies_headline"><?php echo $this->data['hiw_headline']; ?></h2>
                            <div class="detail_section_controls">
                                <div class="detail_zu noselect">-</div>
                                <div class="detail_auf noselect active">+</div>
                            </div>
                        </div>

                    </header>

                    <div class="detail_content">

                        <div class="technologies clearfix paragraphs">

                            <div class="content-align">

                                <?php

                                $set = false;

                                foreach ($this->data['hiw_content'] as $key => $val) {

                                    if ($set) {
                                        echo '<div class="trenner"></div>';
                                    }
                                    $set = true;

                                    $heading = "";
                                    $heading_own_row = true;

                                    if ($val['heading'] != '') {

                                        $heading = '<div class="heading"><h2>' . str_replace([
                                                '<p>',
                                                '</p>',
                                                'font-size: 20px;'
                                            ], '', $val['heading']) . '</h2></div>';

                                    }

                                    $html = [];

                                    if ($val['content_layout'] == 'Image+Text') {

                                        $heading_own_row = true;

                                        $html[] = '<div class="content-split">' .
                                            '<img class="content-image" src="' . $val['image'] . '">' .
                                            '</div>';

                                        $html[] = '<div class="content-split">' .
                                            //$heading .
                                            $val['text_field_1'] .
                                            '</div>';

                                    } elseif ($val['content_layout'] == 'Text+Text') {

                                        $heading_own_row = true;

                                        $html[] =
                                            '<div class="content-split">' .
                                            $val['text_field_1'] .
                                            '</div>';

                                        $html[] =
                                            '<div class="content-split">' .
                                            $val['text_field_2'] .
                                            '</div>';


                                    } else {

                                    }

                                    if ($val['content_mirror'] == 'Yes') {
                                        $html = array_reverse($html);
                                    }

                                    if ($heading_own_row) {
                                        echo '<div class="content-row clearfix">' . $heading . '</div>';
                                    } else {

                                    }

                                    echo '<div class="content-row clearfix">' . implode('', $html) . '</div>';

                                }

                                ?>

                            </div>

                        </div>

                    </div>

                </section><!-- .section_technologies -->

                <section class="detail_section section_features">

                    <header class="features_header">

                        <div class="features_header_inner">
                            <h2 class="features_headline"><?php echo $this->data['wid_headline']; ?></h2>
                            <div class="detail_section_controls">
                                <div class="detail_zu noselect">-</div>
                                <div class="detail_auf noselect active">+</div>
                            </div>
                        </div>

                    </header>

                    <div class="detail_content">

                        <div class="features clearfix paragraphs">

                            <?php
                            $set = false;
                            foreach ($this->data['wid_content'] as $key => $val) {

                                if ($set) {
                                    echo '<div class="trenner"></div>';
                                }
                                $set = true;

                                if ($val['heading'] != '') {

                                    echo '<div class="heading content-split"><h2>' . str_replace([
                                            '<p>',
                                            '</p>',
                                            'font-size: 20px;'
                                        ], '', $val['heading']) . '</h2></div>';
                                }

                                $html = [];

                                if ($val['content_layout'] == 'Image+Text') {

                                    $html[] = '<div class="content-split image_field">' .
                                        '<img src="' . $val['image'] . '">' .
                                        '</div>';

                                    $html[] = '<div class="content-split text_field_1">' .
                                        $val['text_field_1'] .
                                        '</div>';

                                } elseif ($val['content_layout'] == 'Text+Text') {

                                    $html[] =
                                        '<div class="content-split text_field_1">' .
                                        $val['text_field_1'] .
                                        '</div>';

                                    $html[] =
                                        '<div class="content-split text_field_2">' .
                                        $val['text_field_2'] .
                                        '</div>';


                                } else {

                                }

                                if ($val['content_mirror'] == 'Yes') {
                                    $html = array_reverse($html);
                                }


                                echo '<div class="content-row">' . implode('', $html) . '</div>';

                            }

                            ?>

                        </div><!-- .features -->

                    </div>

                </section><!-- .section_features -->

            </div><!-- .inner_wrapper -->

        </section>

        <?php

        //parent::add_script(get_template_directory_uri() . '/module/TechHighlights/scripts/TechHighlights.js');

    }

}

new ModulProduct();

?>