/**
 * Created by aberezinets on 29.06.2017.
 */

$(function () {

    var cushioning = $('.cushioning');
    var structuredCushioning = $('.structured-cushioning');
    var cushioningContainer = $('.wrapper:nth-of-type(2)');
    var structuredCushioningContainer = $('.wrapper:nth-of-type(1)');
    var $product_page = $(".product-page");

    var duration = .25;
    var tl;

    function fix_height() {
        var h = Math.max(cushioningContainer.height(), structuredCushioningContainer.height());
        $product_page.css("min-height", h);
    }

    function remove_fix_height() {
        $product_page.css("min-height", "");
    }

    structuredCushioning.click(function () {

        structuredCushioning.addClass('active');
        cushioning.removeClass('active');

        fix_height();
        tl.reverse();

    });

    cushioning.click(function () {

        structuredCushioning.removeClass('active');
        cushioning.addClass('active');

        fix_height();
        tl.play();

    });

    function init() {

        var cushioning_ready = false;
        var structured_cushioning_ready = false;

        if (cushioning && cushioningContainer) {
            cushioning_ready = true;
        }

        if (cushioningContainer && structuredCushioningContainer) {
            structured_cushioning_ready = true;
        }

        if (cushioning_ready && structured_cushioning_ready) {

            var set_height = true;

            tl = new TimelineMax({
                paused: true,
                onComplete: remove_fix_height,
                onReverseComplete: remove_fix_height
            });

            tl.fromTo(structuredCushioningContainer, duration, {
                    alpha: 1,
                    display: "block"
                },
                {
                    alpha: 0,
                    display: "none"
                }
            );

            tl.fromTo(cushioningContainer, duration, {
                    alpha: 0,
                    display: "none"
                }, {
                    alpha: 1,
                    display: "block"
                }
            );

        } else {

            $(".selector").remove();

        }

    }

    init();

});