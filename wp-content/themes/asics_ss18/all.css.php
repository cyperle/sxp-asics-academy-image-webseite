<?php

$cache_time = 3600;
$last_modified_time = filemtime('all.css.php');

ob_start("ob_gzhandler");

header("Content-type: text/css");
header("Cache-Control: public");
header("Last-Modified: " . gmdate("D, d M Y H:i:s", $last_modified_time) . " GMT");
header("Expires: " . gmdate("D, d M Y H:i:s", time() + $cache_time) . " GMT");

$files = Array(
"style/main.css",
"module/Footer/style/Footer.css",
"module/Header/style/Header.css",
"module/KategorieProgramm/style/KategorieProgramm.css",
"module/Navigation/style/Navigation.css",
"module/Overview/style/Overview.css",
"module/Product_v200/style/Product_v200.css",
"module/Simple/style/Simple.css",
"module/TechHighlights/style/TechHighlights.css",
"module/TechOverview/style/TechOverview.css",
"style/schemes.css"
);

echo "/* @time ". date("d.m.Y G:i") . " */\n";
echo "/* @sources " . count($files) . " */";
foreach($files as $file) {
    if(file_exists($file)) {
        echo "\n\n/* @source $file */\n\n";
        readfile($file);
    }
}

?>
