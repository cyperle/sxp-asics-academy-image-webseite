<!doctype html>

<html lang="<?php echo ICL_LANGUAGE_CODE; ?>" class="desktop">
<head>

    <title><?php the_title(); ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo get_template_directory_uri(); ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo get_template_directory_uri(); ?>/images/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/manifest.json">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/safari-pinned-tab.svg"
          color="#101213">

    <meta name="theme-color" content="#101213">

    <meta
            name="format-detection"
            content="telephone=no">

    <link
            rel="stylesheet"
            type="text/css"
            href="<?php echo get_template_directory_uri(); ?>/all.css.php?<?php echo microtime(); ?>">

    <link href="/wp-content/font-awesome/css/fontawesome.css" rel="stylesheet">
    <link href="/wp-content/font-awesome/css/solid.css" rel="stylesheet">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

</head>

<?php
$attr = htmlAttr(Array(
    "class" => Array(
        'body_scheme',
        Asics::classProgram(),
        Asics::classPageType(),
        "nojs"
    )
));
echo "<body $attr>";
?>

<script>
    document.body.className = document.body.className.replace("nojs", "js");
</script>

<div class="site_wrapper">