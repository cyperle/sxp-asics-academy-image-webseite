/**
 * Globale Config
 */

var ANILONG = 1;
var ANIMID = .5;
var ANISHORT = .25;

/**
 * Trigger: Callback ausführen, wenn sich die Höhe eines Elements ändern.
 * @param elm
 * @param callback
 */
function onElementHeightChange(elm, callback) {

    var lastHeight = elm.clientHeight, newHeight;
    var nochange = 0;

    (function run() {
        newHeight = elm.clientHeight;

        if (lastHeight != newHeight) {
            nochange = 0;
            callback(elm);
        } else {
            nochange++;
        }

        time = 200;
        if (nochange > 10) {
            time = 1000;
        }

        lastHeight = newHeight;

        if (elm.onElementHeightChangeTimer) {
            clearTimeout(elm.onElementHeightChangeTimer);
        }

        elm.onElementHeightChangeTimer = setTimeout(run, time);

    })();

}

/**
 * Allgemeine Skripte
 */

(function ($) {

    //Entferne Klasse "preload" von Body nach Skriptausführung
    function remove_body_preload_class() {
        $.body.removeClass("preload");
    }

    function viewport_ready() {
        $.body.addClass("ready");
    }

    //Sticky Footer
    function sticky_footer() {

        var $footer = $('.footer_wrapper');
        var $pageContent = $('.page_content');
        var delay = 20;
        var fixed = false;

        function check() {
            var top = $pageContent.offset().top + $pageContent.outerHeight() + $footer.outerHeight();
            if (top < $.win.height() && !fixed) {
                fixed = true;
                $footer.css({
                    display: 'block',
                    width: '100%',
                    position: 'fixed',
                    left: 0,
                    bottom: 0
                });
            } else if (top >= $.win.height() && fixed) {
                fixed = false;
                $footer.css({
                    display: '',
                    width: '',
                    position: '',
                    left: '',
                    bottom: ''
                });
            }
            setTimeout(check, delay);
        }

        check();
        if (document.readyState !== "complete") {
            $(document).on('readystatechange', function () {
                if (document.readyState === "complete") {
                    delay = 1000;
                }
            });
        } else {
            delay = 1000;
        }

    }

    function init() {

        remove_body_preload_class();
        $.win.on("initviewport", viewport_ready);
        sticky_footer();

    }

    $(init);

})(jQuery);

/**
 * Overlay für Beschreibungen und Texte
 */

var BIGOVERLAY = {};

(function ($) {

    var big = {};

    big.prevent_page = $('<div class="prevent_page">');
    big.overlay = $('<div class="big_overlay">');
    big.white1 = $('<div class="white_1">').appendTo(big.overlay);
    big.white2 = $('<div class="white_2">').appendTo(big.overlay);
    big.content = $('<div class="big_content">').appendTo(big.overlay);
    big.titel = $('<div class="big_titel">').appendTo(big.content);
    big.beschreibung = $('<div class="big_beschreibung">').appendTo(big.content);
    big.close = $('<div class="big_close">').appendTo(big.overlay);

    BIGOVERLAY.pos = function () {

        var width = big.overlay.width();
        var height = big.overlay.height();
        var scrollTop = $.doc.scrollTop();

        var white_css = {
            width: width + 30,
            height: height + 30,
            left: -15,
            top: -15
        };

        big.white1.css(white_css);
        big.white2.css(white_css);

        var left = $.win.width() / 2 - big.overlay.width() / 2;
        var top = $.win.height() / 2 - big.overlay.height() / 2 + scrollTop;

        left = Math.max(0, left);
        top = Math.max(0, top);

        big.overlay.css({
            top: top,
            left: left
        });
    };

    BIGOVERLAY.show = function (titel, beschreibung) {

        big.overlay.hide();

        $(document.body).append(big.overlay);
        $(document.body).append(big.prevent_page);

        big.titel.html(titel);
        big.beschreibung.html(beschreibung);

        TweenMax.fromTo(big.overlay, ANIMID, {
            display: 'block',
            alpha: 0
        }, {
            alpha: 1
        });

        $.win.on("viewport.Bigoverlay", BIGOVERLAY.pos);
        BIGOVERLAY.pos();
        big.prevent_page.on("click", BIGOVERLAY.leave);
        big.close.on("click", BIGOVERLAY.leave);

    };

    BIGOVERLAY.leave = function () {

        big.close.off("click");
        big.prevent_page.off("click");

        TweenMax.to(big.overlay, ANIMID, {
            alpha: 0,
            onComplete: function () {
                big.overlay.remove();
                big.prevent_page.remove();
            }
        });

        $.win.off("pageresize.bigoverlay", BIGOVERLAY.pos);

    }
    ;

})(jQuery);

/**
 * Sticky Season Button
 */

(function ($) {

    var sticky = false;
    var stateChange = false;
    var $btn = $(".header_wrapper .season_toggle_btn");
    var $parent = $(".header_space");
    var scrollTop = 0;
    var btnTop = 0;

    function setPos() {
        var fixedLeft = $parent.width() + $parent.offset().left;
        $btn.css("left", fixedLeft - $btn.width());
    }

    function makeSticky() {
        if (stateChange) {
            stateChange = false;
            if (sticky) {
                $btn.css({
                    position: "fixed",
                    right: "auto"
                });
                setPos();
            } else {
                $btn.css({
                    position: "absolute",
                    left: "",
                    right: ""
                });
            }
        }
    }

    function checkOffsetTop() {

        scrollTop = $.win.scrollTop();

        if (!sticky) {
            btnTop = $btn.offset().top;
        }

        if (scrollTop > btnTop && !sticky) {
            sticky = true;
            stateChange = true;
        } else if (scrollTop <= btnTop && sticky) {
            sticky = false;
            stateChange = true;
        }

        makeSticky();

    }

    function init() {
        if ($btn.length > 0) {
            $.win.on("scroll", checkOffsetTop);
        }
    }

    $(init);

})(jQuery);
