<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:15
 */

require("CyClasses/CyModul.php");
require("CyClasses/CyTheme.php");

//include("functions/custom_taxonomies.php"); IN PLUGIN AUSGELAGERT!
//include("functions/disable_schrott.php"); IN PLUGIN AUSGELAGERT!

CyModul::add_script(get_template_directory_uri() . "/scripts/TweenMax.min.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/jquery-3.2.1.min.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/cy-jquery.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/global.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/detailseiten.js");
CyModul::add_script(get_template_directory_uri() . "/scripts/Viewport.js");

function _date($format="r", $timestamp=false, $timezone=false)
{
    $userTimezone = new DateTimeZone(!empty($timezone) ? $timezone : 'GMT');
    $gmtTimezone = new DateTimeZone('GMT');
    $myDateTime = new DateTime(($timestamp!=false?date("r",(int)$timestamp):date("r")), $gmtTimezone);
    $offset = $userTimezone->getOffset($myDateTime);
    return date($format, ($timestamp!=false?(int)$timestamp:$myDateTime->format('U')) + $offset);
}

add_action("wp", function() {
    //echo "debug";
});

//$user_id = username_exists( "debug" );
//$userdata = array(
//    'ID' => $user_id,
//    'user_pass' => 'development',
//    'role' => 'administrator'
//);
//wp_update_user($userdata);