<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulSpecialKayano extends CyModul
{

    public $data = [];
    public $path = "";

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {
        $this->data = get_field('page_content');
        //CyTheme::printt(get_field('page_content'));
    }

    private function load_component($str, $data = [])
    {
        extract($data);
        $_file = dirname(__FILE__) . '/components/' . $str . '.php';
        if (file_exists($_file))
            include $_file;
    }

    private function catch_component($str, $data = [])
    {
        ob_start();
        $this->load_component($str, $data);
        return ob_get_clean();
    }

    public function print_module()
    {

        $content = "";

        $sectionAnchors = Array();
        $sectionId = 0;

        foreach ($this->data as $_section) {
            $componentContent = "";
            $sectionAnchors[] = $_section['anchor_name'];
            $sectionId++;
            foreach ($_section['section'] as $_component) {
                $componentLayout = $_component['acf_fc_layout'];
                $componentContent .= $this->catch_component($componentLayout, $_component);
            };
            $content .= $this->catch_component('section', Array(
                'content' => $componentContent,
                'style' => $_section['style'],
                'sectionId' => $sectionId
            ));
        }

        echo $this->load_component('master', Array(
            'content' => $content,
            'sectionAnchors' => $sectionAnchors
        ));

    }

}

new ModulSpecialKayano();

?>