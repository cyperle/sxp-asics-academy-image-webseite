<!-- Module Special:Block -->
<div class="<?php echo ($block_width === 'extended') ? 'special_block' : 'special_block_compact'; ?> padding-wrapper">

    <?php if (!empty($block_heading)) : ?>
        <div class="special_headline_skewed">
            <div class="special_headline_skewed_skew">
                <div class="special_headline_skewed_text"><?php echo $block_heading; ?></div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($text_above)) : ?>
        <div class="padding-wrapper">
            <div class="special_block_text_above paragraphs">
                <?php echo (new Parsedown())->text($text_above); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($block_image) || !empty($block_text)) : ?>
        <div class="special_block_content padding-wrapper">

            <div class="special_block_left pointer-ignore">
                <?php if (!empty($block_image)) : ?>
                    <img src="<?php echo $block_image; ?>"
                         class="special_block_left_image">
                <?php endif; ?>
            </div>

            <div class="special_block_right paragraphs">
                <?php if (!empty($block_text)) : ?>
                    <?php echo (new Parsedown())->text($block_text); ?>
                <?php endif; ?>
            </div>

        </div>
    <?php endif; ?>
</div>