<!-- Module Special: Technologies -->
<div class="special_technologies padding-wrapper">
    <div class="special_technologies_padding padding-wrapper">

        <?php if(!empty($image)) : ?>
        <div class="special_technologies_illustration pointer-ignore">
            <img class="block img-responsive" src="<?php echo $image; ?>" alt="">
        </div>
        <?php endif; ?>

        <div class="special_technologies_details paragraphs">
            <?php if (!empty($text)) : ?>
                <?php echo (new Parsedown())->text($text); ?>
            <?php endif; ?>
        </div>
    </div>
</div>