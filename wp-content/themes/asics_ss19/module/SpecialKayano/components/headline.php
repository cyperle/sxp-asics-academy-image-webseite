<!-- Module Special:Headline -->
<?php if (!empty($head) || !empty($subhead)) : ?>
    <div class="special_headline">
        <?php if (!empty($head)) : ?>
            <div class="special_headline_sub"><?php echo $head; ?></div>
        <?php endif; ?>
        <?php if (!empty($subhead)) : ?>
            <div class="special_headline_main"><?php echo $subhead; ?></div>
        <?php endif; ?>
    </div>
<?php endif; ?>