<!-- Module Special:Text -->
<?php if (!empty($text_content)) : ?>
    <div class="special_text padding-wrapper padding-zero-mobile paragraphs">
        <div class="padding-wrapper">
            <?php echo (new Parsedown())->text($text_content); ?>
        </div>
    </div>
<?php endif; ?>