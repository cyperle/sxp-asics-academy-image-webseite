<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 02.06.2017
 * Time: 16:08
 */
class ModulSimple extends CyModul
{

    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function set_module()
    {
        while (have_posts()) : the_post();
            $this->data['title'] = get_the_title();
            $this->data['content'] = apply_filters('the_content', get_the_content());
        endwhile;
    }

    public function print_module()
    {

        ?>

        <article class="page_simple">

            <div class="inner_wrapper">

                <div class="simple_content">

                    <header>
                        <h1 class="page_title"><?php echo $this->data['title']; ?></h1>
                    </header>
                    <section class="content paragraphs">
                        <?php echo $this->data['content']; ?>
                    </section>

                </div>

            </div>

        </article>

        <?php

    }

}

new ModulSimple();