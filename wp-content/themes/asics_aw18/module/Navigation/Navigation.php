<?php

class ModulNavigation extends CyModul
{

    public $data = [];
    public $breadhtml = "";

    public function __construct()
    {

        $this->set_module();
        $this->print_module();

    }

    public function set_module()
    {
        $this->determine_breadcrumbs();
    }

    private function init_cat($key)
    {

        return Asics::getHomeUrl() . '?' . parent::$init_cat_key . '=' . $this->lowercase($key);
    }

    private function determine_breadcrumbs()
    {

        $breadhtml = "";
        $breaddata = [];

        @ $techTranslate = Asics::getTechTranslation(get_field('highlight')->slug);

        if (get_page_template_slug() == 'page-product-overview.php') {

            $cat = get_field('categories');
            $prog = get_field('programs');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $breaddata['program'] = Array(
                'name' => $prog->name,
                'permalink' => Asics::getOverviewUrl($cat->slug, $prog->slug)
            );

            if (!empty($cat) && !empty($prog)) {

                $breaddata['layout'] = get_field('layout', $prog);

                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';

                $breadhtml .= '<div class="bread_item bread_last ' . $breaddata['layout'] . '"><span>' . $breaddata['program']['name'] . '</span></div>';

            }

        } else if (get_page_template_slug() == 'page-tech-overview.php') {

            $cat = get_field('categories');
            $prog = get_field('type');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $breaddata['program'] = Array(
                'name' => $techTranslate['headline']
            );

            if (!empty($cat) && !empty($prog)) {

                $breaddata['layout'] = 'silver';

                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';

                $breadhtml .= '<div class="bread_item bread_last ' . $breaddata['layout'] . '"><span>' . $breaddata['program']['name'] . '</span></div>';

            }

        } else if (get_page_template_slug() == 'page-product-single.php') {

            $cat = get_field('categories');
            $prog = get_field('programs');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $breaddata['program'] = Array(
                'name' => $prog->name,
                'permalink' => Asics::getOverviewUrl($cat->slug, $prog->slug)
            );

            $stage = get_field('stage');
            if ($stage[0]['name']) {
                $name = $stage[0]['name'];
            } else {
                $name = null;
            }

            $breaddata['product'] = Array(
                'name' => $name
            );

            if (!empty($breaddata['category']['name'])) {
                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';
            }

            if (!empty($breaddata['program']['name'])) {
                $breadhtml .= '<div class="bread_item bread_program"><a href="' . $breaddata['program']['permalink'] . '"><span>' . $breaddata['program']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';
            }

            if (!empty($breaddata['product']['name'])) {
                $breadhtml .= '<div class="bread_item bread_last"><span>' . $breaddata['product']['name'] . '</span></div>';
            }

        } else if (get_page_template_slug() == 'page-apparel-single.php') {

            $cat = get_field('categories');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->name),
                'permalink' => $this->init_cat($cat->name)
            );

            $stage = get_field('stage');
            if ($stage[0]['name']) {
                $name = $stage[0]['name'];
            } else {
                $name = null;
            }

            $breaddata['product'] = Array(
                'name' => $name
            );

            if (!empty($breaddata['category']['name'])) {
                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';
            }

            if (!empty($breaddata['product']['name'])) {
                $breadhtml .= '<div class="bread_item bread_last ' . $breaddata['layout'] . '"><span>' . $breaddata['product']['name'] . '</span></div>';
            }

        } else if (get_page_template_slug() == 'page-technology.php') {

            $cat = get_field('categories');
            $highlight = get_field('highlight');

            $breaddata['category'] = Array(
                'name' => $cat->name,
                'slug' => $this->lowercase($cat->slug),
                'permalink' => $this->init_cat($cat->name)
            );

            Asics::collectTechOverviews();
            $breaddata['program'] = Array(
                'name' => $techTranslate['headline'],
                'permalink' => Asics::getTechOverviewUrl($highlight->slug)
            );

            $name = CyTheme::clipTechName(get_field('name'), get_field('short_name'));
            $breaddata['techname'] = $name;
            $breaddata['product'] = Array(
                'name' => $name
            );

            if (!empty($breaddata['category']['name'])) {
                $breadhtml .= '<div class="bread_item bread_category"><a href="' . $breaddata['category']['permalink'] . '"><span>' . $breaddata['category']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';
            }

            if (!empty($breaddata['program']['name'])) {
                $breadhtml .= '<div class="bread_item bread_program"><a href="' . $breaddata['program']['permalink'] . '"><span>' . $breaddata['program']['name'] . ' <i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div>';
            }

            if (!empty($breaddata['product']['name'])) {
                $breadhtml .= '<div class="bread_item bread_last"><span>' . $breaddata['product']['name'] . '</span></div>';
            }

        }

        if ($breadhtml !== "") {
            $breadhtml = '<div class="breadcrumbs noselect"><div class="inner_wrapper"><div class="bread_inner">' . $breadhtml . '</div></div></div>';
        }

        $this->data['translations'] = $techTranslate;
        $this->breadhtml = $breadhtml;
        $this->data['breadcrumbs'] = $breaddata;
        CyCache::set("breadcrumbs", $breaddata);

    }

    public function print_module()
    {

        $attr = htmlAttr(Array(
            "class" => Array(
                'topbar',
                Asics::getSpecialTopbarScheme()
            )
        ));

        ?>

        <div <?php echo $attr; ?>>

            <div class="topbar_wrapper inner_wrapper">

                <div class="topbar_inner">

                    <header class="topbar_left">

                        <a class="topbar_logo" href=" <?php echo Asics::getHomeUrl(); ?>"
                           title="Zur Asics-Startseite">
                            <h1 class="hide">Asics</h1>
                            <svg class="topbar_svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 174.6 56.7" enable-background="new 0 0 174.6 56.7" xml:space="preserve">
<path fill="#FFFFFF" d="M17.6,40.2c-3.2,0-4.8-2.5-4-5.4c1.7-6,12.4-14.8,19.8-14.8c5.4,0,4.9,4.8,1.8,8.9l-1.7,1.9
	C27.3,36.9,21.5,40.2,17.6,40.2 M43.9,0c-9.4,0-19.7,5.7-26.2,11.2l0.5,0.7c10.1-7,24-11.6,27.9-4.8c2,3.6-1.4,11-6.3,16.7
	c1.2-2.6,0.4-7.5-6.4-7.5C22.2,16.4,0,31.3,0,46.6c0,6.1,4.2,10.1,11.2,10.1c18.7,0,44.1-30.6,44.1-46C55.3,5.2,52.1,0,43.9,0"/>
                                <path fill="#FFFFFF" d="M85.2,16.2c-1.7-2.1-4.5-3.1-6.5-3.1H67.8L65.9,20h10.3l1,0.1c0,0,1,0.1,1.5,0.8c0.4,0.6,0.5,1.5,0.2,2.6
	l-0.5,1.8h-6.5c-5.6,0-13.4,4-15.1,10.5c-0.9,3.2-0.1,6,1.6,8.1c1.7,2,4.8,3.2,8.2,3.2h13.7l3.6-13.4l2.6-9.8
	C87.5,20,86.3,17.6,85.2,16.2z M74.4,40.2h-6.5c-1.1,0-2-0.4-2.6-1.1c-0.6-0.7-0.8-1.8-0.5-2.8c0.6-2.3,3.1-4.1,5.7-4.1h6L74.4,40.2
	z M159.8,26.3c-2.4-1.3-4.8-2.6-4.3-4.1c0.2-0.9,1.2-2.2,3.1-2.2h9.9l1.9-6.9h-10c-3,0-5.7,0.9-7.9,2.5c-2.3,1.7-3.5,3.7-4.2,6.4
	c-1.6,6,3.1,8.9,6.9,11c2.7,1.5,5.1,2.7,4.6,4.6c-0.4,1.4-1,2.5-4.3,2.5h-10.2l-1.9,6.9h11.3c3,0,5.8-0.6,8.1-2.4
	c2.3-1.7,3.5-4.2,4.3-7.1c0.8-2.8,0.2-5.5-1.6-7.5C163.9,28.5,161.7,27.3,159.8,26.3z M106.1,30.2c-1.5-1.7-3.7-2.8-5.6-3.9
	c-2.4-1.3-4.8-2.6-4.3-4.1c0.2-0.9,1.2-2.2,3.1-2.2h9.9l1.9-6.9h-10c-3,0-5.7,0.9-7.9,2.5c-2.3,1.7-3.5,3.7-4.2,6.4
	c-1.6,6,3.1,8.9,6.9,11c2.7,1.5,5.1,2.7,4.6,4.6c-0.4,1.4-1,2.5-4.3,2.5H85.8L84,47.1h11.3c3,0,5.8-0.6,8.1-2.4
	c2.3-1.7,3.5-4.2,4.3-7.1C108.4,34.9,107.9,32.2,106.1,30.2z M116.5,13.1l-8.9,34h7.5l8.9-34H116.5z M142.5,20h4l1.9-6.9h-4.5
	c-13.2,0-20.1,8.8-22.3,17c-2.6,9.7,2.6,17,12,17h6.3l1.9-6.9h-6.3c-3.5,0-8.4-2.8-6.4-10.1C130.6,24.3,136.3,20,142.5,20z
	 M173.6,41.3c-0.7-0.7-1.5-1-2.4-1c-0.9,0-1.7,0.3-2.4,1c-0.7,0.7-1,1.5-1,2.4c0,0.9,0.3,1.8,1,2.4c0.7,0.7,1.5,1,2.4,1
	c0.9,0,1.7-0.3,2.4-1c0.7-0.7,1-1.5,1-2.4C174.6,42.8,174.3,42,173.6,41.3z M173.3,45.8c-0.6,0.6-1.3,0.9-2.1,0.9
	c-0.8,0-1.5-0.3-2.1-0.9c-0.6-0.6-0.8-1.3-0.8-2.1c0-0.8,0.3-1.5,0.9-2.1c0.6-0.6,1.3-0.9,2.1-0.9c0.8,0,1.5,0.3,2.1,0.9
	c0.6,0.6,0.9,1.3,0.9,2.1C174.1,44.5,173.8,45.2,173.3,45.8z M172.7,45.3c0-0.1,0-0.2,0-0.3v-0.3c0-0.2-0.1-0.4-0.2-0.6
	c-0.1-0.2-0.4-0.3-0.6-0.4c0.2,0,0.4-0.1,0.5-0.2c0.2-0.2,0.4-0.4,0.4-0.7c0-0.5-0.2-0.8-0.6-0.9c-0.2-0.1-0.6-0.1-1-0.1h-1.3v3.7
	h0.7v-1.5h0.5c0.3,0,0.6,0,0.7,0.1c0.2,0.1,0.4,0.4,0.4,0.9v0.3l0,0.1c0,0,0,0,0,0c0,0,0,0,0,0h0.6l0,0
	C172.7,45.5,172.7,45.4,172.7,45.3z M171.7,43.6c-0.1,0.1-0.3,0.1-0.6,0.1h-0.6v-1.4h0.6c0.4,0,0.6,0,0.8,0.1
	c0.2,0.1,0.3,0.3,0.3,0.5C172.1,43.3,172,43.5,171.7,43.6z"/>
</svg>
                        </a>

                    </header>

                    <div class="topbar_right">

                        <ul class="topbar_nav">

                            <?php $cat = get_terms('sociallinks', 'orderby=count&hide_empty=0'); ?>

                            <li class="topnav_item hide_on_search">
                                <a class="topnav_btn" href="<?php echo $cat[1]->description; ?>" target="_blank">
                                    <div class="topnav_icon">
                                        <div class="topnav_icon_instagram">

                                            <svg class="topbar_svg" version="1.1" id="Capa_1"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 510 510" enable-background="new 0 0 510 510"
                                                 xml:space="preserve">
<g>
    <g>
        <path d="M459,0H51C23,0,0,23,0,51v408c0,28,23,51,51,51h408c28,0,51-23,51-51V51C510,23,487,0,459,0z M255,153
			c56.1,0,102,45.9,102,102s-45.9,102-102,102s-102-45.9-102-102S198.9,153,255,153z M63.8,459c-7.7,0-12.8-5.1-12.8-12.8V229.5
			h53.6c-2.6,7.6-2.6,17.9-2.6,25.5c0,84.1,68.9,153,153,153s153-68.9,153-153c0-7.6,0-17.9-2.5-25.5H459v216.8
			c0,7.6-5.1,12.8-12.8,12.8H63.8z M459,114.8c0,7.7-5.1,12.8-12.8,12.8h-51c-7.6,0-12.8-5.1-12.8-12.8v-51
			c0-7.7,5.1-12.8,12.8-12.8h51c7.6,0,12.8,5.1,12.8,12.8V114.8z"/>
    </g>
</g>
</svg>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li class="topnav_item hide_on_search">
                                <a class="topnav_btn" href="<?php echo $cat[0]->description; ?>">
                                    <div class="topnav_icon">
                                        <div class="topnav_icon_facebook"></div>
                                    </div>
                                </a>
                            </li>

                            <li id="search" class="topnav_search topnav_item">

                                <div class="search_cell contains_search_btn">

                                    <div class="search_btn">
                                        <div class="search_padding">
                                            <div class="topnav_btn topnav_btn_search">
                                                <div class="topnav_icon topnav_search_icon"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="search_cell">

                                    <div class="search_form_ani">
                                        <div class="search_form_wrapper hide">

                                            <div class="search_padding">

                                                <div class="search_form_cell">
                                                    <div class="search_input_wrapper">
                                                        <?php
                                                        echo get_search_form();
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="search_form_cell">
                                                    <div class="topnav_btn topnav_btn_close close_ref">
                                                        <div class="topnav_icon topnav_search_close close">
                                                            <b></b><b></b></div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div id="search-result">
                                    <ul></ul>
                                </div>

                            </li>

                            <li class="topnav_item burger">
                                <div class="topnav_btn topnav_burger">
                                    <div class="topnav_icon topnav_icon_burger">
                                        <svg class="topbar_svg" version="1.1" id="Ebene_1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 246.4 139.8" enable-background="new 0 0 246.4 139.8"
                                             xml:space="preserve">
<rect width="246.4" height="13.3"/>
                                            <rect y="31.6" width="246.4" height="13.3"/>
                                            <rect y="63.3" width="246.4" height="13.3"/>
                                            <rect y="94.9" width="246.4" height="13.3"/>
                                            <rect y="126.5" width="246.4" height="13.3"/>
</svg>
                                    </div>
                                </div>
                            </li>

                        </ul>

                    </div>

                </div><!-- .topbar_inner -->

            </div><!-- .topbar_wrapper -->

        </div><!-- .topbar -->


        <section class="sidebar_wrapper">

            <div class="sidebar_top">

                <div class="sidebar_top_inner">

                    <?php if (Asics::getLangs() !== null) :

                        $l = count(Asics::getLangs());
                        ?>

                        <div class="sidebar_top_cell lang">
                            <ul class="lang_list <?php if ($l < 2) echo "disabled"; ?>">

                                <?php

                                $i = 0;

                                foreach (Asics::getLangs() as &$lang) : ?>

                                    <li class="lang_item i_<?php echo $i; ?> <?php if ($i < 1) {
                                        echo 'has_selection';
                                    } else {
                                        echo 'is_option';
                                    } ?>">

                                        <?php if ($i > 0) {
                                            echo '<a href="' . $lang['url'] . '" class="lang_item_link" title="' . $lang['native_name'] . '">';
                                        } else {
                                            echo '<div class="lang_item_link">';
                                        }

                                        ?>

                                        <div class="lang_item_holder">
                                                <span class="country_flag"
                                                      style="background:url('<?php echo $lang['country_flag_url']; ?>')"></span>
                                            <span class="lang_text">
													<?php echo $lang['tag']; ?>
												</span>
                                        </div>

                                        <?php if ($i > 0) {
                                            echo '</a>';
                                        } else {
                                            echo '</div>';
                                        } ?>

                                    </li>

                                    <?php

                                    $i++;
                                endforeach;
                                ?>
                            </ul>
                        </div>

                    <?php endif; ?>

                    <?php if (class_exists("AsicsToggleSeason")) :
                        if (in_array(Asics::getLang(), ["en", "de", "es", "fr", "it", "pt"])) :
                            ?>

                            <div class="sidebar_top_cell season">

                                <div class="season_list">
                                    <div class="season_item" <?php echo AsicsToggleSeason::data("SS 2019"); ?>>
                                        <div class="season_item_holder">
                                            <div class="season_icon">
                                                <?php AsicsToggleSeason::echoSvg(); ?>
                                            </div>

                                            <div class="season_text">
                                                <?php AsicsToggleSeason::echoLabel("SS 2019", Asics::getLang()); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        endif;
                    endif;
                    ?>

                    <div class="sidebar_top_cell closebtn">

                        <div class="sidebar_close">
                            <div class="topnav_btn close_ref">
                                <div class="topnav_icon close"><b></b><b></b></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="sidebar_content">

                <?php

                $error_at_overviews = false;
                $my_prog = "";
                $my_cat = "";
                $my_type = "";
                if (!empty($this->data['breadcrumbs']['program']['name'])) {
                    $my_prog = $this->data['breadcrumbs']['program']['name'];
                    $my_cat = $this->data['breadcrumbs']['category']['name'];
                    if (isset($this->data['breadcrumbs']['type'])) {
                        $my_type = $this->data['breadcrumbs']['type']['name'];
                    }
                }

                CyTheme::collectAllTechs();
                $techs = CyTheme::$techs;

                foreach (Asics::getOverviews() as &$category) :

                    if ($category['name'] != ''):

                        $attr = htmlAttr(Array(
                            "class" => [
                                "sidebar_section",
                                str_replace(" ", "_", strtolower($category['css-name'])),
                                ($my_cat === $category["name"]) ? "active" : ""
                            ]
                        ));

                        ?>

                        <div <?php echo $attr; ?>>

                            <div class="sidebar_kategorie_holder">
                                <div class="sidebar_kategorie"><?php echo $category['name']; ?></div>
                            </div>

                            <div class="sidebar_programme">

                                <ul class="sidebar_programme_list clearfix">

                                    <?php foreach ($category['programs'] as &$program) :

                                        $attr = htmlAttr(Array(
                                            "class" => ["sidebar_programm_link", $program['layout'], ($my_prog === $program["name"]) ? "active" : ""],
                                            "href" => $program['permalink']
                                        ));

                                        ?>

                                        <li class="sidebar_programm">
                                            <a <?php echo $attr; ?>>
                                                            <span class="sidebar_programm_text"><i
                                                                        class="fa fa-caret-right"
                                                                        aria-hidden="true"></i>&nbsp;<?php echo $program['name']; ?></span>
                                            </a>
                                        </li>

                                    <?php endforeach; ?>


                                </ul>

                            </div>

                        </div>

                        <?php
                    endif;
                endforeach;
                ?>

                <!--TECH HIGHLIGHTS-->
                <?php if (count($techs) > 0):

                    $attr = htmlAttr(Array(
                        "class" => ["sidebar_section", "tech_highlights", ($my_cat === $category["name"]) ? "active" : ""]
                    ));

                    ?>
                    <div <?php echo $attr; ?>>

                        <div class="sidebar_kategorie_holder">
                            <div class="sidebar_kategorie">
                                <?= __('Technology Highlights', 'CyTheme@Asics'); ?>
                            </div>
                        </div>

                        <div class="sidebar_programme">

                            <ul class="sidebar_programme_list clearfix">

                                <?php foreach ($techs as &$tech) :

                                    $activeClass = "";
                                    if (isset($this->data['breadcrumbs']['techname'])) {
                                        if ($this->data['breadcrumbs']['techname'] === $tech["name"]) {
                                            $activeClass = "active";
                                        }
                                    }

                                    $attr = htmlAttr(Array(
                                        "class" => ["sidebar_programm_link", $activeClass],
                                        "href" => $tech['permalink']
                                    ));

                                    ?>

                                    <li class="sidebar_programm">
                                        <a <?php echo $attr; ?>>
                                                        <span class="sidebar_programm_text"><i
                                                                    class="fa fa-caret-right"
                                                                    aria-hidden="true"></i>&nbsp;<?php echo $tech['name']; ?></span>
                                        </a>
                                    </li>

                                <?php endforeach; ?>

                            </ul>

                        </div>

                    </div>
                <?php endif; ?>
            </div>

        </section><!-- .sidebar_wrapper -->

        <?php echo $this->breadhtml; ?>

        <?php
        parent::add_script(get_template_directory_uri() . "/module/Navigation/scripts/Navigation.js");

    }

}

new ModulNavigation();