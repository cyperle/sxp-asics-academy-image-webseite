var KategorieProgramm = KategorieProgramm || {};

(function ($) {

    var win = $.win;

    var shoes_menu, apparel_menu, shoes_text, apparel_text;

    var pItems = new Array();

    KategorieProgramm.initialized = false;

    /**
     * Initialisiert die Funktionalität der einzelnen Programm-Elemente.
     */
    function fn_programme() {

        /**
         * Verfügbare Farben der Programme. Dies wird benötigt, um die Farbe des großen Overlays zu bestimmen.
         * @type {[*]}
         */
        var available_colors = ["dunkelblau", "dunkelgruen", "hellblau", "hellgruen"];

        /**
         * Diese Klasse dient als Controller für die Programm-Elemente im DOM.
         * @param el
         */
        function Programm_Item(el) {

            this.$el = $(el);
            this.titel = this.$el.find(".text_name").text();
            this.$overlay = this.$el.find(".overlay_konzept");
            this.$btn_konzept = this.$el.find(".btn_konzept");
            this.$btn_programm = this.$el.find(".btn_programm");
            this.$btn_close = this.$el.find(".close");
            //this.$verlauf = this.$el.find(".verlauf");
            this.$name = this.$el.find(".holder_name");
            this.$tween = {};

            //this.$btn_konzept.on("click", this.display_overlay.bind(this));
            this.$btn_konzept.onEnterClick(this.display_overlay.bind(this));

            this.$beschreibung_div = this.$el.find(".overlay_konzept_beschreibung");
            this.$beschreibung_holder = this.$el.find(".overlay_konzept_beschreibung_holder");
            this.$beschreibung_text_div = this.$el.find(".overlay_konzept_beschreibung_text");

            this.beschreibung_html = this.$beschreibung_text_div.html();
            this.beschreibung_text = this.$beschreibung_text_div.text();
            this.beschreibung_words = this.beschreibung_text.split(" ");
            this.$weiterlesen = $('<span class="weiterlesen">').html("...&nbsp;" + readmore + "&nbsp;");

            this.color = this.$el.determineClass(available_colors);
            this.side = this.$el.determineClass(["left", "right"]);

            this.overlay_status = "closed"; // rendering, open
            this.overlay_rendered = false;

            Programm_Item.item_list.push(this);

        }


        Programm_Item.item_list = [];

        Programm_Item.prototype = {

            /**
             * Zeigt das große Overlay für das Programm-Element an.
             */
            show_big_overlay: function () {

                BIGOVERLAY.show(this.titel, this.beschreibung_html, this.color);

            },

            /**
             * Blendet das Standard-Overlay ein.
             */
            display_overlay: function () {

                this.overlay_status = "rendering";

                //this.$btn_konzept.off("click");
                this.$btn_konzept.offEnterClick();
                //this.$btn_close.on("click", this.hide_overlay.bind(this));
                this.$btn_close.onEnterClick(this.hide_overlay.bind(this));
                this.$weiterlesen.onEnterClick(this.show_big_overlay.bind(this));

                this.$overlay.show();

                //Falls die Tweens noch nicht existieren, erstelle diese. Ansonsten spiele die vorhandenen VORWÄRTS ab.
                if (typeof this.$tween.overlay === "undefined") {

                    this.$tween.overlay = TweenMax.to(this.$overlay, ANIMID, {
                        x: "0%",
                        opacity: 1
                    });

                    this.$tween.fade = TweenMax.to([this.$name, this.$btn_programm], ANIMID, {
                        opacity: "0"
                    });

                    this.$tween.konzept = TweenMax.to(this.$btn_konzept, ANIMID, {
                        color: this.$overlay.css("background-color"),
                        backgroundColor: this.$overlay.css("color")
                    });

                } else {

                    $.each(this.$tween, function () {
                        this.play();
                    });

                }

                this.render_beschreibung();

                $.each(Programm_Item.item_list, function () {
                    if (this.overlay_status === "open") {
                        this.hide_overlay();
                    }
                });

                this.overlay_status = "open";

            },

            /**
             * Blendet das Standard-Overlay aus.
             */
            hide_overlay: function () {

                this.$btn_konzept.onEnterClick(this.display_overlay.bind(this));
                this.$btn_close.offEnterClick();
                this.$weiterlesen.offEnterClick();

                //Spiele die vorhandenen Tweens RÜCKWÄRTS ab, um zu den Ausgangswerten zurückzukehren.
                $.each(this.$tween, function () {
                    this.reverse();
                });

                this.overlay_status = "closed";

            },

            /**
             * Rendert den String der Beschreibung im Standard-Overlay und zeigt bei Überlänge einen "weiterlesen"-Button an, der das große Overlay öffnet.
             */
            render_beschreibung: function () {
                this.overlay_rendered = false;

                this.$beschreibung_text_div.text(this.beschreibung_text);
                if (this.overlay_rendered) {
                    return;
                }

                var _this = this,
                    beschreibung_height = this.$beschreibung_div.height(),
                    words = this.beschreibung_words,
                    current_text = "",
                    tmp_text = null,
                    cut = false;

                this.$beschreibung_holder.append(this.$weiterlesen);


                function validate_height() {
                    return _this.$beschreibung_holder.height() < beschreibung_height;
                }


                if (!validate_height()) {

                    var i;
                    for (i = 0; i < words.length; i++) {

                        tmp_text = current_text + " " + words[i];
                        this.$beschreibung_text_div.text(tmp_text);

                        if (validate_height()) {
                            current_text = tmp_text;
                        } else {
                            this.$beschreibung_text_div.text(current_text);
                            cut = true;
                            break;
                        }

                    }

                }

                if (!cut) {
                    this.$weiterlesen.remove();
                } else {

                }

                this.overlay_rendered = true;

            }

        };


        /**
         * Selektiert jedes Programm-Element und erzeugt ein Programm_Item-Objekt.
         */
        $(".nav_programm_item").each(function (index, el) {
            pItems.push(new Programm_Item(el));

        });

        $("#nav_programm").addClass("anzahl-" + Programm_Item.item_list.length);

        $.win.resize(function () {
            //resize_start();
            $.each(pItems, function (key, value) {
                value.render_beschreibung();
            });
        });
    }

    /**
     * Zählt die Anzahl der Programme in einer Liste und fügt eine entsprechende Klasse hinzu.
     */
    function fn_child_count() {
        $(".nav_programm_list").each(function (index, element) {
            var _this = $(this);
            var children = _this.children().length;
            _this.addClass("anzahl-" + children);
        });
    }

    /**
     * Initialisiert die Logik zum Wechsel zwischen Kategorien und Typen.
     */
    function fn_switcher() {

        var initial_typ = '';
        var initial_kategorie = '';

        var current_typ = null;
        var current_kategorie = null;

        /**
         * Typen (Shoes, Apparel)
         */
        function Typ(name, active) {

            this.name = name;
            this.active = active;
            this.startbtn = $('.startstreen_btn[data-typ=' + name + ']:not(.disabled)');
            this.navbtn = $('.nav_typ_item[data-typ=' + name + ']');

            if (active) {

                this.navbtn.onEnterClick(this.show.bind(this));
                this.startbtn.onEnterClick(this.show.bind(this));

            } else {

                this.startbtn.addClass('disabled');
                this.navbtn.addClass('disabled');

            }

            Typ.instances.push(this);

        }

        Typ.instances = [];

        Typ.show = function (name, triggerKategorie) {

            $.each(Typ.instances, function (index, instance) {

                //Angeforderter Typ entspricht der Instanz
                if (instance.name === name) {

                    //Setze Instanz als aktuellen Typ
                    current_typ = instance;

                    $('.multi-cat').css('display', 'none');
                    $('.nav_typ').addClass('show');

                    instance.navbtn.addClass('active');
                    instance.navbtn.offEnterClick();

                    //Zeige Elemente der aktuellen Kategorie abhängig davon, um welchen Typ es sich handelt.

                    current_kategorie.programmHolder.addClass("show");
                    if (name === "shoes") {
                        current_kategorie.programmShoes.list.addClass("show");
                        current_kategorie.programmApparels.list.removeClass("show");
                    } else if (name === "apparel") {
                        current_kategorie.programmApparels.list.addClass("show");
                        current_kategorie.programmShoes.list.removeClass("show");
                    }

                } else {

                    instance.navbtn.removeClass('active');
                    if (instance.active) {
                        instance.navbtn.offEnterClick();
                        instance.navbtn.onEnterClick(instance.show.bind(instance));
                    }

                }

            });

            /*if (current_kategorie !== null && triggerKategorie !== false) {
             current_kategorie.show();
             }*/

        };

        Typ.prototype = {
            show: function () {
                if (!this.navbtn.hasClass("disabled")) {
                    Typ.show(this.name);
                }
            }
        };

        /**
         * Kategorien
         */
        function Kategorie(btn) {

            this.btn = btn;
            this.name = this.btn.data('kategorie'); //zb Running
            if (this.name == "") {
                return;
            }

            this.programmShoes = new ProgrammTyp('shoes', this.name);
            this.programmApparels = new ProgrammTyp('apparel', this.name);
            this.hasShoes = (this.programmShoes.list.length > 0);
            this.hasApparels = (this.programmApparels.list.length > 0);
            this.programmHolder = $('.nav_programm[data-kategorie=' + this.name + ']');

            console.log(this.programmShoes)

            this.btn.onEnterClick(this.show.bind(this));
            Kategorie.instances.push(this);

        }

        Kategorie.show = function (name) {

            var categorySwitched = false;

            $.each(Kategorie.instances, function (index, instance) {

                //Instanzname entspricht Parameter
                if (instance.name === name) {

                    instance.btn.addClass('active');

                    if (this.hasShoes) {
                        $('.nav_typ_item[data-typ="shoes"]').removeClass('disabled');
                    } else {
                        $('.nav_typ_item[data-typ="shoes"]').addClass('disabled');
                    }

                    if (this.hasApparels) {
                        $('.nav_typ_item[data-typ="apparel"]').removeClass('disabled');
                    } else {
                        $('.nav_typ_item[data-typ="apparel"]').addClass('disabled');
                    }

                    //Noch kein Typ gesetzt
                    if (current_typ === null) {
                        current_typ = "shoes";
                    }

                    //Apparel gesetzt, aber die Kategorie enthält keine Apparels
                    if (current_typ === "apparel" && instance.hasApparels) {

                    } else {
                        current_typ = "shoes";
                    }

                    //Eine Kategorie ist noch nicht gesetzt
                    if (current_kategorie === null) {

                        //Blende die Elemente der neuen Kategorie ein
                        $('.nav_typ_startscreen[data-kategorie=' + instance.name + ']').addClass('show');
                        current_kategorie = instance;
                        categorySwitched = true;

                    }

                    //Eine Kategorie ist bereits gesetzt
                    else {

                        //Die aktuelle Kategorie entspricht der angeforderten Kategorie
                        if (current_kategorie.name == name) {

                            //Nothing

                        }

                        //Die aktuelle Kategorie entspricht nicht der angeforderten Kategorie
                        else {

                            //Blende die Elemente der aktuellen Kategorie aus
                            $('.multi-cat').css('display', 'block');
                            $('.nav_typ').removeClass('show');
                            current_kategorie.programmShoes.list.removeClass('show');
                            current_kategorie.programmApparels.list.removeClass('show');
                            //$('.nav_typ_startscreen.' + current_kategorie.name).removeClass('show');
                            $('.nav_typ_startscreen[data-kategorie=' + current_kategorie.name + ']').removeClass('show');

                            //Blende die Elemente der neuen Kategorie ein
                            //$('.nav_typ_startscreen.' + instance.name).addClass('show');
                            $('.nav_typ_startscreen[data-kategorie=' + instance.name + ']').addClass('show');

                            //Setze die neue Kategorie als aktuelle Kategorie
                            current_kategorie = instance;
                            categorySwitched = true;

                        }

                    }

                }

                //Instanzname entspricht nicht dem Paremter
                else {

                    instance.btn.removeClass('active');
                    this.programmShoes.list.removeClass('show');
                    this.programmApparels.list.removeClass('show');
                    //$('.nav_typ_startscreen.' + instance.name).removeClass('show');
                    $('.nav_typ_startscreen[data-kategorie=' + instance.name + ']').removeClass('show');

                }

            });
        };

        Kategorie.instances = [];

        Kategorie.prototype = {
            show: function () {
                Kategorie.show(this.name);
            }
        };

        function ProgrammTyp(name, category) {

            if (name == "") return;
            if (category == "") return;

            this.name = name;
            this.holder = $('.nav_programm[data-kategorie=' + category + ']');
            this.list = this.holder.find('.nav_programm_list[data-typ=' + name + ']');
        }

        /**
         * Init Switcher
         */
        function Init() {

            var $auswahl_bereich = $('.auswahl_bereich');
            initial_kategorie = $auswahl_bereich.data('icat');
            initial_typ = $auswahl_bereich.data('itype');

            new Typ('apparel', true);
            new Typ('shoes', true);

            $('.nav_kategorie_item').each(function (index, btn) {
                new Kategorie($(btn));
            });

            var instance_found = false

            if (initial_kategorie !== 'none') {
                $.each(Kategorie.instances, function (index, instance) {
                    if (instance.name === initial_kategorie) {
                        instance.show();
                        instance_found = true;
                    }
                });
            }

            if (!instance_found) {
                if (Kategorie.instances.length > 0) {
                    Kategorie.instances[0].show();
                }
            }

        }

        Init();

    }

    /**
     * In der mobilen Ansicht werden alle Programm-Container wie ein "rechter" behandelt.
     */
    function onResizeProgrammItems() {

        var itemsLeft = $(".nav_programm_item.left");
        var itemsLeftHolderButtons = itemsLeft.find(".holder_buttons");
        var mobileView = false;

        function doReverseButtons() {

            itemsLeftHolderButtons.reverseChildren();

        }

        function handle() {

            //Wenn mobiler Viewport, Klasse von "left" zu "right" wechseln und die Buttons umdrehen.
            if (Viewport.is("vp0x640")) {

                if (!mobileView) {

                    mobileView = true;
                    itemsLeft.removeClass("left").addClass("right");
                    doReverseButtons();

                }

            } else {

                if (mobileView) {

                    mobileView = false;
                    itemsLeft.removeClass("right").addClass("left");
                    doReverseButtons();

                }

            }

        }

        $.win.on("viewport.programmItems", handle);
        $.win.on("initviewport.programmItems", handle);
        handle();

    }

    function init() {

        shoes_menu = $('.start_shoes');
        apparel_menu = $('.start_apparel');
        shoes_text = $('.start_shoes span');
        apparel_text = $('.start_apparel span');

        fn_programme();
        fn_child_count();
        fn_switcher();
        onResizeProgrammItems();

        KategorieProgramm.initialized = true;

    }

    $.win.on("initviewport", init);

})(jQuery);
