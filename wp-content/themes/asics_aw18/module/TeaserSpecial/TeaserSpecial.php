<?php

/**
 * Created by PhpStorm.
 * User: Perleberg
 * Date: 22.05.2017
 * Time: 15:20
 */
class ModulTeaserSpecial extends CyModul
{

    public $data = [];

    public function __construct()
    {
        $this->set_module();
        $this->create_css();
        $this->print_module();
    }

    public function set_module()
    {
        $datenArray = [];
        $datenArray['image_desktop'] = get_sub_field('image_desktop');
        $datenArray['image_mobile'] = get_sub_field('image_mobile');
        $datenArray['target_page'] = get_sub_field('target_page');
        if (!empty($datenArray['target_page'])) {
            $datenArray['targetUrl'] = get_permalink($datenArray['target_page']);
        }
        $datenArray['user_visibility'] = get_sub_field('user_visibility');
        $this->data = $datenArray;
    }

    private function create_css()
    {

        $style = '<style data-info="dynamic-header-styles">';

        if (!empty($this->data['image_desktop'])) {
            $style .= '.special_teaser_bg{background-image: url("' . $this->data['image_desktop'] . '")}';
        }

        if (!empty($this->data['image_mobile'])) {
            $style .= '.vp0x640 .special_teaser_bg{background-image: url("' . $this->data['image_mobile'] . '")';
        }

        $style .= '</style>';

        echo $style;

    }

    public function print_module()
    {

        global $asicsLogin;

        if (class_exists('AsicsLogin')) {
            $visibility = $this->data['user_visibility'];
            $visibility = explode('-', $visibility);
            $min = 0;
            $max = 100;
            foreach ($visibility as $tmp) {
                $tmp = intval($tmp);
                if ($min === 0) {
                    $min = $tmp;
                } else if ($max === 100) {
                    $max = $tmp;
                }
            }
            $currentUser = $asicsLogin->getUser();
            if (empty($currentUser)) {
                return;
            } else {
                $rank = $currentUser->rank;
                if (($rank >= $min && $rank <= $max) || $rank == 100) {

                } else {
                    return;
                }
            }
        } else {
            return;
        }

        if (
            !empty($this->data['image_desktop'])
            && !empty($this->data['image_mobile'])
            && !empty($this->data['targetUrl'])
        ) : ?>
            <a href="<?php echo $this->data['targetUrl']; ?>" class="special_teaser_wrapper">
                <div class="special_teaser_inner inner_wrapper">
                    <div class="special_teaser_space">
                        <div class="special_teaser_bg"></div>
                    </div>
                </div>
            </a>
        <?php endif;
    }

}

new ModulTeaserSpecial();