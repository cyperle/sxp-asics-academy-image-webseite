<?php


class ModulProduct extends CyModul
{

    public $data = [];
    public $type = "shoe"; //mögliche Werte: shoe, apparel

    public function __construct()
    {
        $this->set_module();
        $this->print_module();
    }

    public function is_apparel()
    {
        return $this->type == "apparel";
    }

    public function is_shoe()
    {
        return $this->type == "shoe";
    }

    public function set_module()
    {

        /**
         * Stelle fest, um welchen Produkttyp es sich handelt.
         */
        if (parent::$template_slug == "page-product-single.php") {
            $this->type = "shoe";
            $this->set_shoe();
        } else if (parent::$template_slug == "page-apparel-single.php") {
            $this->type = "apparel";
            $this->set_apparel();
        }


    }

    public function set_shoe()
    {

        $datenArray = [];
        $datenArray['stage'] = get_field('stage');
        $datenArray['program'] = get_field('programs');

        /* Es existiert ein weiteres Feld mit dem ähnlichen Schlüssel "category". Zur Verständlichkeit wird der Schlüssel der Programm-Kategorie-Relation hier diesmal mit dem Präfix "rel_" ausgestattet! */
        $datenArray['rel_category'] = get_field('categories');
        $datenArray['related_overview'] = Asics::getOverviewUrl($datenArray['rel_category']->slug, $datenArray['program']->slug);

        if (!empty($datenArray['program'])) {

            $datenArray['layout'] = get_field('layout', $datenArray['program']);

            if (empty($datenArray['layout'])) {
                $datenArray['layout'] = 'blue';
            }

        } else {
            $datenArray['layout'] = 'blue';
        }

        if (!empty($datenArray['stage'])) {

            $datenArray['stage'] = $datenArray['stage'][0];
            $datenArray['scrolldown'] = $datenArray['stage']['scrolldown'];
            if ($datenArray['scrolldown'] === "Discover more") {
                $datenArray['scrolldown'] = "Discover";
            }

            $video_label_hard = Array(
                "Play Video" => "Video",
                "Video abspielen" => "Video"
            );

            if (isset($video_label_hard[$datenArray['stage']['video_label']])) {
                $datenArray['stage']['video_label'] = $video_label_hard[$datenArray['stage']['video_label']];
            }

            if (is_array($datenArray['stage']['men_women'])) {
                $datenArray['has_men'] = (in_array('Men', $datenArray['stage']['men_women'])) ? true : false;
                $datenArray['has_women'] = (in_array('Women', $datenArray['stage']['men_women'])) ? true : false;
            } else {
                $datenArray['has_men'] = false;
                $datenArray['has_women'] = false;
            }

            if ($datenArray['has_men']) {


                $datenArray['has_men'] = (!empty($datenArray['stage']['material_varianten_1_men'][0]['product_image']));

                if ($datenArray['has_men']) {

                    for ($i = 0; $i < 7; $i++) {
                        $j = $i + 1;
                        if ($datenArray['stage']['material_varianten_' . $j . '_men'][0]['status'] == 'Active') {
                            $datenArray['materials_men'][] = $datenArray['stage']['material_varianten_' . $j . '_men'];
                        }
                    }

                    $j = 0;
                    $datenArray['materials_men']['highlights'] = array();
                    if ($datenArray['stage']['material_varianten_1_men'][0]['status'] == 'Active') {
                        foreach ($datenArray['materials_men'][0] as $nr => $material) {
                            if ($material['technologies'] !== false) {


                                foreach ($material['technologies'] as $key => $val) {


                                    $datenArray['materials_men']['highlights'][$j] = array();
                                    $datenArray['materials_men']['highlights'][$j]['bild'] = $nr;
                                    $datenArray['materials_men']['highlights'][$j]['name'] = $val['technology_selector']->name;
                                    $datenArray['materials_men']['highlights'][$j]['desc'] = $val['technology_selector']->description;
                                    $datenArray['materials_men']['highlights'][$j]['y'] = $val['y'];
                                    $datenArray['materials_men']['highlights'][$j]['x'] = $val['x'];
                                    $datenArray['materials_men']['highlights'][$j]['size'] = $val['size'];

                                    $j++;
                                }
                            }
                        }
                    }

                }

            }

            if ($datenArray['has_women']) {

                $datenArray['has_women'] = (!empty($datenArray['stage']['material_varianten_1_women'][0]['product_image']));
                for ($i = 0; $i < 7; $i++) {
                    $j = $i + 1;
                    if ($datenArray['stage']['material_varianten_' . $j . '_women'][0]['status'] == 'Active') {
                        $datenArray['materials_women'][] = $datenArray['stage']['material_varianten_' . $j . '_women'];
                    }
                }

                $datenArray['materials_women']['highlights'] = array();
                if ($datenArray['stage']['material_varianten_1_women'][0]['status'] == 'Active') {
                    foreach ($datenArray['materials_women'][0] as $nr => $material) {
                        if ($material['technologies'] !== false) {


                            foreach ($material['technologies'] as $key => $val) {


                                $datenArray['materials_women']['highlights'][$j] = array();
                                $datenArray['materials_women']['highlights'][$j]['bild'] = $nr;
                                $datenArray['materials_women']['highlights'][$j]['name'] = $val['technology_selector']->name;
                                $datenArray['materials_women']['highlights'][$j]['desc'] = $val['technology_selector']->description;
                                $datenArray['materials_women']['highlights'][$j]['y'] = $val['y'];
                                $datenArray['materials_women']['highlights'][$j]['x'] = $val['x'];
                                $datenArray['materials_women']['highlights'][$j]['size'] = $val['size'];

                                $j++;
                            }
                        }
                    }
                }
            }

        }

        $techs = get_field('technologies2');

        $datenArray['highlights'] = Array();
        $datenArray['technologies'] = Array();

        foreach ($techs as &$tech) {

            $name = $tech['technology']->name;
            $cat_name = get_field('category_name', $tech['technology']);

            if (!isset($datenArray['technologies'][$cat_name])) {
                $datenArray['technologies'][$cat_name] = Array();
            }
            $cat = &$datenArray['technologies'][$cat_name];

            $cat[$name]['name'] = $name;
            $cat[$name]['desc'] = $tech['technology']->description;

        }

        $datenArray['technologies_headline'] = get_field('technologies_headline');
        $datenArray['technologies_open_label'] = get_field('technologies_open_label');
        $datenArray['updates'] = get_field('updates');
        $datenArray['updates_headline'] = get_field('updates_headline');
        $datenArray['features_headline'] = get_field('headline');
        $datenArray['features_open_label'] = get_field('open_label');
        $datenArray['category'] = get_field('category');
        $datenArray['type'] = get_field('type');
        $datenArray['distance'] = get_field('distance');
        $datenArray['weight'] = get_field('weight');
        $datenArray['heel_drop'] = get_field('heel_drop');
        $datenArray['item'] = get_field('item');
        $datenArray['key_colors'] = get_field('key_colors');
        $datenArray['srp_euro'] = get_field('srp_euro');
        $datenArray['intro'] = get_field('intro');
        $datenArray['sdp'] = get_field('sdp');
        $datenArray['consumer_segment'] = get_field('consumer_segment');

        $this->data = $datenArray;

    }

    public function set_apparel()
    {

        $datenArray = [];
        $this->data = $datenArray;

    }

    private $unique_create_triangle_mask = 0;

    /**
     * @deprecated
     */
    private function create_triangle($pass)
    {

        $def = new stdClass();
        $def->src = "http://cy-live1.de/asics2017/wp-content/uploads/2017/06/primary_T749N_4358_GEL-KAYANO-24.png";
        $def->img_width = 662;
        $def->img_height = 306;
        $def->width = 150;
        $def->height = 75;
        $def->x = 220;
        $def->y = 150;
        $def->material = "";

        $def = (object)array_merge((array)$def, (array)$pass);

        /*$points = Array(
            $def->x . ',' . $def->y,
            ( $def->x + $def->width / 2 ) . ',' . ( $def->y + $def->height ),
            ( $def->x + $def->width ) . ',' . $def->y,
        );*/


        //$points = implode( ' ', $points );

        $points = $def->x / $def->img_width . ' ' . $def->y / $def->img_height . ',
         ' . (($def->x + ($def->width / 2)) / $def->img_width) . ' ' . ((($def->y + $def->height) / $def->img_height)) . ',
         ' . (($def->x + $def->width) / $def->img_width) . ' ' . $def->y / $def->img_height;

        $id = 'unique-triangle-mask' . $this->unique_create_triangle_mask++;
        $tri_css = 'width:' . $def->width / 16 . 'rem;height:' . $def->height / 16 . 'rem';
        $tri_holder_css = 'position:relative;left:' . ($def->x * -1) . 'px;top:' . ($def->y * -1) . 'px';

        ?>

        <div class="tri" style="" data-material="<?php echo $def->material; ?>">
            <div class="tri_container" style="overflow:hidden;">
                <div class="tri_hover" data-material="<?php echo $def->material; ?>"></div>
                <div class="tri_holder" style="background-image:url(<?php echo $def->src; ?>)"></div>
            </div>
        </div>

        <?php

    }

    public function print_module()
    {

        if ($this->is_apparel()) {
            CyTheme::printt("Modul: Product_v200 (Apparel)");
        } else if ($this->is_shoe()) {
            $this->print_shoe();
        }

    }

    public function print_shoe()
    {

        $has_men = $this->data['has_men'];
        $has_women = $this->data['has_women'];

        ?>

        <script>
            var readmore = '<?php echo __('read more', 'CyTheme@Asics'); ?>';
            var myvideo = '<?php echo $this->data['stage']['video_url']; ?>';
        </script>

        <section class="product_single <?php echo $this->data['layout']; ?>"
                 data-layout="<?php echo $this->data['layout']; ?>">

            <div class="inner_wrapper">

                <!-- Beschreibung -->
                <section class="detail_section product_description">
                    <p>
                        <?php echo strip_tags(apply_filters('the_content', $this->data['stage']['description'])); ?>
                    </p>
                </section>

                <!-- Slider -->

                <div class="slider_section">

                    <?php

                    $nav_triangles = "";
                    $i = 0;

                    if ($has_men) : ?>

                        <section class="detail_section slider slider_men" data-gender="men">

                            <div class="info-menu focus">

                                <h4 class="menu-title">Highlights</h4>

                                <div class="menu-wrapper">
                                    <?php $i = 0;
                                    if (isset($this->data['materials_men']['highlights'])) {
                                        foreach ($this->data['materials_men']['highlights'] as $key => $val):

                                            $i++;
                                            echo '<div class="menu-point menu-point-' . ($i + 1) . '"data-text="' . str_replace('"', '”', $val['desc']) . '"  data-highlight="' . ($i + 1) . '" data-image="' . $val['bild'] . '" data-size="' . $val['size'] . '" data-x="' . $val['x'] . '" data-y="' . $val['y'] . '"><span>' . $val['name'] . '</span></div> ';
                                        endforeach;
                                    }
                                    ?>
                                </div>

                            </div>

                            <div class="slider_wrapper">
                                <div class="slider_holder">
                                    <div class="highlights men">

                                        <?php
                                        $read = $this->data['stage']['info_button_text_top'];
                                        $more = $this->data['stage']['info_button_text_bottom'];
                                        $i = 0;
                                        if (isset($this->data['materials_men']['highlights'])) {
                                            $dataMater = $this->data['materials_men']['highlights'];
                                            foreach ($dataMater as $keys => $vals):
                                                $full_width = (760 / 16) * ($vals['size'] / 100);
                                                $half_width = $full_width / 2;
                                                $font_size = 760 / 6 * ($vals['size'] / 125) / 16;
                                                $i++;
                                                echo
                                                    '<div class="highlight" data-image="' . $vals['bild'] . '" data-name="' . $vals['name'] . '" data-text="' . str_replace('"', '”', $vals['desc']) . '" data-highlight="' . ($i + 1) . '" style="left:' . $vals['x'] . '%;top:' . $vals['y'] . '%;">
													<div class="highlight_inner" style="height:' . $full_width . 'rem; width:' . $full_width . 'rem; margin-left:-' . $half_width . 'rem; margin-top:-' . $half_width . 'rem;">
														<div class="highlight_transparent">
															<div class="highlight_canvas">
																<div class="hightlight_canvas_inner">
																	<div class="highlight_hover">
																		<div class="highlight_hover_active" style="font-size:' . $font_size . 'rem">
																		<span>
																			   ' . $read . '<br/>
																			   <span class="more-svg"></span>
																			   ' . $more . '<br/>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>';
                                            endforeach;
                                        }
                                        $i = 0;
                                        ?>
                                    </div>

                                    <div class="slider_gallery">

                                        <div class="slider_frame">
                                            <?php foreach ($this->data['materials_men'] as $mat_i => &$mat) :


                                                if (empty($mat)) {
                                                    continue;
                                                }

                                                foreach ($mat as &$pic) :
                                                    $bg = $pic['product_image'];
                                                    if (empty($bg)) {
                                                        continue;
                                                    }
                                                    ?>

                                                    <div
                                                            class="slider_item"
                                                            data-item="<?php echo $i; ?>"
                                                            data-material="<?php echo $mat_i; ?>"
                                                            style="background-image:url('<?php echo $bg; ?>')">
                                                    </div>

                                                    <?php
                                                    $nav_triangles .= '<div class="slider_nav_triangle" data-item="' . $i . '" data-material="' . $mat_i . '"></div>';
                                                    $i++;
                                                endforeach;
                                                ?>

                                            <?php endforeach; ?>

                                        </div>

                                    </div>

                                    <div class="arrow prev"></div>
                                    <div class="arrow next"></div>

                                </div>

                                <div class="slider_nav">
                                    <?php echo $nav_triangles; ?>
                                </div>

                            </div>

                        </section>

                    <?php endif; ?>

                    <?php

                    $i = 0;
                    $nav_triangles = "";

                    if ($has_women) : ?>

                        <section class="detail_section slider slider_women" style="<?php if ($has_men) {
                            echo 'display:none';
                        } ?>">

                            <div class="info-menu focus">

                                <h4 class="menu-title">Highlights</h4>

                                <div class="menu-wrapper">

                                    <?php
                                    $i = 0;
                                    if (isset($this->data['materials_women']['highlights'])) {
                                        foreach ($this->data['materials_women']['highlights'] as $key => $val):

                                            $i++;
                                            echo '<div class="menu-point menu-point-' . ($i + 1) . '"data-text="' . str_replace('"', '”', $val['desc']) . '"  data-highlight="' . ($i + 1) . '" data-image="' . $val['bild'] . '" data-size="' . $val['size'] . '" data-x="' . $val['x'] . '" data-y="' . $val['y'] . '"><span>' . $val['name'] . '</span></div>';
                                        endforeach;
                                    }
                                    ?>

                                </div>

                            </div>


                            <div class="slider_wrapper">
                                <div class="slider_holder">
                                    <div class="highlights women">

                                        <?php
                                        $read = $this->data['stage']['info_button_text_top'];
                                        $more = $this->data['stage']['info_button_text_bottom'];
                                        $i = 0;
                                        if (isset($this->data['materials_women']['highlights'])) {
                                            $dataMater = $this->data['materials_women']['highlights'];
                                            foreach ($dataMater as $keys => $vals):
                                                $full_width = (760 / 16) * ($vals['size'] / 100);
                                                $half_width = $full_width / 2;
                                                $font_size = 760 / 6 * ($vals['size'] / 125) / 16;
                                                $i++;
                                                echo
                                                    '<div class="highlight" data-image="' . $vals['bild'] . '" data-name="' . $vals['name'] . '" data-text="' . str_replace('"', '”', $vals['desc']) . '" data-highlight="' . ($i + 1) . '" style="left:' . $vals['x'] . '%;top:' . $vals['y'] . '%;">
													<div class="highlight_inner" style="height:' . $full_width . 'rem; width:' . $full_width . 'rem; margin-left:-' . $half_width . 'rem; margin-top:-' . $half_width . 'rem;">
														<div class="highlight_transparent">
															<div class="highlight_canvas">
																<div class="hightlight_canvas_inner">
																	<div class="highlight_hover">
																		<div class="highlight_hover_active" style="font-size:' . $font_size . 'rem">
																			<span>
																			   ' . $read . '<br/>
																			   <span class="more-svg"></span>
																			   ' . $more . '<br/>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>';
                                            endforeach;
                                        }
                                        $i = 0;
                                        ?>
                                    </div>

                                    <div class="slider_gallery">
                                        <div class="slider_frame">
                                            <?php foreach ($this->data['materials_women'] as $mat_i => &$mat) :
                                                if (empty($mat)) {
                                                    continue;
                                                }
                                                foreach ($mat as &$pic) :
                                                    $bg = $pic['product_image'];
                                                    if (empty($bg)) {
                                                        continue;
                                                    }
                                                    ?>

                                                    <div
                                                            class="slider_item"
                                                            data-item="<?php echo $i; ?>"
                                                            data-material="<?php echo $mat_i; ?>"
                                                            style="background-image:url('<?php echo $bg; ?>')">
                                                    </div>

                                                    <?php
                                                    $nav_triangles .= '<div class="slider_nav_triangle" data-item="' . $i . '" data-material="' . $mat_i . '"></div>';
                                                    $i++;
                                                endforeach;
                                                ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="arrow prev"></div>
                                    <div class="arrow next"></div>
                                </div>

                                <div class="slider_nav">
                                    <?php echo $nav_triangles; ?>
                                </div>

                            </div>

                        </section>

                    <?php endif; ?>

                    <div class="info-btns-wrapper">

                        <div class="info-btns">

                            <?php if ($this->data['stage']['video_url'] != '' && $this->data['stage']['video_label'] != ''): ?>
                                <div class="info-btn vid noselect">
                                    <span><?php echo $this->data['stage']['video_label'] ?></span>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                </div>
                            <?php endif; ?>

                            <?php if ($this->data['updates'] != '' && $this->data['updates_headline'] != ''): ?>
                                <div class="info-btn update noselect">
                                    <span><?php echo $this->data['updates_headline'] ?></span>
                                </div>
                            <?php endif; ?>

                            <div class="info-btn learn-more noselect">
                                <span><?php echo $this->data['scrolldown'] ?></span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                            </div>

                        </div>

                    </div>

                </div> <!-- .inner_wrapper -->

            </div><!-- . slider_section -->

            <div class="inner_wrapper">

                <section class="detail_section design">

                    <?php if ($has_men || $has_women) :

                        $initial_gender = 'men';
                        if (!$has_men && $has_women) {
                            $initial_gender = 'women';
                        }

                        ?>

                        <div class="gender_select <?php echo $initial_gender; ?>">

                            <div class="men <?php if (!$has_men) {
                                echo 'inactive';
                            } ?>">

                                <div class="men_icon">
                                    <div class="men_icon_holder">
                                        <svg class="men_svg" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 87.83 89.32" enable-background="new 0 0 87.83 89.32"
                                             xml:space="preserve">
<path fill="#1D1D1B" d="M87.83,0L59.32,0.54l-0.01-0.01l-9.35,9.35L69.4,9.54l-9.96,9.92l0.01,0.01l-7.06,7.07
	c-5.29-3.39-11.57-5.36-18.31-5.36C15.28,21.17,0,36.46,0,55.25c0,18.79,15.28,34.07,34.08,34.07c18.79,0,34.07-15.29,34.07-34.07
	c0-7.5-2.44-14.43-6.56-20.06l4.26-4.27l12.39-12.53L77.9,37.93l0.01,0.01l9.39-9.4l-0.01-0.01L87.83,0z M34.08,78.68
	c-12.92,0-23.43-10.51-23.43-23.43c0-12.92,10.51-23.43,23.43-23.43c12.92,0,23.43,10.51,23.43,23.43
	C57.5,68.17,46.99,78.68,34.08,78.68z"/>
</svg>
                                    </div>
                                </div>

                                <div class="men_label noselect">
                                    <span class="men_text"><?php echo $this->data['stage']['men_label']; ?></span>
                                </div>

                            </div>

                            <div class="women <?php if (!$has_women) {
                                echo 'inactive';
                            } ?>">

                                <div class="women_label noselect">
                                    <span class="women_text"><?php echo $this->data['stage']['women_label']; ?></span>
                                </div>

                                <div class="women_icon">
                                    <div class="women_icon_clip">
                                        <div class="women_icon_holder">
                                            <svg class="women_svg" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 68.15 103.48" enable-background="new 0 0 68.15 103.48"
                                                 xml:space="preserve">
<path fill="#1D1D1B" d="M68.15,34.08C68.15,15.29,52.87,0,34.08,0C15.29,0,0,15.29,0,34.08c0,16.75,12.16,30.71,28.11,33.54v10.52
	H14.69v11.93h13.42v13.42h11.93V90.06h13.42V78.13H40.04V67.62C55.99,64.79,68.15,50.83,68.15,34.08z M10.65,34.08
	c0-12.92,10.51-23.43,23.43-23.43c12.92,0,23.43,10.51,23.43,23.43c0,12.92-10.51,23.43-23.43,23.43
	C21.16,57.5,10.65,46.99,10.65,34.08z"/>
</svg>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    <?php endif; ?>

                    <div class="material_selection">

                        <div class="material_inner">

                            <?php if ($has_men) : ?>

                                <div class="material_items material_men">

                                    <?php
                                    foreach ($this->data['materials_men'] as $mat_i => &$mat) {

                                        if (empty($mat)) {
                                            continue;
                                        }

                                        foreach ($mat as &$pic) {

                                            $bg = $pic['product_image'];

                                            if (empty($bg)) {
                                                continue;
                                            }

                                            $args = new stdClass();
                                            $args->src = $bg;
                                            $args->material = $mat_i;
                                            $this->create_triangle($args);
                                            break;

                                        }

                                    }
                                    ?>

                                </div>

                            <?php endif; ?>

                            <?php if ($has_women) : ?>

                                <div class="material_items material_women"
                                     style="<?php if ($has_men) {
                                         echo 'display:none';
                                     } ?>">

                                    <?php

                                    foreach ($this->data['materials_women'] as $mat_i => &$mat) {

                                        if (empty($mat)) {
                                            continue;
                                        }

                                        foreach ($mat as &$pic) {

                                            $bg = $pic['product_image'];
                                            if (empty($bg)) {
                                                continue;
                                            }

                                            $args = new stdClass();
                                            $args->src = $bg;
                                            $args->material = $mat_i;
                                            $this->create_triangle($args);
                                            break;

                                        }

                                    }
                                    ?>

                                </div>

                            <?php endif; ?>

                        </div>

                    </div>

                </section><!-- .design -->

                <section class="detail_section section_technologies">

                    <header class="technologies_header">

                        <div class="technologies_header_inner">
                            <h2 class="technologies_headline"><?php echo $this->data['technologies_headline']; ?></h2>
                            <div class="detail_section_controls">
                                <div class="detail_zu noselect">-</div>
                                <div class="detail_auf noselect active">+</div>
                            </div>
                        </div>

                    </header>

                    <div class="detail_content">

                        <ul class="technologies_list">

                            <?php
                            foreach ($this->data['technologies'] as $cat_name => &$cat) {
                                echo '<li><h2>' . $cat_name . '</h2><p>';
                                $i = 0;
                                foreach ($cat as $tech_name => &$tech) {
                                    if ($i > 0) {
                                        echo ', ';
                                    }
                                    echo '<span class="tech-desc" data-desc="' . strip_tags(str_replace('"', '”', $tech['desc'])) . '">' . $tech['name'] . '</span>';
                                    $i++;
                                }
                            }

                            ?>
                            <?php if ($this->data['updates_headline'] != '' && $this->data['updates'] != ''): ?>
                                <li class="updates">
                                    <h2><?php echo $this->data['updates_headline'] ?></h2>
                                    <div class="updates_text">
                                        <?php echo $this->data['updates']; ?>
                                    </div>
                                </li>
                            <?php endif; ?>

                        </ul>

                    </div>

                </section><!-- .section_technologies -->

                <section class="detail_section section_features">

                    <header class="features_header">

                        <div class="features_header_inner">
                            <h2 class="features_headline"><?php echo $this->data['features_headline']; ?></h2>
                            <div class="detail_section_controls">
                                <div class="detail_zu noselect">-</div>
                                <div class="detail_auf noselect active">+</div>
                            </div>
                        </div>

                    </header>

                    <div class="detail_content">

                        <div class="features clearfix">

                            <div class="feature_column">

                                <div class="feature_category_holder clearfix">
                                    <h2 class="feature_category_name">Specification</h2>
                                </div>

                                <ul class="features_list">

                                    <?php if (!empty($this->data['category'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Category</h4>
                                            <span class="feature_value"><?php echo $this->data['category']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['type'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Type</h4>
                                            <span class="feature_value"><?php echo $this->data['type']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['distance'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Size</h4>
                                            <span class="feature_value"><?php echo $this->data['distance']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['weight'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Weight</h4>
                                            <span class="feature_value"><?php echo $this->data['weight']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['heel_drop'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Heel Drop</h4>
                                            <span class="feature_value"><?php echo $this->data['heel_drop']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                </ul>

                            </div>

                            <div class="feature_column">

                                <div class="feature_category_holder clearfix">
                                    <h2 class="feature_category_name">Commercial</h2>
                                </div>

                                <ul class="features_list">

                                    <?php if (!empty($this->data['item'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Item#</h4>
                                            <span class="feature_value"><?php echo $this->data['item']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['key_colors'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Key Color</h4>
                                            <span class="feature_value"><?php echo $this->data['key_colors']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['srp_euro'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">ARP Euro</h4>
                                            <span class="feature_value"><?php echo $this->data['srp_euro']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['intro'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Intro</h4>
                                            <span class="feature_value"><?php echo $this->data['intro']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                    <?php if (!empty($this->data['sdp'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">SDP</h4>
                                            <span class="feature_value"><?php echo $this->data['sdp']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                </ul>

                            </div>

                            <div class="feature_column">

                                <div class="feature_category_holder clearfix">
                                    <h2 class="feature_category_name">Target group</h2>
                                </div>

                                <ul class="features_list">

                                    <?php if (!empty($this->data['consumer_segment'])) : ?>
                                        <li class="feature_item clearfix">
                                            <h4 class="feature_name">Consumer Segment</h4>
                                            <span class="feature_value"><?php echo $this->data['consumer_segment']; ?></span>
                                        </li>
                                    <?php endif; ?>

                                </ul>

                            </div>

                        </div><!-- .features -->

                    </div>

                </section><!-- .section_features -->

            </div><!-- .inner_wrapper -->

        </section>
        <?php

        //parent::add_script(get_template_directory_uri() . '/module/Product_v200/scripts/Product_v200.js');

    }

}

new ModulProduct();

?>