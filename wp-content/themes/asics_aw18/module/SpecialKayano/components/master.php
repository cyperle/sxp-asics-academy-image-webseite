<div class="special_page">
    <div class="special_crumbs">
        <?php foreach ($sectionAnchors as $sectionAnchor) : $sectionId = ($sectionId??0) + 1; ?>
            <?php if (!empty($sectionAnchor)) : ?>
                <a href="<?php echo "#" . $sectionId; ?>">
                    <span><?php echo $sectionAnchor; ?></span>
                </a>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <?php echo $content; ?>
</div>

<?php ob_start(); ?>
<script>
    (function () {

        var scrollPos = 0;
        var $specialCrumbs = $('.special_crumbs');
        var $specialCrumbsClone = $specialCrumbs.clone(true);

        $specialCrumbsClone
            .insertAfter($specialCrumbs)
            .css({
                position: 'fixed',
                top: 0,
                zIndex: 10,
                width: '100%',
                paddingTop: '.625rem',
                transform: 'translateY(-.625rem)',
                transition: 'transform .5s'
            })
            .hide();

        var $specialCrumbsLinks = $('.special_crumbs a');

        $specialCrumbsLinks.click(function (e) {
            e.preventDefault();
            var aid = $(this).attr("href");
            $('html,body').animate({scrollTop: $(aid).offset().top - $specialCrumbs.height()});
        });

        var $specialSections = $('.special_section');

        function scrollSpy() {
            var sectionTop = 0;
            var scrollTop = $(document).scrollTop();
            var activeSection = null;
            $specialSections.each(function () {
                sectionTop = this.offsetTop;
                if (scrollTop >= sectionTop - 100) {
                    activeSection = this;
                }
            });
            if (activeSection !== null) {
                $specialCrumbsLinks.removeClass('active').filter('[href="#' + activeSection.id + '"]').addClass('active');
            }
        }

        $(window).scroll(scrollSpy);

        var offsetTop = 0;
        var visible = false;
        $(window).scroll(function () {
            scrollPos = $(document).scrollTop();
            if (scrollPos > $specialCrumbs.offset().top) {
                if (!visible) {
                    $specialCrumbsClone.show().css('transform', 'translateY(0)');
                    visible = true;
                }
            } else {
                if (visible) {
                    $specialCrumbsClone.hide().css('transform', 'translateY(-.625rem)');
                    visible = false;
                }
            }
        });

    })()
</script>
<?php CyModul::push_end(ob_get_clean()); ?>