<!-- Module Special:Header -->
<?php if (!empty($source)) : ?>
    <div class="special_header pointer-ignore">

        <?php if (!empty($side_padding)) : ?>
        <div class="padding-wrapper">
            <?php endif; ?>

            <img src="<?php echo $source; ?>" class="special_header_image">

            <?php if (!empty($side_padding)) : ?>
        </div>
    <?php endif; ?>

    </div>
<?php endif; ?>
