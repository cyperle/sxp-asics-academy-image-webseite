<!-- Module Special:EmptyBlock -->
<?php if (!empty($text)) : ?>
    <div class="special_empty">
        <div class="special_headline_skewed">
            <div class="special_headline_skewed_skew">
                <div class="special_headline_skewed_text"><?php echo $text; ?></div>
            </div>
        </div>
    </div>
<?php endif; ?>