<div class="special_page">

    <div class="special_section style_1">
        <div class="inner_wrapper padding-zero">

            <!-- Module Special:Header -->
            <div class="special_header pointer-ignore">
                <img src="http://via.placeholder.com/1202x565/adadad/adadad" alt="" class="special_header_image">
            </div>

            <!-- Module Special:Headline -->
            <div class="special_headline">
                <div class="special_headline_sub">sit amet</div>
                <div class="special_headline_main">Lorem ipsum dolor sit amet, consetetur</div>
            </div>

            <!-- Module Special:Text -->
            <div class="special_text padding-wrapper">
                <div class="padding-wrapper">

                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                        invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
                        et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                        Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                        diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                        voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                        gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>

                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                        invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
                        et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                        Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                        diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                        voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                        gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>

                </div>

            </div>

            <!-- Module Special:Picture -->
            <div class="special_pic pointer-ignore">
                <img src="http://via.placeholder.com/951x240/adadad/adadad">
            </div>

            <!-- Module Special:Block Compact-->
            <div class="special_block_compact">

                <div class="padding-wrapper">

                    <div class="special_headline_skewed">
                        <div class="special_headline_skewed_skew">
                            <div class="special_headline_skewed_text">sed diam nonumy</div>
                        </div>
                    </div>

                    <div class="special_block_text_above">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                            sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                            voluptua. At</p>
                    </div>

                </div>

                <div class="special_block_content">

                    <div class="special_block_left pointer-ignore">
                        <img src="http://via.placeholder.com/569x569/adadad/adadad"
                             class="special_block_left_image">
                    </div>

                    <div class="special_block_right">

                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                            accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
                            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                            aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
                            rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                            amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
                            et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet.</p>

                    </div>

                </div>
            </div>

            <!-- Module Special:Picture -->
            <div class="special_pic pointer-ignore">
                <img src="http://via.placeholder.com/951x240/adadad/adadad">
            </div>

            <!-- Module Special:Block -->
            <div class="special_block">

                <div class="padding-wrapper">

                    <div class="special_headline_skewed">
                        <div class="special_headline_skewed_skew">
                            <div class="special_headline_skewed_text">no sea takimata sanctus</div>
                        </div>
                    </div>

                    <div class="special_block_text_above">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                            accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
                            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                            aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
                            rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                            amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
                            et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet. </p>
                    </div>

                </div>

                <div class="special_block_content">

                    <div class="special_block_left pointer-ignore">
                        <img src="http://via.placeholder.com/569x717/adadad/adadad"
                             class="special_block_left_image">
                    </div>

                    <div class="special_block_right">

                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                            accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
                            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                            aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
                            rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                            amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
                            et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet.</p>

                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
                            vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio
                            dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla
                            facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                            nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                            nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                            vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at
                            vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril
                            delenit augue duis dolore te feugait nulla facilisi.</p>

                    </div>

                </div>
            </div>

            <!-- Module Special:EmptyBlock -->
            <div class="special_empty">
                <div class="special_headline_skewed">
                    <div class="special_headline_skewed_skew">
                        <div class="special_headline_skewed_text">sed diam nonumy</div>
                    </div>
                </div>
            </div>

            <!-- Module Special:Headline -->
            <div class="special_headline">
                <div class="special_headline_sub">GEL-Kayano 25</div>
                <div class="special_headline_main">Hightech und Knowhow <br>bis ins Kleinste</div>
            </div>

            <!-- Module Special:Header -->
            <div class="special_header pointer-ignore padding-wrapper">
                <img src="test/le_shoe.png" alt="" class="special_header_image">

            </div>

            <!-- Module Special: Technologies -->
            <div class="special_technologies padding-wrapper">
                <div class="special_technologies_padding padding-wrapper">

                    <div class="special_technologies_illustration pointer-ignore">
                        <img class="block img-responsive" src="test/technologies.png" alt="">
                    </div>

                    <div class="special_technologies_details">

                        <h3>FlyteFoam PROPEL</h3>
                        <p>Von ASICS entwickeltes, NEUES MITTELSOHLENMATERIAL mit der höchsten Rückstellenergie im
                            Markt, für mehr Dynmaik und Vortrieb in der Abdruckphase - auch über die lange
                            Distanz.</p>

                        <h3>FlyteFoam LYTE</h3>
                        <p>Neues, von ASICS entwickeltes Mittelsohlenmaterial mit dem leichtesten Gewicht im Markt –
                            bei gleichzeitig gesteigertem Komfort und um 10% verbesserter Haltbar-
                            keit (im Vergleich zum normalen FLYTEFOAM-Material)</p>

                        <h3>GEL-Dämpfungstechnologie</h3>
                        <p>Die bewährte ASICS Technologie für effektive, sensible Dämpfung in der neuesten
                            Generation – in der am weitesten entwickelten, noch gezielter gesetzten TWIST-GEL-Form
                            im Vorfußbereich und als sichtbare Variante im Rückfußbereich.</p>

                        <h3>GUIDANCE LINE & STABILITY TRUSSTIC-System</h3>
                        <p>Für einen perfekte Führung des Fußes vom ersten Bodenkontakt bis zum Abdruck mit
                            zusätzlicher, gezielter Unterstützung in der Standphase und beim Übergang in die
                            Abdruckphase – gerade für die lange Distanz.</p>

                        <h3>SlopedDUOMAX & PinDUOMAX</h3>
                        <p>Hoch innovative DUOMAX-Variante, die gezielte Unterstützung durch eine besondere
                            Konstruktion der Mittelsohle erzielt. Die Mittelsohle arbeitet mit zwei unterschiedlich
                            dichten Materialien und ist so ausge-
                            formt und aufgebaut, das sie der Pronationstendenz sanft entgegenwirkt und sich
                            gleichzeitig perfekt der individuellen Bewegungsdynamik anpassen kann.</p>

                        <h3>AHAR +</h3>
                        <p>Von ASICS entwickelte Gummi-Mischung mit Karbon-
                            Anteil für höchste Belastbarkeit und damit längere Haltbarkeit der Außensohle im
                            Aufprallbereich.</p>

                        <h3>2-lagiges Jacquard-MESH</h3>
                        <p>Neue ASICS Upper-Konstruktion für weniger Hautirritationen, bessere Passform und einen
                            noch effektiveren Luftaustausch.</p>

                        <h3>META CLUTCH HEEL COUNTER</h3>
                        <p>Außenliegenmde Fersenkappe der neuesten Generation für mehr Halt, mehr Passform, mehr
                            Unterstützung und damit ein sicheres Gefühl im Fersenbereich.</p>

                        <h3>Inner Stability Wrap System</h3>
                        <p>Innenliegendes Band-System im Vorfußbereich, das den Fuß beim Übergang von der Stand- in
                            die Abdruckphase stabilisiert und für eine bessere Passform sorgt.</p>

                        <h3>ORTHOLITE X-40 Einlegesohle</h3>
                        <p>Hoch anpassungsfähige, auswechselbare Einlegesohle für eine optimale, individuelle und
                            komfortable Fuss-
                            bettung, eine verbesserte Absorption der Aufprallkräfte und ein komfortableres
                            Laufgefühl.</p>

                        <h3>Light SpevaFoam-Lasting</h3>
                        <p>Das durchgehende SpEVA-45°-Lasting reduziert das Schuhgesamtgewicht, erhöht die
                            Stabilität und – dank sensiblerer Dämpfung – den Komfort während des Abrollvorgangs.</p>


                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<script>
//    var special_section = document.querySelector('.special_section');
//    var currentStyleNo = 1;
//    if (localStorage.getItem('style') !== null) {
//        special_section.classList.remove('style_' + currentStyleNo);
//        currentStyleNo = localStorage.getItem('style');
//        special_section.classList.add('style_' + currentStyleNo);
//    }
//    document.addEventListener('keydown', function (e) {
//        var oldClass, newClass;
//        oldClass = 'style_' + currentStyleNo;
//        if (e.key === 'ArrowRight') {
//            currentStyleNo++;
//            if (currentStyleNo > 7) currentStyleNo = 1;
//        } else if (e.key === 'ArrowLeft') {
//            currentStyleNo--;
//            if (currentStyleNo < 1) currentStyleNo = 7;
//        }
//        newClass = 'style_' + currentStyleNo;
//        special_section.classList.remove(oldClass);
//        special_section.classList.add(newClass);
//        localStorage.setItem('style', currentStyleNo);
//    });
</script>