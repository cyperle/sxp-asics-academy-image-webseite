(function ($) {

    $.win = $(window);
    $.doc = $(document);
    $.body = $(document.body);

    /**
     * Prüft, welche Klasse aus einem gegebenen Array existiert und gibt den ersten Treffer zurück.
     * @param array
     * @returns {string}
     */
    $.fn.determineClass = function (array) {

        var result = "";
        var element = this;

        $.each(array, function (index, currentClassName) {
            if (element.hasClass(currentClassName)) {
                result = currentClassName;
                return false;
            }
        });

        return result;

    };

    /**
     * Fügt einen namespaced Event-Listener auf Click und Enter hinzu. Weiterhin wird das Attribut "tabindex" gesetzt, um das Element mit der Tastatur fokusierbar zu machen.
     * @param func
     */
    $.fn.onEnterClick = function (func) {

        this.attr("tabindex", "0");

        this.on("click.onEnterClick keypress.onEnterClick", function (e) {

            if ((e.which && e.which == 13) || !!e.which) {
                //console.log("listen")
                func.call(this, e);
            }

        });

    };

    /**
     * Entfernt alle namespaced Event-Listener von Click und Enter. Die Fokusierbarkeit mit der Tastatur (tabindex) wird ebenfalls wieder entfernt.
     * @param func
     */
    $.fn.offEnterClick = function () {
        this.removeAttr("tabindex");
        this.off(".onEnterClick");
    };

    /**
     * Kehrt die Reihenfolge der Child-Elemente um.
     */
    $.fn.reverseChildren = function () {

        this.each(function (index, parent) {
            parent = $(parent);
            parent.children().each(function (index, child) {
                parent.prepend($(child));
            });
        });

    };

})(jQuery);