<?php
/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 09.11.2017
 * Time: 14:35
 */

function hide_slug()
{
    ?>
    <style>
        .term-slug-wrap,
        .term-parent-wrap,
        .inline-edit-col label:nth-child(2),
        .row-actions .view,
        #slug,
        .column-slug {
            display: none
        }
    </style>
    <?php
}

function hide_desc()
{
    ?>
    <style>
        .term-description-wrap {
            display: none
        }
    </style>
    <?php
}


/**
 * Integriere Taxonomie "Social Links"
 */

add_action('init', 'tax_social_add', 0);
add_action('social_add_form_fields', 'hide_slug', 10, 2);
add_action('social_edit_form_fields', 'hide_slug', 10, 2);

function tax_social_add()
{

    $labels = array(
        'name' => 'Social Links',
        'singular_name' => 'Sociallink',
        'search_items' => 'Search Social Links',
        'all_items' => 'All Social Links',
        'parent_item' => 'Parent Social Link',
        'parent_item_colon' => 'Parent Social:',
        'edit_item' => 'Edit Social Link',
        'update_item' => 'Update Social Link',
        'add_new_item' => 'Add new Social Link',
        'new_item_name' => 'New Social Link Name',
        'menu_name' => 'Social Links'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'sociallinks'),
        'meta_box_cb' => false
    );

    register_taxonomy('sociallinks', array('page'), $args);

}

/**
 * Integriere Taxonomie "Technologies"
 */

add_action('init', 'tax_technologies_add', 0);
add_action('technology_add_form_fields', 'hide_slug', 10, 2);
add_action('technology_edit_form_fields', 'hide_slug', 10, 2);

function tax_technologies_add()
{

    $labels = array(
        'name' => 'Technologies',
        'singular_name' => 'Technology',
        'search_items' => 'Search Technologies',
        'all_items' => 'All Technologies',
        'parent_item' => 'Parent Technology',
        'parent_item_colon' => 'Parent Technology:',
        'edit_item' => 'Edit Technology',
        'update_item' => 'Update Technology',
        'add_new_item' => 'Add new Technology',
        'new_item_name' => 'New Technology Name',
        'menu_name' => 'Technologies'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'technology'),
        'meta_box_cb' => false
    );

    register_taxonomy('technology', array('page'), $args);

}

/**
 * Integriere Taxonomie "Programs"
 */

add_action('init', 'tax_programs_add', 0);
add_action('program_add_form_fields', 'hide_slug', 10, 2);
add_action('program_edit_form_fields', 'hide_slug', 10, 2);

function tax_programs_add()
{

    $labels = array(
        'name' => 'Programs',
        'singular_name' => 'Program',
        'search_items' => 'Search Programs',
        'all_items' => 'All Programs',
        'parent_item' => 'Parent Program',
        'parent_item_colon' => 'Parent Program:',
        'edit_item' => 'Edit Program',
        'update_item' => 'Update Program',
        'add_new_item' => 'Add new Program',
        'new_item_name' => 'New Program Name',
        'menu_name' => 'Programs'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'program'),
        'meta_box_cb' => false,
        'show_in_nav_menus' => true
    );

    register_taxonomy('program', array('page'), $args);

}

/**
 * Integriere Taxonomie "Categories"
 */

add_action('init', 'tax_acategory_add', 0);
add_action('acategory_add_form_fields', 'hide_slug', 10, 2);
add_action('acategory_edit_form_fields', 'hide_slug', 10, 2);

function tax_acategory_add()
{

    $labels = array(
        'name' => 'Categories',
        'singular_name' => 'Category',
        'search_items' => 'Search Categories',
        'all_items' => 'All Categories',
        'parent_item' => 'Parent Category',
        'parent_item_colon' => 'Parent Category:',
        'edit_item' => 'Edit Category',
        'update_item' => 'Update Category',
        'add_new_item' => 'Add new Category',
        'new_item_name' => 'New Category Name',
        'menu_name' => 'Categories'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'acategory'),
        'meta_box_cb' => false,
        'show_in_nav_menus' => true
    );

    register_taxonomy('acategory', array('page'), $args);

}

add_action('init', 'tax_highlights_add', 0);
add_action('highlight_add_form_fields', 'hide_slug', 10, 2);
add_action('highlight_edit_form_fields', 'hide_slug', 10, 2);
add_action('highlight_add_form_fields', 'hide_desc', 10, 2);
add_action('highlight_edit_form_fields', 'hide_desc', 10, 2);

/**
 * Integriere Taxonomie "Highlights"
 */

function tax_highlights_add()
{

    $labels = array(
        'name' => 'Highlights',
        'singular_name' => 'Highlight',
        'search_items' => 'Search Highlights',
        'all_items' => 'All Highlights',
        'parent_item' => 'Parent Highlight',
        'parent_item_colon' => 'Parent Highlight:',
        'edit_item' => 'Edit Highlight',
        'update_item' => 'Update Highlight',
        'add_new_item' => 'Add new Highlight',
        'new_item_name' => 'New Highlight Name',
        'menu_name' => 'Highlights'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'highlight'),
        'meta_box_cb' => false
    );

    register_taxonomy('highlight', array('page'), $args);

}

/**
 * Integriere Taxonomie "Season"
 */

add_action('init', 'tax_season_add', 0);
//add_action('technology_add_form_fields', 'hide_slug', 10, 2);
//add_action('technology_edit_form_fields', 'hide_slug', 10, 2);

function tax_season_add()
{

    $labels = array(
        'name' => 'Season',
        'singular_name' => 'Season',
        'search_items' => 'Search Season',
        'all_items' => 'All Season',
        'parent_item' => 'Parent Season',
        'parent_item_colon' => 'Parent Season:',
        'edit_item' => 'Edit Season',
        'update_item' => 'Update Season',
        'add_new_item' => 'Add new Season',
        'new_item_name' => 'New Season Name',
        'menu_name' => 'Season'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'season'),
        'meta_box_cb' => false
    );

    register_taxonomy('season', array('page'), $args);

}