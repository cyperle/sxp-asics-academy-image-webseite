<?php
/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 09.11.2017
 * Time: 14:28
 */

/**
 * Super-Klasse für alle Module. Weiterhin wird hier entschieden, welche Module geladen werden müssen.
 */
abstract class CyModul
{

    public static $config = null; //Konfigurations-Objekt
    public static $html = ""; //Auszugebendes HTML
    public static $dbg = ""; //Auszugebendes HTML
    public static $scripts = []; //Zu ladende Scripte am Seitenende
    public static $styles = []; //Einzubindende Style-Dateien am Seitenende
    public static $inline_css = ""; //CSS, das inline auf der Seite eingebunden werden soll
    public static $template_slug = "";
    public static $end = []; //Wird an das Ende von <body> angefügt

    public static $init_cat_key = 'icat';

    /**
     * Initialisiert das Modul
     */
    abstract function set_module();

    /**
     * Erzeuge HTML des Moduls
     */
    abstract function print_module();

    /**
     * Lade ein Modul anhand seines Pfadnamens
     *
     * @param $module_name
     */
    static function load_module($module_name)
    {

        $str = self::$config->dir . "/module/" . $module_name . "/" . $module_name . ".php";

        if (file_exists($str)) {
            include($str);
        } else {
            CyTheme::alert('Die Modul-Datei ' . $str . ' existiert nicht.');
        }

    }

    /**
     * Intialisiere benötigte Module
     */
    static function init()
    {

        self::$template_slug = get_page_template_slug();

        // Initialisiere Config
        self::$config = new stdClass();
        self::$config->dir = get_template_directory();
        self::$config->uri = get_template_directory_uri();

        // Lade Modul: Globale Navigation
        self::load_module("Navigation");

        echo '<div class="page_content">';

        /**
         * Home Module
         */

        // Lade seitenspezifische Module
        $content_module_map = Array(
            //Name aus dem Backend (falls gegeben) => Verzeichnis im Theme,
            "header" => "Header",
            "special_teaser" => "TeaserSpecial",
            "kategorie_navigation_layout" => "KategorieProgramm",
        );

        if (have_rows('content-module')) {

            while (have_rows('content-module')): the_row();

                $row_key = get_row_layout();
                $row_name = $content_module_map[$row_key];

                if (isset($row_name)) {
                    self::load_module($row_name);
                } else {
                    // die($row_key);
                }

            endwhile;

        };

        /**
         * Product Overview Module
         */
        if (CyModul::is_template(["page-product-overview.php"])) {
            self::load_module("Overview");
        };

        if (CyModul::is_template(["page-tech-overview.php"])) {
            self::load_module('TechOverview');
        }

        /**
         * Techhighlights
         */
        if (CyModul::is_template(["page-technology.php"])) {
            self::load_module("TechHighlights");
        }

        /**
         * Product Single Module
         */
        if (CyModul::is_template(["page-product-single.php", "page-apparel-single.php"])) {
            self::load_module("Product_v200");
        }

        /**
         * Kayano Special
         */

        if (CyModul::is_template(["page-special-kayano.php"])) {
            self::load_module("SpecialKayano");
        };


        /**
         * Page Standard Modules
         */
        //$simple = Array('', 'page-impressum.php', 'page-datenschutz.php');
        if (CyModul::is_template(['', 'page-impressum.php', "page-datenschutz.php"])) {
            self::load_module("Simple");
        }

        echo "</div><!-- .page_content -->";

        // Lade Modul: Globaler Footer
        self::load_module("Footer");

        self::print_styles();

    }

    /**
     * Prüft, ob das aktuelle Template sich in einem gegebenen Array befindet.
     */
    public static function is_template($array)
    {

        if (in_array(self::$template_slug, $array)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Wandelt einen String in lowercase um und ersetzt alle Whitespaces durch Underscores.
     */
    public function lowercase($str)
    {
        return str_replace(" ", "_", strtolower($str));
    }

    public static function add_css($str)
    {
        self::$inline_css .= $str;
    }

    public static function add_html($str)
    {
        self::$html .= $str;
    }

    public static function push_end($str)
    {
        self::$end[] = $str;
    }

    public static function print_end()
    {
        foreach (self::$end as $end) {
            echo $end;
        }
    }

    public static function add_dbg($str)
    {
        self::$dbg .= $str;
    }

    public static function add_script($src)
    {
        array_push(self::$scripts, $src);
    }

    public static function add_style($src)
    {
        array_push(self::$styles, $src);
    }

    public static function print_styles()
    {
        foreach (self::$styles as $style) {
            echo '<link rel="stylesheet" type="text/css" href="' . $style . '">';
        }
        echo '<style>' . self::$inline_css . '</style>';
    }

    public static function print_html()
    {
        echo self::$html;
    }

    public static function print_scripts()
    {
        foreach (self::$scripts as $script) {
            echo '<script type="text/javascript" src="' . $script . '"></script>';
        }
    }

    public static function print_dbg()
    {
        echo self::$dbg;
    }

}