<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login — Asics Academy</title>
    <link href="<?php echo plugins_url('login.css', __FILE__); ?>" type="text/css" rel="stylesheet">
</head>

<body>

<div id="background"></div>

<div id="header">
    <div class="topbar">
        <div class="topbar_wrapper inner_wrapper">
            <div class="topbar_inner">
                <header class="topbar_left">
                    <a class="topbar_logo" href="<?php echo get_home_url(); ?>">
                        <h1 class="hide">Asics</h1>
                        <svg class="topbar_svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 174.6 56.7"
                             enable-background="new 0 0 174.6 56.7" xml:space="preserve">
<path fill="#FFFFFF" d="M17.6,40.2c-3.2,0-4.8-2.5-4-5.4c1.7-6,12.4-14.8,19.8-14.8c5.4,0,4.9,4.8,1.8,8.9l-1.7,1.9
	C27.3,36.9,21.5,40.2,17.6,40.2 M43.9,0c-9.4,0-19.7,5.7-26.2,11.2l0.5,0.7c10.1-7,24-11.6,27.9-4.8c2,3.6-1.4,11-6.3,16.7
	c1.2-2.6,0.4-7.5-6.4-7.5C22.2,16.4,0,31.3,0,46.6c0,6.1,4.2,10.1,11.2,10.1c18.7,0,44.1-30.6,44.1-46C55.3,5.2,52.1,0,43.9,0"></path>
                            <path fill="#FFFFFF" d="M85.2,16.2c-1.7-2.1-4.5-3.1-6.5-3.1H67.8L65.9,20h10.3l1,0.1c0,0,1,0.1,1.5,0.8c0.4,0.6,0.5,1.5,0.2,2.6
	l-0.5,1.8h-6.5c-5.6,0-13.4,4-15.1,10.5c-0.9,3.2-0.1,6,1.6,8.1c1.7,2,4.8,3.2,8.2,3.2h13.7l3.6-13.4l2.6-9.8
	C87.5,20,86.3,17.6,85.2,16.2z M74.4,40.2h-6.5c-1.1,0-2-0.4-2.6-1.1c-0.6-0.7-0.8-1.8-0.5-2.8c0.6-2.3,3.1-4.1,5.7-4.1h6L74.4,40.2
	z M159.8,26.3c-2.4-1.3-4.8-2.6-4.3-4.1c0.2-0.9,1.2-2.2,3.1-2.2h9.9l1.9-6.9h-10c-3,0-5.7,0.9-7.9,2.5c-2.3,1.7-3.5,3.7-4.2,6.4
	c-1.6,6,3.1,8.9,6.9,11c2.7,1.5,5.1,2.7,4.6,4.6c-0.4,1.4-1,2.5-4.3,2.5h-10.2l-1.9,6.9h11.3c3,0,5.8-0.6,8.1-2.4
	c2.3-1.7,3.5-4.2,4.3-7.1c0.8-2.8,0.2-5.5-1.6-7.5C163.9,28.5,161.7,27.3,159.8,26.3z M106.1,30.2c-1.5-1.7-3.7-2.8-5.6-3.9
	c-2.4-1.3-4.8-2.6-4.3-4.1c0.2-0.9,1.2-2.2,3.1-2.2h9.9l1.9-6.9h-10c-3,0-5.7,0.9-7.9,2.5c-2.3,1.7-3.5,3.7-4.2,6.4
	c-1.6,6,3.1,8.9,6.9,11c2.7,1.5,5.1,2.7,4.6,4.6c-0.4,1.4-1,2.5-4.3,2.5H85.8L84,47.1h11.3c3,0,5.8-0.6,8.1-2.4
	c2.3-1.7,3.5-4.2,4.3-7.1C108.4,34.9,107.9,32.2,106.1,30.2z M116.5,13.1l-8.9,34h7.5l8.9-34H116.5z M142.5,20h4l1.9-6.9h-4.5
	c-13.2,0-20.1,8.8-22.3,17c-2.6,9.7,2.6,17,12,17h6.3l1.9-6.9h-6.3c-3.5,0-8.4-2.8-6.4-10.1C130.6,24.3,136.3,20,142.5,20z
	 M173.6,41.3c-0.7-0.7-1.5-1-2.4-1c-0.9,0-1.7,0.3-2.4,1c-0.7,0.7-1,1.5-1,2.4c0,0.9,0.3,1.8,1,2.4c0.7,0.7,1.5,1,2.4,1
	c0.9,0,1.7-0.3,2.4-1c0.7-0.7,1-1.5,1-2.4C174.6,42.8,174.3,42,173.6,41.3z M173.3,45.8c-0.6,0.6-1.3,0.9-2.1,0.9
	c-0.8,0-1.5-0.3-2.1-0.9c-0.6-0.6-0.8-1.3-0.8-2.1c0-0.8,0.3-1.5,0.9-2.1c0.6-0.6,1.3-0.9,2.1-0.9c0.8,0,1.5,0.3,2.1,0.9
	c0.6,0.6,0.9,1.3,0.9,2.1C174.1,44.5,173.8,45.2,173.3,45.8z M172.7,45.3c0-0.1,0-0.2,0-0.3v-0.3c0-0.2-0.1-0.4-0.2-0.6
	c-0.1-0.2-0.4-0.3-0.6-0.4c0.2,0,0.4-0.1,0.5-0.2c0.2-0.2,0.4-0.4,0.4-0.7c0-0.5-0.2-0.8-0.6-0.9c-0.2-0.1-0.6-0.1-1-0.1h-1.3v3.7
	h0.7v-1.5h0.5c0.3,0,0.6,0,0.7,0.1c0.2,0.1,0.4,0.4,0.4,0.9v0.3l0,0.1c0,0,0,0,0,0c0,0,0,0,0,0h0.6l0,0
	C172.7,45.5,172.7,45.4,172.7,45.3z M171.7,43.6c-0.1,0.1-0.3,0.1-0.6,0.1h-0.6v-1.4h0.6c0.4,0,0.6,0,0.8,0.1
	c0.2,0.1,0.3,0.3,0.3,0.5C172.1,43.3,172,43.5,171.7,43.6z"></path>
</svg>
                    </a>
                    <div class="topbar_title">Asics Academy</div>
                </header>
            </div><!-- .topbar_inner -->
        </div><!-- .topbar_wrapper -->
    </div>
</div>

<div id="page">
    <div class="inner_wrapper">
        <div class="dialog">
            <div class="head">
                <div class="title">Login</div>
            </div>
            <div class="padding">
                <?php echo $error; ?>
                <?php if (!empty($tryError)) : ?>
                    <div class="alert"><?php echo $tryError; ?></div>
                <?php endif; ?>
                <form method="POST">
                    <div class="field">
                        <label for="acis-login-user">Username</label>
                        <input class="input" type="text" id="asics-login-user" name="user"
                               value="<?php echo $tryUser; ?>" required>
                    </div>
                    <div class="field">
                        <label for="acis-login-passwd">Password</label>
                        <input class="input" type="password" id="asics-login-passwd" name="passwd" required>
                    </div>
                    <div class="form-bottom">
                        <div class="btn-item">
                            <button class="btn" type="reset">Cancel</button>
                        </div>
                        <div class="btn-item">
                            <button class="btn" type="submit">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</body>

</html>