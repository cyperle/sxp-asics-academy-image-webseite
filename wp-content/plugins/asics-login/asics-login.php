<?php

/**
 * Plugin Name: Asics Login
 * Version: 1.180517.0
 * Description: Versieht das Frontend mit einem einfachen Login.
 * Author: Cynapsis Interactive
 * Author URI: https://cynapsis.de/
 * Text Domain: Cynapsis Interactive
 */

$lifetime = 365 * 24 * 60 * 60;
session_start();
setcookie(session_name(), session_id(), time() + $lifetime);

class AsicsUser
{

    static $users = array();

    public
        $name,
        $password,
        $rank = 0;

    function __construct($name, $pass, $rank)
    {
        $this->name = (String)$name;
        $this->password = (String)$pass;
        $this->rank = (int)$rank;
        self::$users[$name] = $this;
    }

    static function getUsers()
    {
        return self::$users;
    }

    static function getUserByName($name)
    {
        if (isset(self::$users[$name])) {
            return self::$users[$name];
        }
        return null;
    }

}

class AsicsLogin
{

    private $authenticated = false;
    private $user;
    const LOGOUT = 'logout';

    function getUser()
    {
        return $this->user;
    }

    function doLogin($username, $password)
    {
        $users = AsicsUser::getUsers();
        if (isset($users[$username])) {
            //User existiert
            if ($users[$username]->password == $password) {
                //Login erfolgreich
                $_SESSION["AsicsUserName"] = $username;
                $_SESSION["AsicsUserPassword"] = $password;
                $this->authenticated = true;
                $this->user = $users[$username];
                return 1;
            } else {
                //Passwort falsch
                return -1;
            }
        } else {
            //User existiert nicht
            return -2;
        }

    }

    function doLogout()
    {
        session_destroy();
        wp_redirect(get_home_url());
        exit;
    }

    function loginPage()
    {
        $tryUser = $_POST['user'] ?? false;
        $tryPassword = $_POST['passwd'] ?? false;
        $tryState = 0;
        $tryError = "";

        if ($tryUser && $tryPassword) {
            //Login-Formular ist abgesendet worden
            $tryState = $this->doLogin($tryUser, $tryPassword);
        }

        if ($tryState > 0) {
            //Login war erfolgreich, setze Wordpress ungehindert fort bzw. aktualisiere die Seite, um die Parameter zurückzusetzen
            $url = $_SERVER['REQUEST_URI'];
            header("Location: $url");
            die();
        } else {
            switch ($tryState) {
                case -1: {
                    $tryError = 'You entered the wrong password.';
                    break;
                }
                case -2: {
                    $tryError = 'Username does not exist.';
                    break;
                }
            }
            //Login war nicht erfolgreich oder hat noch nicht stattgefunden
            require("page/login.php");
            die();
        }

    }

    function checkLogout()
    {

        //ermittle Anfrage
        $request = $_SERVER['REQUEST_URI'];
        //Entferne ersten Slash von links
        $request = ltrim($request, '/');
        //Teile String an Slash
        $request = explode('/', $request);
        //Nehme den ersten Eintrag des Arrays
        $request = reset($request);
        //Entferne GET Parameter vom Resultat
        $request = reset(explode('?', $request));

        //Prüfe, ob das Resultat dem logout entspricht
        if ($request == self::LOGOUT) {
            return true;
        }

        return false;

    }

    function checkLogin()
    {

        if (is_user_logged_in()) {
            //Wenn bereits als Wordpress-User angemeldet, überspringe die Authentifizierung
            $this->user = new AsicsUser('admin', rand(), 100);
            return true;
        }

        if (isset($_SESSION["AsicsUserName"])) {
            if (
                $this->doLogin(
                    $_SESSION["AsicsUserName"],
                    $_SESSION["AsicsUserPassword"]
                )
                > 0
            ) {
                //Benutzer ist bereits korrekt eingeloggt
                return true;
            }
        }

        //Benutzer ist nicht eingeloggt oder Session ist ungültig
        return false;

    }

    function logout()
    {
        session_destroy();
    }

    function init()
    {

        //$this->logout();

        get_currentuserinfo();
        global $current_user;

        if ($this->checkLogout()) {
            //Benutzer loggt sich aus
            $this->doLogout();
        } else if ($this->checkLogin()) {
            //Benutzer ist eingeloggt, setze Wordpress fort
        } else {
            //Benutzer ist nicht eingeloggt
            $this->loginPage();
        }

    }

    function __construct()
    {
        new AsicsUser('asics', 'asics', 1);
        new AsicsUser('gel', 'kayano', 2);

        add_action('init', array($this, 'init'));
    }

}

$asicsLogin = new AsicsLogin();