var AsicsToggleSeason = {

    setCookie: function (key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (365 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + "; path=/";
    },

    onChoose: function ($item) {

        var theme = $item.data("theme");
        var reload = $item.data("reload");

        AsicsToggleSeason.setCookie("use_theme", theme);
        window.location = reload;

    },

    init: function () {

        var seasonItems = $("[data-theme][data-reload]");
        console.log(seasonItems);
        seasonItems.on("click", function () {
            AsicsToggleSeason.onChoose($(this));
        });

    }

};

$(AsicsToggleSeason.init());