<?php

/**
 * Plugin Name: Asics Toggle Season
 * Version: 1.180125.0
 *
 * Author: Cynapsis Interactive
 * Author URI: https://cynapsis.de/
 * Text Domain: Cynapsis Interactive
 */
class AsicsToggleSeason
{

    static $cookieName = "use_theme";
    static $themeFromCookie = "";

    static $getParamName = "use_theme";
    static $themeFromGetParam = "";

    static $determinedTheme = null;

    static function themeSwitch($currentTheme)
    {
        if (self::$determinedTheme === null) {

            //Ermittle einzusetzendes Theme
            $resultTheme = $currentTheme;
            $cookieTheme = self::$themeFromCookie;
            $getParamTheme = self::$themeFromGetParam;
            $themeSwitched = false;

            if (
                !empty($cookieTheme) && $resultTheme !== $cookieTheme
                OR
                !empty($getParamTheme) && $resultTheme !== $getParamTheme
            ) {
                $themeSwitched = true;
            }

            //Prüfe, ob das einzusetzende Theme existiert, ansonsten verwende das Default-Theme
            if ($themeSwitched) {

                $installedThemes = wp_get_themes();

                if (!empty($getParamTheme) && isset($installedThemes[$getParamTheme])) {
                    $resultTheme = $getParamTheme;
                } else if (!empty($cookieTheme) && isset($installedThemes[$cookieTheme])) {
                    $resultTheme = $cookieTheme;
                } else {
                    $resultTheme = $currentTheme;
                }

            }

            self::$determinedTheme = $resultTheme;

        }

        return self::$determinedTheme;

    }

    static function setCookieFromGetParam()
    {
        if (self::$determinedTheme == self::$themeFromGetParam) {
            setcookie(self::$cookieName, self::$themeFromGetParam, time() + 365 * 24 * 3600, "/");
        }

    }

    static function readUserTheme()
    {

        if (isset($_COOKIE[self::$cookieName])) {
            self::$themeFromCookie = $_COOKIE[self::$cookieName];
        }

        if (isset($_GET[self::$getParamName])) {
            self::$themeFromGetParam = $_GET[self::$getParamName];
            add_action('send_headers', 'AsicsToggleSeason::setCookieFromGetParam');
        }

    }

    static function label($label, $lang)
    {

        $translations = Array(
            "AW 2017" => Array(
                "de" => "AW 2017",
                "en" => "AW 2017",
                "es" => "AW 2017",
                "fr" => "AW 2017",
                "it" => "AW 2017",
                "pt-pt" => "AW 2017",
            ),
            "SS 2018" => Array(
                "de" => "SS 2018",
                "en" => "SS 2018",
                "es" => "SS 2018",
                "fr" => "SS 2018",
                "it" => "SS 2018",
                "pt-pt" => "SS 2018",
            ),
            "AW 2018" => Array(
                "de" => "AW 2018",
                "en" => "AW 2018",
                "es" => "AW 2018",
                "fr" => "AW 2018",
                "it" => "AW 2018",
                "pt-pt" => "AW 2018",
            ),
            "SS 2019" => Array(
                "de" => "SS 2019",
                "en" => "SS 2019",
                "es" => "SS 2019",
                "fr" => "SS 2019",
                "it" => "SS 2019",
                "pt-pt" => "SS 2019",
            ),
        );

        $useLabel = $label;
        if (isset($translations[$label])) {
            if (isset($translations[$label][$lang])) {
                $useLabel = $translations[$label][$lang];
            }
        }

        return $useLabel;

    }

    static function echoLabel($label, $lang)
    {
        echo self::label($label, $lang);
    }

    static function svg()
    {
        $output = <<<html
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 615.31 375.09"><path d="M713.15,490.73l-119-93.55V463.8H243.9a26.93,26.93,0,0,0,0,53.86H594.1v66.61ZM594,302.73A26.93,26.93,0,0,0,567.1,275.8H216.9V209.18l-119,93.55L216.9,396.27V329.66H567.1A26.93,26.93,0,0,0,594,302.73Z" transform="translate(-97.85 -209.18)"/></svg>
html;

        return $output;
    }

    static function echoSvg()
    {

        echo self::svg();

    }

    static function data($seasonUse)
    {

        $seasons = Array(
            "AW 2017" => "asics_aw17",
            "SS 2018" => "asics_ss18",
            "AW 2018" => "asics_aw18",
            "SS 2019" => "asics_ss19",
        );

        if (isset($seasons[$seasonUse])) {
            return ' data-theme="' . $seasons[$seasonUse] . '" data-reload="' . home_url() . '" ';
        } else {
            return "";
        }

    }

    //deprecated
    static function btn($goto, $label, $lang, $themeName)
    {

        $useLabel = self::label($label, $lang);

        $home_url = home_url();

        $output = <<<html
<div class="season_toggle_btn season_toggle" data-theme="$themeName" data-reload="$home_url">
    <div class="season_toggle_icon">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 615.31 375.09"><path d="M713.15,490.73l-119-93.55V463.8H243.9a26.93,26.93,0,0,0,0,53.86H594.1v66.61ZM594,302.73A26.93,26.93,0,0,0,567.1,275.8H216.9V209.18l-119,93.55L216.9,396.27V329.66H567.1A26.93,26.93,0,0,0,594,302.73Z" transform="translate(-97.85 -209.18)"/></svg>
</div>
    <div class="season_toggle_text">
        <div class="season_toggle_goto">$goto</div>
        <div class="season_toggle_label">$useLabel</div>
    </div>
</div>
html;

        echo $output;

    }

    static function init()
    {
        self::readUserTheme();

        add_filter('stylesheet', 'AsicsToggleSeason::themeSwitch');
        add_filter('template', 'AsicsToggleSeason::themeSwitch');
        add_action('wp_enqueue_scripts', function () {
            wp_enqueue_script('asics-toggle-season', plugins_url('/js/asics-toggle-season.js', __FILE__), null, false, true);
            //wp_enqueue_style('asics-toggle-season', plugins_url('/css/asics-toggle-season.css', __FILE__), null, false);
        });

    }

}

AsicsToggleSeason::init();