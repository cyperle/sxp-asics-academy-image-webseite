<?php

/**
 * Plugin Name: Asics Main
 * Version: 1.180529.0
 * Description: Bringt diverse Grundfunktionen, die alle Asics-Themes zwingend benötigen. Dies ist z.B. das Laden der Startseite der jeweiligen Season.
 * Author: Cynapsis Interactive
 * Author URI: https://cynapsis.de/
 * Text Domain: Cynapsis Interactive
 */

require("include/Asics.php");
require("include/Timer.php");
require("include/CyCache.php");
require("include/CyTransient.php");
require("include/helperFunctions.php");



