<?php

/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 25.01.2018
 * Time: 15:51
 */
abstract class CyCache
{

    public static $data = Array();

    public static function set($key, $value)
    {
        self::$data[$key] = $value;
    }

    public static function get($key)
    {
        return self::$data[$key];
    }

    public static function exists($key)
    {
        return isset(self::$data[$key]);
    }

}