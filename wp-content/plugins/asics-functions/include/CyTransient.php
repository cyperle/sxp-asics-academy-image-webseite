<?php

/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 14.12.2018
 * Time: 09:31
 */
class CyTransient
{

    private static $transientKeys = [];
    private static $keysQueried = false;
    private static $enable = true;

    public static function clear()
    {
        self::querySavedKeys();
        if (is_array(self::$transientKeys)) {
            foreach (self::$transientKeys as $key) {
                delete_transient($key);
            }
        }
        delete_transient('CyCache');
    }

    public static function querySavedKeys()
    {
        if (self::$keysQueried) {

        } else {
            if ($data = get_transient('CyCache')) {
                self::$transientKeys = $data;
            }
            self::$keysQueried = true;
        }
        return self::$transientKeys;
    }

    public static function set($key, $data, $expiry = DAY_IN_SECONDS)
    {
        self::querySavedKeys();
        $key = Asics::getThemeSlug() . '_' . $key;
        set_transient($key, $data, $expiry);
        if (!in_array($key, self::$transientKeys)) {
            self::$transientKeys[] = $key;
        }
        set_transient('CyCache', self::$transientKeys, DAY_IN_SECONDS);;
    }

    public static function get($key)
    {
        if (self::$enable) {
            self::querySavedKeys();
            $key = Asics::getThemeSlug() . '_' . $key;
            return get_transient($key);
        } else {
            return false;
        }
    }

    public static function delete($key)
    {
        self::querySavedKeys();
        $key = Asics::getThemeSlug() . '_' . $key;
        delete_transient($key);
    }

}

add_action('save_post', function () {
    CyTransient::clear();
});