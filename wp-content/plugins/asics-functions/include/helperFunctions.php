<?php
/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 25.01.2018
 * Time: 15:50
 */

//Erzeugt anhand eines Arrays einen String mit Attributen für HTML-Elemente.
//makeAttr(["class"=>["header","big"]) => class="header big"
function htmlAttr($attributes)
{
    $str = "";
    $first_attr = true;
    foreach ($attributes as $attribute => $values) {

        if ($first_attr) {
            $first_attr = false;
        } else {
            $str .= " ";
        }
        $str .= "$attribute=\"";

        if (!is_array($values)) $values = [$values];
        $first_value = true;
        foreach ($values as $value) {

            if ($first_value) {
                $first_value = false;
            } else {
                $str .= " ";
            }
            $str .= $value;

        }
        $str .= "\"";

    }
    return $str;
}

function wtf($var)
{

    echo "
<style>
code > pre { 
    font-size: 16px; 
    color: lime; 
    background: #000; 
    font-weight: normal; 
    font-family: Arial;
    max-height: 90vh;
    margin: 5vh 0;
    overflow: auto;
    white-space: pre;
    }
</style>";

    echo "<code><pre>";
    var_dump($var);
    echo "</pre></code>";
}