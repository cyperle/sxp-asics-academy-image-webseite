<?php

/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 14.12.2018
 * Time: 08:49
 */
class Timer
{

    private $name = "";
    private $start = 0;
    private $end = 0;

    public function __construct($name)
    {
        $this->name = $name;
        $this->start = microtime(true);
    }

    public function stop()
    {
        $this->end = microtime(true);
        $duration = $this->end - $this->start;
        echo "<script>console.log('Execution Time $this->name: ' + $duration)</script>";
    }

}