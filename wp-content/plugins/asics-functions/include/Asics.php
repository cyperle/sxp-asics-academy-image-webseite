<?php

/**
 * Created by PhpStorm.
 * User: Marvin Perleberg
 * Date: 25.01.2018
 * Time: 15:50
 */
//Enthält grundliegende Funktionalitäten, die spezifisch für die Anforderungen der Asics-Themes sind. Änderungen sollten mit Vorsicht vorgenommen werden, da diese für ein älteres Theme Fehler verursachen könnten (Ggf. Versions-Attribut integrieren, das vom initialisierenden Theme gesetzt wird, um Abwärtskompatibilität beizubehalten).

class Asics
{

    static $themeSlug = null;
    static $season = null;
    static $seasonIds = [];
    static $overviews = [];
    static $lang = "en";
    static $langs = [];
    static $techOverviews = null;
    static $imprint = null;
    static $privacy = null;
    static $availablePrograms = null; //Die auf der Startseite registrierten Programme

    /**
     * Ermittelt den Namen des Verzeichnisses, in dem sich das aktuelle Theme befindet.
     * @return null
     */
    static function getThemeSlug()
    {
        if (self::$themeSlug == null) {
            self::$themeSlug = get_template();
        }
        return self::$themeSlug;
    }

    /**
     * Ermittelt die Season des Themes.
     */
    static function deterSeason()
    {

        if (self::$season == null) {

            self::$season = [];

            $all_seasons = get_terms(array(
                'taxonomy' => 'season',
                'hide_empty' => false,
            ));

            $name = self::getThemeSlug();

            foreach ($all_seasons as $the_season) :

                $theme = get_field("theme_slug", "season_" . $the_season->term_id);

                if ($theme == $name) {
                    self::$season = $the_season;
                    self::$seasonIds[] = $the_season->term_id;
                }

            endforeach;

        }

    }

    /**
     * Gibt die Season des Themes zurück.
     * @return null
     */
    static function getSeason()
    {
        if (self::$season == null) {
            self::deterSeason();
        }
        return self::$season;
    }

    /**
     * Gibt die SeasonIDs zurück, die auf das Theme zutreffen.
     * @return array
     */
    static function getSeasonIds()
    {
        if (self::$season == null) {
            self::deterSeason();
        }
        return self::$seasonIds;
    }

    /**
     * Prüfe, ob sich ein Post in der aktuellen Season befindet.
     * Asics::isInSeason(get_field("season",$postID));
     * @param $data
     * @return bool
     */
    static function isInSeason($data)
    {
        if (!is_array($data)) return false;
        $intersect = !!count(array_intersect($data, self::getSeasonIds()));;
        return $intersect;
    }

    static function getSeasonHomeId()
    {

        //Ermittle Datensatz des Feldes home_page der aktuellen Season
        try {
            $homePage = get_field('home_page', 'season_' . self::$season->term_id);
            return $homePage;
        } catch (Exception $exception) {
            return null;
        }

    }

    /**
     * Tausche die aktuelle Query durch die Query zur Startseite der Season aus.
     * @param $query
     * @param $season
     */
    static function loadSeasonHome($query, $season)
    {

        global $wp_query;

        //Führe nur für die main_query aus.
        if (!$query->is_main_query()) {
            return;
        }

        //Stelle fest, ob die angefragte Seite die Startseite wäre. Dies kann zu diesem Zeitpunkt noch nicht mit internen Wordpress-Funktionen festgestellt werden. Die page_id der angefragten ist allerdings bereits im $wp_query-Objekt hinterlegt. Diese wird für eine manuelle Prüfung mit der Option "page_on_front" verwendet.
        $frontPage = get_option("page_on_front");
        if ('page' != get_option('show_on_front') || $frontPage != $wp_query->query_vars['page_id']) {
            return;
        }

        //Ist eine Season für das Theme gefunden worden?
        if ($season == null) {
            wp_die('Es ist keine Season für das Theme gegeben.');
            return;
        }

        //Prüfe die Startseite, die für die Season gesetzt wurde.
        $homePage = self::getSeasonHomeId();
        if ($homePage == null) {
            wp_die('Es ist keine Startseite für ' . $season->name . ' gegeben.');
            return;
        }

        //Wenn alle Bedingungen stimmen, setze die ID der Startseite in die Query ein.
        $query->query_vars["page_id"] = $homePage;

    }

    /**
     * Ermittle die Programme aus der Startseite
     * @return array|null
     */
    static function collectProgramsFromHome()
    {
        $id = self::getSeasonHomeId();

        if (self::$availablePrograms == null) {

        } else {
            return self::$availablePrograms;
        }

        $availablePrograms = Array();

        if (have_rows('content-module', $id)) {
            while (have_rows('content-module', $id)) {
                the_row();

                switch (get_row_layout()) {
                    case 'kategorie_navigation_layout' : {

                        foreach (get_sub_field('categorie_navigation') as $category) {
                            if (empty($category['programm_field_shoes'])) continue;
                            foreach ($category['programm_field_shoes'] as $program) {
                                if (empty($program['name'])) continue;
                                if (empty($program['name']->slug)) continue;
                                $availablePrograms[] = $program['name']->slug;
                            }
                        }

                        break;
                    }
                }

            }
        }

        self::$availablePrograms = $availablePrograms;
        return $availablePrograms;

    }

    /**
     * Sammle sämtliche Overviews der Programme ein und schließe diejenigen aus, die nicht auf der Startseite enthalten sind.
     */
    static function collectOverviews()
    {

        $availablePrograms = self::collectProgramsFromHome();

        global $icl_adjust_id_url_filter_off;

        $data = Array();

        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-product-overview.php'
        );
        $pages = get_pages($args);

        foreach ($pages as &$page) {

            $firstEntryForCategory = true;

            $id = $page->ID;;
            $prog = get_field('programs', $id);

            $progSlug = $prog->slug;
            if (in_array($progSlug, $availablePrograms)) {

            } else {
                continue;
            }

            $cat = get_field('categories', $id);

            if (isset($data[$cat->slug])) $firstEntryForCategory = false;

            $data[$cat->slug]['name'] = $cat->name;
            $data[$cat->slug]['programs'][$prog->slug] = Array(
                'name' => $prog->name,
                'permalink' => get_permalink($id),
                'layout' => get_field('layout', $prog)
            );

            if ($firstEntryForCategory) {
                $orig_flag_value = $icl_adjust_id_url_filter_off;
                $icl_adjust_id_url_filter_off = true;
                $originalID = apply_filters('wpml_object_id', $cat->term_id, 'acategory', false, 'en');
                $originalTerm = get_term($originalID, 'acategory');
                $icl_adjust_id_url_filter_off = $orig_flag_value;
                $data[$cat->slug]["css-name"] = strtolower($originalTerm->name);
            }


        }

        ksort($data);

        self::$overviews = $data;

    }

    /**
     * Gibt das Array der gesammelten Overviews zurück.
     * @return array
     */
    static function getOverviews()
    {
        return self::$overviews;
    }

    /**
     * Gibt aus der Kombination von Kategorie und Programm die zugehörige Overview zurück.
     * @param $category
     * @param $program
     * @return null
     */
    static function getOverviewUrl($category, $program)
    {
        $ret = self::$overviews[$category]['programs'][$program]['permalink'];
        if (empty($ret)) {
            $ret = null;
        }

        return $ret;
    }

    /**
     * Sammelt die Technologien ein, die zur Kategorie gehören.
     * @param $categoriename
     * @return array
     */
    static function collectTechsByCat($categoriename)
    {
        $data = Array();

        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-technology.php',
        );

        $pages = get_pages($args);
        $count = 0;

        foreach ($pages as $page) {

            if (!Asics::isInSeason(get_field("season", $page))) continue;

            $id = $page->ID;
            $cat = get_field('highlight', $id);

            if ($categoriename == $cat->slug) {

                $data[$count]['permalink'] = get_permalink($id);
                $data[$count]['thumb'] = get_field('thumbnail', $id);
                $data[$count]['short_name'] = get_field('short_name', $id);
                $data[$count]['name'] = get_field('name', $id);
                $data[$count]['short_description'] = get_field('short_description', $id);
                $count++;

            }


        }

        self::$techOverviews = $data;
        return $data;

    }

    /**
     * Sammelt die Overviews der Technologien ein.
     */
    static function collectTechOverviews()
    {
        if (self::$techOverviews == null) {

            $data = Array();

            $args = Array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-tech-overview.php',
            );

            $pages = get_pages($args);

            foreach ($pages as $page) {

                $id = $page->ID;
                $cat = get_field('highlight', $id);
                if (isset($cat->slug)) {
                    $data[$cat->slug]['name'] = $cat->name;
                    $data[$cat->slug]['permalink'] = get_permalink($id);
                }

            }

            self::$techOverviews = $data;

        }
    }

    /**
     * Gibt die URL einer Overview zur Technologie zurück.
     * @param $highlight
     * @return null
     */
    static function getTechOverviewUrl($highlight)
    {
        $ret = null;
        if (isset(self::$techOverviews[$highlight])) {
            $ret = self::$techOverviews[$highlight]['permalink'];
            if (empty($ret)) {
                $ret = null;
            }
        }
        return $ret;
    }

    /**
     * Gibt die Übersetzung einer Technologie zurück.
     * @param $highlight
     * @return array
     */
    static function getTechTranslation($highlight)
    {

        $data = Array();

        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-tech-overview.php',
        );

        $pages = get_pages($args);

        foreach ($pages as $page) {
            $id = $page->ID;
            if ($highlight == @get_field('highlight', $id)->slug) {
                $data['headline'] = get_field('overview_title', $id);
                //$data['subheadline'] = get_field('content-module',$id);
            }
        }

        return $data;

    }

    /**
     * Ermittelt die aktuelle aktive Sprache.
     */
    static function determineLang()
    {

        global $wp_query;

        if (defined('ICL_LANGUAGE_CODE')) {
            self::$lang = ICL_LANGUAGE_CODE;
        } else {
            self::$lang = "en";
        }

        if (function_exists('icl_get_languages')) {
            // self::$langs = icl_get_languages('skip_missing=1&orderby=name&order=asc&link_empty_to=' . get_site_url());
            $tmpLangs = apply_filters('wpml_active_languages', NULL, Array(
                'skip_missing' => 1,
                //'orderby' => 'name',
                //'order' => 'asc',
                //'link_empty_to' => get_site_url(),
            ));

            $post_id = $wp_query->post->ID;
            $activeLang = null;

            foreach ($tmpLangs as $lang) {
                $testThisPost = apply_filters('wpml_object_id', $post_id, 'post', false, $lang['code']);
                $testHomePost = apply_filters('wpml_object_id', self::getSeasonHomeId(), 'post', false, $lang['code']);
                if (!empty($testThisPost) && !empty($testHomePost)) {
                    if (!empty($lang['active'])) {
                        $activeLang = $lang;
                    } else {
                        self::$langs[] = $lang;
                    }
                }
            }

            if (!empty($activeLang)) {
                array_unshift(self::$langs, $activeLang);
            }


        } else {
            self::$langs = [];
        }

    }

    /**
     * Gibt die aktuelle Sprache zurück.
     * @return string
     */
    static function getLang()
    {
        return self::$lang;
    }

    /**
     * Gibt die vorhandenen Sprachen zurück.
     * */
    static function getLangs()
    {
        return self::$langs;
    }

    /**
     * Ermittelt die Seiten für Impressum und Datenschutz.
     */
    static function findImprintPrivacy()
    {

        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-impressum.php'
        );
        $pages = get_pages($args);

        foreach ($pages as &$page) {

            self::$imprint = Array(
                'title' => $page->post_title,
                'permalink' => get_permalink($page->ID)
            );

        }
        $args = Array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-datenschutz.php'
        );
        $pages = get_pages($args);

        foreach ($pages as &$page) {

            self::$privacy = Array(
                'title' => $page->post_title,
                'permalink' => get_permalink($page->ID)
            );

        }

    }

    /**
     * Gibt die Seite für das Impressum zurück.
     */
    static function getImprint()
    {
        if (self::$imprint !== null) {
            return self::$imprint;
        }
        return null;
    }

    /**
     * Gibt die Seite für Datenschutz zurück.
     */
    static function getPrivacy()
    {
        if (self::$privacy !== null) {
            return self::$privacy;
        }
        return null;
    }

    /**
     * Sortiert ein Array mit der Function "array_multisort" und unbestimmten Argumenten.
     */
    static function arrayOrderBy()
    {
        $args = func_get_args();
        $data = array_shift($args);

        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row) {
                    @ $tmp[$key] = $row[$field];
                }
                $args[$n] = $tmp;
            }
        }

        $args[] = &$data;
        call_user_func_array('array_multisort', $args);

        return array_pop($args);
    }

    /**
     * Gibt die Home-URL zurück.
     */
    static function getHomeUrl()
    {
        if (function_exists('icl_get_home_url')) {
            return icl_get_home_url();
        } else {
            return get_home_url();
        }
    }

    /**
     * Gibt abhängig vom aktiven Programm einen String für die Verwendung als Element-Klase zurück.
     */
    static function classProgram()
    {

        $key = "css_program";

        if (!CyCache::exists($key)) {

            $current_page = get_page_template_slug();

            $field_program = get_field("programs");
            $field_acategory = get_field("categories");
            $field_type = get_field('type');

            $scheme = "scheme_default";

            //Ermittle das Scheme des zugeordneten Programms.
            if (!empty($field_program)) {

                $tmp_scheme = get_field("layout", $field_program);

            } //Wenn das Feld für Programm nicht gesetzt/gefüllt, dann verwende das Scheme der Kategorie (z.B. Running).
            else if (!empty($field_acategory)) {

                //Verwende Scheme für Apparel/Technology, sofern eine entsprechende Seite aufgerufen ist. Ansonsten Default.
                /*if (in_array($current_page, ["page-apparel-single.php"])) {
                    $tmp_scheme = get_field("layout_apparel", $field_acategory);
                } else if (in_array($current_page, ["page-technology.php"])) {
                    $tmp_scheme = get_field("layout_technology", $field_acategory);
                }*/

                if ($field_type === "Apparels") {
                    $tmp_scheme = get_field("layout_apparel", $field_acategory);
                } else if ($field_type === "Shoes") {
                    $tmp_scheme = get_field("layout_technology", $field_acategory);
                }

            } //Wenn weder Programm noch Kategorie gesetzt sind, verwende default-Werte (z.B. Startseite).
            else {

            }

            if (!empty($tmp_scheme)) {
                $scheme = $tmp_scheme;
            }

            $css_class = $scheme;

            CyCache::set($key, $css_class);

        } else {

            $css_class = CyCache::get($key);

        }

        return $css_class;

    }

    /**
     * Gibt abhängig vom Seitentyp einen String für die Verwendung als Element-Klasse zurück.
     */
    static function classPageType()
    {

        $page_type = 'page_type_';
        $current_page = get_page_template_slug();

        switch ($current_page) {

            case 'page-product-single.php':
                $page_type .= 'product';
                break;
            case 'page-apparel-single.php':
                $page_type .= 'apparel';
                break;
            case 'page-product-overview.php':
                $page_type .= 'overview';
                break;
            case 'page-home.php':
                $page_type .= 'home';
                break;
            case 'page-datenschutz.php':
                $page_type .= 'simple';
                break;
            case 'page-tech-overview.php':
                $page_type .= 'tech_overview';
                break;
            case 'page-technology.php':
                $page_type .= 'tech';
                break;
            case 'page-impressum.php':
                $page_type .= 'simple';
                break;
            case 'page-special-kayano.php':
                $page_type .= 'special';
                break;
            default:
                $page_type .= 'none';

        }

        return $page_type;

    }

    /**
     * Gibt den Scheme-Klassennamen für die Spezial-Seite aus.
     */
    static function getSpecialTopbarScheme()
    {
        $classes = Array();
        $current_page = get_page_template_slug();
        if ($current_page === 'page-special-kayano.php') {
            $classes[] = 'body_scheme';
            $classes[] = 'scheme_2';
        }
        return join(" ", $classes);
    }

    static function init()
    {

        self::findImprintPrivacy();
        self::deterSeason();
        self::collectOverviews();

        add_action("wp", function ($query) {
            Asics::determineLang($query, self::$langs);
        });

        add_action("pre_get_posts", function ($query) {
            Asics::loadSeasonHome($query, self::$season);
        });

    }

}