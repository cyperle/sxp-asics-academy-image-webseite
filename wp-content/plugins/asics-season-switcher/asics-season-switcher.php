<?php

/**
 * Plugin Name: Asics Season Switcher
 * Version: 1.180121.0
 * Description: Gibt dem User die Möglichkeit, zwischen Seasons umzuschalten. So können mehrere Seasons parallel laufen.
 * Author: Cynapsis Interactive
 * Author URI: https://cynapsis.de/
 * Text Domain: Cynapsis Interactive
 */
class AsicsSeasonSwitcher
{

    static $cookieName = "use_theme";
    static $themeFromCookie = "";

    static $getParamName = "use_theme";
    static $themeFromGetParam = "";

    static $determinedTheme = null;

    static function themeSwitch($currentTheme)
    {
        if (self::$determinedTheme === null) {

            //Ermittle einzusetzendes Theme
            $resultTheme = $currentTheme;
            $cookieTheme = self::$themeFromCookie;
            $getParamTheme = self::$themeFromGetParam;
            $themeSwitched = false;

            if (
                !empty($cookieTheme) && $resultTheme !== $cookieTheme
                OR
                !empty($getParamTheme) && $resultTheme !== $getParamTheme
            ) {
                $themeSwitched = true;
            }

            //Prüfe, ob das einzusetzende Theme existiert, ansonsten verwende das Default-Theme
            if ($themeSwitched) {

                $installedThemes = wp_get_themes();

                if (!empty($getParamTheme) && isset($installedThemes[$getParamTheme])) {
                    $resultTheme = $getParamTheme;
                } else if (!empty($cookieTheme) && isset($installedThemes[$cookieTheme])) {
                    $resultTheme = $cookieTheme;
                } else {
                    $resultTheme = $currentTheme;
                }

            }

            self::$determinedTheme = $resultTheme;

        }

        return self::$determinedTheme;

    }

    static function setCookieFromGetParam()
    {
        if (self::$determinedTheme == self::$themeFromGetParam) {
            setcookie(self::$cookieName, self::$themeFromGetParam, time() + 365 * 24 * 3600, "/");
        }

    }

    static function readUserTheme()
    {

        if (isset($_COOKIE[self::$cookieName])) {
            self::$themeFromCookie = $_COOKIE[self::$cookieName];
        }

        if (isset($_GET[self::$getParamName])) {
            self::$themeFromGetParam = $_GET[self::$getParamName];
            add_action('send_headers', 'AsicsSeasonSwitcher::setCookieFromGetParam');
        }

    }

    static function getSeasonsList()
    {
        $seasons = get_terms(array(
            'taxonomy' => 'season',
            'hide_empty' => false,
        ));

        $hideToUser = !is_user_logged_in();

        $visibleSeasons = 0;
        $output = "";

        if (count($seasons) > 1) {

            $currentTheme = self::$determinedTheme;

            $output .= '<ul class="season_toggle">';

            foreach ($seasons as $season) {

                if ($hideToUser) {
                    if ($season->name[0] === "_") {
                        continue;
                    }
                }

                $visibleSeasons++;

                $seasonTheme = trim($season->description);
                $activeClass = "inactive";
                if ($currentTheme == $seasonTheme) {
                    $activeClass = "active";
                }

                $output .= '<li class="season_item ' . $activeClass . '" data-theme="' . $seasonTheme . '">' . $season->name . '</li>';

            }

            $output .= '</ul>';

        }

        return $output;

    }

    static function injectList()
    {

        $list = self::getSeasonsList();
        $html = "";

        if (!empty($list)) {

            $html .= '<div class="seasons_menue">';
            $html .= $list;
            $html .= '<div class="seasons_menue_open">Season wählen</div>';
            $html .= '</div>';

        }

        echo $html;

    }

    /**
     * Init
     */
    static function init()
    {

        self::readUserTheme();

        add_filter('stylesheet', 'AsicsSeasonSwitcher::themeSwitch');
        add_filter('template', 'AsicsSeasonSwitcher::themeSwitch');

        add_action('wp_footer', 'AsicsSeasonSwitcher::injectList');

        wp_enqueue_script('asics-season-switcher', plugins_url('/js/asics-season-switcher.js', __FILE__), null, false, true);
        wp_enqueue_style('asics-season-switcher', plugins_url('/css/asics-season-switcher.css', __FILE__), null, false);

    }

}

AsicsSeasonSwitcher::init();