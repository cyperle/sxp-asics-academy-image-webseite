var AsicsSeasonSwitcher = {

    setCookie: function (key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (365 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + "; path=/";
    },

    onChoose: function ($item) {
        var theme = $item.data("theme");
        AsicsSeasonSwitcher.setCookie("use_theme", theme);
        window.location.reload();
    },

    init: function () {

        var seasonItems = $(".season_item");
        seasonItems.on("click", function () {
            AsicsSeasonSwitcher.onChoose($(this));
        });

    }

};

$(AsicsSeasonSwitcher.init());