<?php

/**
 * Plugin Name: Asics Relations
 * Version: 1.171124.0
 * Description: Fügt diverse Taxonomien hinzu und nimmt einige diverse Einstellungen vor spezifisch für den Anwendungsfall der Asics-Themes.
 * Author: Cynapsis Interactive
 * Author URI: https://cynapsis.de/
 * Text Domain: Cynapsis Interactive
 */
abstract class AsicsRelations
{

    static function hide_slug()
    {
        ?>
        <style>
            .term-slug-wrap,
            .term-parent-wrap,
            .inline-edit-col label:nth-child(2),
            .row-actions .view,
            #slug,
            .column-slug {
                display: none
            }
        </style>
        <?php

    }

    static function hide_desc()
    {
        ?>
        <style>
            .term-description-wrap {
                display: none
            }
        </style>
        <?php
    }

    /**
     * Taxonomie Technologies (technology)
     */

    static function taxonomy_technology()
    {

        $labels = array(
            'name' => 'Technologies',
            'singular_name' => 'Technology',
            'search_items' => 'Search Technologies',
            'all_items' => 'All Technologies',
            'parent_item' => 'Parent Technology',
            'parent_item_colon' => 'Parent Technology:',
            'edit_item' => 'Edit Technology',
            'update_item' => 'Update Technology',
            'add_new_item' => 'Add new Technology',
            'new_item_name' => 'New Technology Name',
            'menu_name' => 'Technologies'
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array('slug' => 'technology'),
            'meta_box_cb' => false
        );

        register_taxonomy('technology', array('page'), $args);

    }

    /**
     * Taxonomie Programs (program)
     */

    static function taxonomy_program()
    {

        $labels = array(
            'name' => 'Programs',
            'singular_name' => 'Program',
            'search_items' => 'Search Programs',
            'all_items' => 'All Programs',
            'parent_item' => 'Parent Program',
            'parent_item_colon' => 'Parent Program:',
            'edit_item' => 'Edit Program',
            'update_item' => 'Update Program',
            'add_new_item' => 'Add new Program',
            'new_item_name' => 'New Program Name',
            'menu_name' => 'Programs'
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array('slug' => 'program'),
            'meta_box_cb' => false,
            'show_in_nav_menus' => true
        );

        register_taxonomy('program', array('page'), $args);

    }

    /**
     * Taxonomie Categories (acategory)
     */

    static function taxonomy_acategory()
    {

        $labels = array(
            'name' => 'Categories',
            'singular_name' => 'Category',
            'search_items' => 'Search Categories',
            'all_items' => 'All Categories',
            'parent_item' => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item' => 'Edit Category',
            'update_item' => 'Update Category',
            'add_new_item' => 'Add new Category',
            'new_item_name' => 'New Category Name',
            'menu_name' => 'Categories'
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array('slug' => 'acategory'),
            'meta_box_cb' => false,
            'show_in_nav_menus' => true
        );

        register_taxonomy('acategory', array('page'), $args);

    }

    /**
     * Taxonomie Sociallinks (sociallinks)
     */
    static function taxonomy_sociallinks()
    {

        $labels = array(
            'name' => 'Social Links',
            'singular_name' => 'Sociallink',
            'search_items' => 'Search Social Links',
            'all_items' => 'All Social Links',
            'parent_item' => 'Parent Social Link',
            'parent_item_colon' => 'Parent Social:',
            'edit_item' => 'Edit Social Link',
            'update_item' => 'Update Social Link',
            'add_new_item' => 'Add new Social Link',
            'new_item_name' => 'New Social Link Name',
            'menu_name' => 'Social Links'
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array('slug' => 'sociallinks'),
            'meta_box_cb' => false
        );

        register_taxonomy('sociallinks', array('page'), $args);

    }

    /**
     * Taxonomie Seasons (season)
     */

    static function taxonomy_season()
    {

        $labels = array(
            'name' => 'Season',
            'singular_name' => 'Season',
            'search_items' => 'Search Season',
            'all_items' => 'All Season',
            'parent_item' => 'Parent Season',
            'parent_item_colon' => 'Parent Season:',
            'edit_item' => 'Edit Season',
            'update_item' => 'Update Season',
            'add_new_item' => 'Add new Season',
            'new_item_name' => 'New Season Name',
            'menu_name' => 'Season'
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array('slug' => 'season'),
            'meta_box_cb' => false
        );

        register_taxonomy('season', array('page'), $args);

    }

    /**
     * Taxonomie Highlights (highlight)
     */

	static function taxonomy_highlight()
{

    $labels = array(
        'name' => 'Highlights',
        'singular_name' => 'Highlight',
        'search_items' => 'Search Highlights',
        'all_items' => 'All Highlights',
        'parent_item' => 'Parent Highlight',
        'parent_item_colon' => 'Parent Highlight:',
        'edit_item' => 'Edit Highlight',
        'update_item' => 'Update Highlight',
        'add_new_item' => 'Add new Highlight',
        'new_item_name' => 'New Highlight Name',
        'menu_name' => 'Highlights'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'highlight'),
        'meta_box_cb' => false
    );

    register_taxonomy('highlight', array('page'), $args);

}

    /**
     * Init
     */

    static function init()
    {

        $slugs = ["technology", "program", "acategory", "sociallinks", "season", "highlight"];
        foreach ($slugs as $slug) {
            add_action("init", "AsicsRelations::taxonomy_{$slug}", 0);
            //add_action("{$slug}_add_form_fields", "AsicsRelations::hide_slug", 10, 2);
            //add_action("{$slug}_edit_form_fields", "AsicsRelations::hide_slug", 10, 2);
        }

    }

}

AsicsRelations::init();